//
//  GIACourseDetailsViewController.m
//  GIA
//
//  Created by Kirti Nikam on 04/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIACourseDetailsViewController.h"
#import "GIAAppDelegate.h"

#define SPACE @"              "
@interface GIACourseDetailsViewController ()

@end

@implementation GIACourseDetailsViewController
@synthesize selectedCourseId;
@synthesize region,state,city,startDate,endDate,approval,seats,tentative_enrollments,confimed_enrollments;
@synthesize courseDetailTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self getCourseDetails];
    courseDetailTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"citydropdownbg.png"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [self setCourseDetailTableView:nil];
    [super viewDidUnload];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}
- (void)getCourseDetails{
    DebugLog(@"Selected as %@",selectedCourseId);
    
    
    //CREATE TABLE courses_table(sr_no,type,course_calendar_name,course_unique_auto_number,product,course_city_name,
    //                           course_city_state,course_city_zone,course_start_date,course_end_date,approval_status,
    //                           seats_available,tentative_enrollments,confimed_enrollments,course_calender_id,
    //                           course_city_record_id,course_city_region);
    
    if(sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK){
        NSString *sqlQueryString = [NSString stringWithFormat:@"SELECT course_city_region,course_city_name,course_city_state,course_start_date,course_end_date,approval_status,seats_available,tentative_enrollments,confimed_enrollments FROM COURSES_TABLE WHERE course_unique_auto_number = \"%@\"",selectedCourseId];
        const char *sqlQuery = [sqlQueryString UTF8String];
        DebugLog(@"Query -%@-",sqlQueryString);
      
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, sqlQuery, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                region      = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                city        = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                state       = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                startDate   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                endDate     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                approval    = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
                seats       = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
                tentative_enrollments   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
                confimed_enrollments    = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)];
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    [courseDetailTableView reloadData];
    NSDictionary *scheduleParams = [NSDictionary dictionaryWithObjectsAndKeys:self.title, @"Course", city, @"City",   nil];
    [Flurry logEvent:@"Courses_Schedule" withParameters:scheduleParams];
}
#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UILabel *lbl;
    UILabel *lblValue;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType              = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled     = YES;
        cell.selectionStyle             = UITableViewCellSelectionStyleNone;
//        cell.textLabel.textColor        = [UIColor whiteColor];
//        cell.textLabel.font             = TAHOMA_FONT(20);
//        cell.textLabel.textAlignment    = UITextAlignmentLeft;
        
        
        lbl = [[UILabel alloc] initWithFrame:CGRectMake(150,0,200,50.0)];
        lbl.tag = 1;
        lbl.font = TAHOMA_FONT(20);
        lbl.textAlignment = UITextAlignmentLeft;
        lbl.textColor = [UIColor whiteColor];
        lbl.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lbl];
        
        lblValue = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lbl.frame)+20,0,400,50.0)];
        lblValue.tag = 2;
        lblValue.font = TAHOMA_FONT(20);
        lblValue.textAlignment = UITextAlignmentLeft;
        lblValue.textColor = [UIColor whiteColor];
        lblValue.backgroundColor =  [UIColor clearColor];
        [cell.contentView addSubview:lblValue];
    }
    lbl = (UILabel *)[cell viewWithTag:1];
    lblValue = (UILabel *)[cell viewWithTag:2];
    switch (indexPath.row) {
        case 0:
        {
            lbl.text = @"";
            lblValue.text = @"";
        }
            break;
        case 1:
        {
            lbl.text = @"Region";
            lblValue.text = [NSString stringWithFormat:@":%@%@",SPACE,region];
        }
            break;
        case 2:
        {
            lbl.text = @"State";
            lblValue.text = [NSString stringWithFormat:@":%@%@",SPACE,state];
        }
            break;
        case 3:
        {
            lbl.text = @"City";
            lblValue.text = [NSString stringWithFormat:@":%@%@",SPACE,city];
        }
            break;
        case 4:
        {
            lbl.text = @"Start Date";
            lblValue.text = [NSString stringWithFormat:@":%@%@",SPACE,startDate];
        }
            break;
        case 5:
        {
            lbl.text = @"End Date";
            lblValue.text = [NSString stringWithFormat:@":%@%@",SPACE,endDate];
        }
            break;
        case 6:
        {
            lbl.text = @"Approval";
            lblValue.text = [NSString stringWithFormat:@":%@%@",SPACE,approval];
        }
            break;
        case 7:
        {
            lbl.text = @"Seats";
            lblValue.text = [NSString stringWithFormat:@":%@%@",SPACE,seats];
        }
            break;
        case 8:
        {
            lbl.text = @"";
            lblValue.text = @"";
        }
            break;
            
//        case 7:
//        {
//            lbl.text = @"Tentative Enrollments";
//            lblValue.text = [NSString stringWithFormat:@":%@%@",SPACE,tentative_enrollments];
//        }
//            break;
//        case 8:
//        {
//            lbl.text = @"Confimed Enrollments";
//            lblValue.text = [NSString stringWithFormat:@":%@%@",SPACE,confimed_enrollments];
//        }
//            break;
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
}

@end
