//
//  GIALabServicesMasterViewController.h
//  GIA
//
//  Created by Kirti Nikam on 05/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GIALabServicesDetailViewController;

@interface GIALabServicesMasterViewController : UITableViewController
@property (strong, nonatomic) GIALabServicesDetailViewController *detailViewController;
@property (nonatomic, retain) NSMutableArray *sideMenuArray;
@end
