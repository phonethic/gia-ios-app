//
//  masterdemoMasterViewController.h
//  masterdemo
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@class masterdemoDetailViewController;

@interface masterdemoMasterViewController : UITableViewController

@property (strong, nonatomic) masterdemoDetailViewController *detailViewController;
@property (nonatomic, retain) NSMutableArray *sideAboutMenuArray;

@end
