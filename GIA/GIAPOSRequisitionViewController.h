//
//  GIAPOSRequisitionViewController.h
//  GIA
//
//  Created by Kirti Nikam on 14/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "GIALoginViewController.h"
#import "GIAPOSOrderViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

@interface GIAPOSRequisitionViewController : UIViewController <iCarouselDataSource, iCarouselDelegate,GIAPOSOrderViewControllerDelegate,MFMailComposeViewControllerDelegate>
{
    
}
@property (nonatomic, retain) NSMutableDictionary *mainitemsTextDic;
@property (strong, nonatomic) IBOutlet UIImageView *mainImageView;
@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (nonatomic, retain) NSMutableArray *mainitems;
@property (nonatomic, retain) NSMutableArray *thumbsitems;
@property (strong, nonatomic) IBOutlet UILabel *headinglbl;
@property (strong, nonatomic) IBOutlet UILabel *subheadinglbl;
@property (strong, nonatomic) IBOutlet UITextField *quantityTextField;
@property (strong, nonatomic) IBOutlet UIButton *orderBtn;
@property (strong, nonatomic) IBOutlet UILabel *dimensionlbl;
@property (strong, nonatomic) IBOutlet UITextView *descriptiontextview;
@property (strong, nonatomic) IBOutlet UILabel *dimensiontextlbl;
@property (strong, nonatomic) IBOutlet UILabel *descriptiontextlbl;

- (IBAction)orderBtnClicked:(id)sender;

@end
