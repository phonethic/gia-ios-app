//
//  GIALabServicesDetailViewController.m
//  GIA
//
//  Created by Kirti Nikam on 05/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIALabServicesDetailViewController.h"
#import "MFSideMenu.h"
#import <QuartzCore/QuartzCore.h>
#import "constants.h"
#import "SQLiteDatabaseOpertaions.h"

@interface GIALabServicesDetailViewController ()

@end

@implementation GIALabServicesDetailViewController
@synthesize detailText;
@synthesize contentWebView;
@synthesize checkBoxBtn;
@synthesize checkBoxBtn1;
@synthesize checkBoxBtn2;
@synthesize checkBoxBtn3;
@synthesize checkBoxBtn4;
@synthesize checkBoxBtn5;
@synthesize checkBoxBtn6;
@synthesize checkBoxBtn7;
@synthesize checkBoxBtn8;
@synthesize commenttextView;
@synthesize submitBtn;
@synthesize lblDgrading,lblDsealing,lblCDgrading,lblCDidentification,lblSDgrading,lblCSidentification,lblCSorigin,lblQualityAS,lblPidentification;
@synthesize lblrequestHeader,lblCommentsHeader;
@synthesize requestView;
@synthesize scrollView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)setDetailText:(NSString *)newdetailText
{
    if (detailText != newdetailText) {
        detailText = newdetailText;
        
        if ([detailText hasPrefix:@"Request"]) {
            self.contentWebView.hidden = TRUE;
            self.requestView.hidden = FALSE;
        }else{
            self.contentWebView.hidden = FALSE;
            self.requestView.hidden = TRUE;
            // Update the view.
            [self loadWebViewWithUrl:detailText];
        }
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    scrollView.frame = CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height);
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    DebugLog(@"detailTExt %@",detailText);
    [self.navigationController.navigationBar setHidden:YES];
    [self setupMenuBarButtonItems];
    contentWebView.scrollView.bounces = TRUE;
    contentWebView.scrollView.scrollEnabled = TRUE;
    if ([detailText hasPrefix:@"Request"]) {
        self.contentWebView.hidden = TRUE;
        self.requestView.hidden = FALSE;
        [self.view bringSubviewToFront:self.requestView];
    }else{
        self.contentWebView.hidden = FALSE;
        self.requestView.hidden = TRUE;
        // Update the view.
        [self loadWebViewWithUrl:detailText];
        [self.view bringSubviewToFront:self.contentWebView];
    }
    [self changeLabelProperties:lblDgrading];
    [self changeLabelProperties:lblDsealing];
    [self changeLabelProperties:lblCDgrading];
    [self changeLabelProperties:lblCDidentification];
    [self changeLabelProperties:lblSDgrading];
    [self changeLabelProperties:lblCSidentification];
    [self changeLabelProperties:lblCSorigin];
    [self changeLabelProperties:lblQualityAS];
    [self changeLabelProperties:lblPidentification];
    [self changeLabelProperties:lblCommentsHeader];
    [self changeLabelProperties:lblrequestHeader];

    submitBtn.titleLabel.font = TAHOMA_FONT(25);
    
    requestView.backgroundColor = [UIColor clearColor];
    commenttextView.font = TAHOMA_FONT(18);
    commenttextView.layer.borderColor = [UIColor clearColor].CGColor;
    commenttextView.layer.borderWidth = 2.0;
    commenttextView.layer.cornerRadius = 2;
    commenttextView.backgroundColor = [UIColor whiteColor];
    
    scrollView.contentSize = CGSizeMake(self.scrollView.bounds.size.width,self.view.bounds.size.height);
    scrollView.scrollsToTop = YES;
    
    UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapDetected:)];
    viewTap.numberOfTapsRequired = 1;
    viewTap.cancelsTouchesInView = NO;
    [self.scrollView  addGestureRecognizer:viewTap];
    
    _requestArray = [[NSMutableArray alloc] init];
}
-(void)changeLabelProperties:(UILabel *)lbl
{
    lbl.backgroundColor = [UIColor clearColor];
    lbl.numberOfLines = 2;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.textAlignment = UITextAlignmentLeft;
    if ([lbl isEqual:lblCommentsHeader] || [lbl isEqual:lblrequestHeader]) {
        lbl.font   = TAHOMA_FONT(25);
        lbl.textColor   = DEFAULT_COLOR_SELECTION;
        lbl.shadowColor = [UIColor blackColor];
        lbl.shadowOffset = CGSizeMake(0, -1);
    }else{
        lbl.font = TAHOMA_FONT(18);
        lbl.textColor = [UIColor blackColor];
    }
}
- (void)tapDetected:(UIGestureRecognizer *)sender {
    [self.view endEditing:YES];
    [self scrollTobottom];
}
-(void)scrollTobottom
{
    CGPoint bottomOffset = CGPointMake(0, 0);
    [scrollView setContentOffset:bottomOffset animated:YES];
}

-(void)loadWebViewWithUrl:(NSString *)pagaeType
{
    self.contentWebView.hidden = FALSE;
    self.requestView.hidden = TRUE;
    //NSString *filePath = [[NSBundle mainBundle] pathForResource:@"jquery-1.7.1" ofType:@"js" inDirectory:@""];
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"jquery-1.7.1" ofType:nil];
//    DebugLog(@"filePath %@",filePath);
//    NSData *fileData = [NSData dataWithContentsOfFile:filePath];
//    DebugLog(@"fileData %@",fileData);
//    NSString *jsString = [[NSMutableString alloc] initWithData:fileData encoding:NSUTF8StringEncoding];
//    DebugLog(@"jsString %@",jsString);
//    [contentWebView stringByEvaluatingJavaScriptFromString:jsString];
//    
//    NSString *filePath1 = [[NSBundle mainBundle] pathForResource:@"tabs" ofType:@"js" inDirectory:@""];
//    DebugLog(@"filePath1 %@",filePath1);
//    NSData *fileData1 = [NSData dataWithContentsOfFile:filePath1];
//    NSString *jsString1 = [[NSMutableString alloc] initWithData:fileData1 encoding:NSUTF8StringEncoding];
//    DebugLog(@"jsString1 %@",jsString1);
//    [contentWebView stringByEvaluatingJavaScriptFromString:jsString1];
    
    
    [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:pagaeType ofType:@"html"] isDirectory:NO]]];
    
 //   NSString *urlString = [NSString stringWithFormat:@"http://192.168.254.46/gia-coloredstone/%@.html",pagaeType];
//    [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    
}

-(void)loadRequestView
{
    self.contentWebView.hidden = TRUE;
    self.requestView.hidden = FALSE;
}

- (IBAction)checkboxBtnPressed:(UIButton *)sender {
    DebugLog(@"%d",sender.tag);
    if([sender isSelected]){
        countSelection --;
        [sender setSelected:FALSE];
    }
    else{
        countSelection ++;
        [sender setSelected:TRUE];
    }
}
- (IBAction)submitBtnPressed:(id)sender {
    if (countSelection <= 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Please choose atleast one service."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }else{
        [self sendServiceEmail];
    }
}
-(void)sendServiceEmail
{
    @try{
        if (_requestArray == nil) {
            _requestArray = [[NSMutableArray alloc]init];
        }else{
            [_requestArray removeAllObjects];
        }
        for (int i = 0 ; i < 10 ; i++) {
            [_requestArray addObject:@""];
        }
        
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        NSString *firstName      = [defaults objectForKey:LOGIN_FIRST_NAME];
//        NSString *lastName       = [defaults objectForKey:LOGIN_LAST_NAME];
//        NSString *phoneNumber    = [defaults objectForKey:LOGIN_NUMBER];
//        NSString *emailAddress   = [defaults objectForKey:LOGIN_EMAIL];
////        NSString *address        = [defaults objectForKey:LOGIN_ADDRESS];
//        NSString *city           = [defaults objectForKey:LOGIN_CITY];
//        DebugLog(@"%@ %@ %@ %@ -%@-",firstName,lastName,phoneNumber,emailAddress,city);
//        
//        NSMutableString *bodyMessage = [NSMutableString stringWithString:@"<b><u>User Details  :</u></b><br>"];
//
//        if(firstName != nil || [firstName isEqualToString:@""])
//            [bodyMessage appendString:[NSString stringWithFormat:@"&nbsp;&nbsp;&nbsp;&nbsp;Name :  %@ ",firstName]];
//        if(lastName != nil || [lastName isEqualToString:@""])
//            [bodyMessage appendString:[NSString stringWithFormat:@"%@",lastName]];
//        if(phoneNumber != nil || [phoneNumber isEqualToString:@""])
//            [bodyMessage appendString:[NSString stringWithFormat:@"<br>&nbsp;&nbsp;&nbsp;&nbsp;Phone Number :  %@",phoneNumber]];
//        if(emailAddress != nil || [emailAddress isEqualToString:@""])
//            [bodyMessage appendString:[NSString stringWithFormat:@"<br>&nbsp;&nbsp;&nbsp;&nbsp;Email :  %@",emailAddress]];
//        if(city != nil || [city isEqualToString:@""])
//            [bodyMessage appendString:[NSString stringWithFormat:@"<br>&nbsp;&nbsp;&nbsp;&nbsp;City :  %@<br>",city]];

//        [bodyMessage appendString:@"<br><b><u>Service Request for  :</u></b><br><ol>"];
        if([checkBoxBtn isSelected]){
//            [bodyMessage appendString:@"<li>Diamond Grading</li>"];
            [_requestArray replaceObjectAtIndex:0 withObject:@"Diamond Grading"];
        }
        if([checkBoxBtn1 isSelected]){
//            [bodyMessage appendString:@"<li>Diamond Sealing</li>"];
            [_requestArray replaceObjectAtIndex:1 withObject:@"Diamond Sealing"];
        }
        if([checkBoxBtn2 isSelected]){
//            [bodyMessage appendString:@"<li>Coloured Diamond Grading</li>"];
            [_requestArray replaceObjectAtIndex:2 withObject:@"Coloured Diamond Grading"];
        }
        if([checkBoxBtn3 isSelected]){
//            [bodyMessage appendString:@"<li>Coloured Diamond Identification and Origin</li>"];
            [_requestArray replaceObjectAtIndex:3 withObject:@"Coloured Diamond Identification and Origin"];
        }
        if([checkBoxBtn4 isSelected]){
//            [bodyMessage appendString:@"<li>Synthetic Diamond Grading</li>"];
            [_requestArray replaceObjectAtIndex:4 withObject:@"Synthetic Diamond Grading"];
        }
        if([checkBoxBtn5 isSelected]){
//            [bodyMessage appendString:@"<li>Coloured Stone Identification</li>"];
            [_requestArray replaceObjectAtIndex:5 withObject:@"Coloured Stone Identification"];
        }
        if([checkBoxBtn6 isSelected]){
//            [bodyMessage appendString:@"<li>Coloured Stone Country of Origin Reports</li>"];
            [_requestArray replaceObjectAtIndex:6 withObject:@"Coloured Stone Country of Origin Reports"];
        }
        if([checkBoxBtn7 isSelected]){
//            [bodyMessage appendString:@"<li>Quality Assurance Services</li>"];
            [_requestArray replaceObjectAtIndex:7 withObject:@"Quality Assurance Services"];
        }
        if([checkBoxBtn8 isSelected]){
//            [bodyMessage appendString:@"<li>Pearl Identification and Classification</li>"];
            [_requestArray replaceObjectAtIndex:8 withObject:@"Pearl Identification and Classification"];
        }
        if (![commenttextView.text isEqualToString:@""]){
//            [bodyMessage appendString:[NSString stringWithFormat:@"</ol><br><b><u>Comments  :</u></b><br>&nbsp;&nbsp;&nbsp;&nbsp;%@<br>",commenttextView.text]];
            [_requestArray replaceObjectAtIndex:9 withObject:commenttextView.text];
        }
        
//        MFMailComposeViewController *picker=[[MFMailComposeViewController alloc]init];
//        picker.mailComposeDelegate=self;
//        picker.modalPresentationStyle = UIModalPresentationPageSheet;
//        picker.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
//        [picker setToRecipients:[[NSArray alloc] initWithObjects:LAB_SERVICE_EMAIL, nil]];
//        [picker setSubject:@"Service Request"];
//        [picker setMessageBody:bodyMessage isHTML:YES];
//        [self presentModalViewController:picker animated:YES];
        
//        if(DEVELOPER_MODE){
            BOOL success = [SQLiteDatabaseOpertaions insertIntoSERVICE_REQUEST_FORTable:_requestArray];
            if (success) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                    message:@"Your request has been sent successfully."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                
                [alertView show];
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                    message:@"Oops! an error has occured. Please try again."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                
                [alertView show];
            }
       
//        }
    }
    @catch (NSException *exception)
    {
        DebugLog(@"Exception %@",exception.reason);
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [controller dismissModalViewControllerAnimated:YES];
    if (result == MFMailComposeResultSent){
        DebugLog(@"email sent");
//        NSDictionary *labParams = [NSDictionary dictionaryWithObjectsAndKeys:@"", @"DealerStoreName",                              nil];
//        [Flurry logEvent:@"Laboratory_Services" withParameters:labParams];
        [SQLiteDatabaseOpertaions insertIntoSERVICE_REQUEST_FORTable:_requestArray];
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if(textView.frame.origin.y > scrollView.contentOffset.y)
        [scrollView setContentOffset:CGPointMake(0,textView.frame.origin.y-100) animated:YES];
    return YES;
}
#pragma mark - Web View
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //[self zoomToFit];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    
}

- (void)setupMenuBarButtonItems {
    switch (self.splitViewController.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.splitViewController.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.splitViewController.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.splitViewController.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.splitViewController.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.splitViewController.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.splitViewController.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.splitViewController.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [self setContentWebView:nil];
    [self setCheckBoxBtn:nil];
    [self setCheckBoxBtn1:nil];
    [self setCheckBoxBtn2:nil];
    [self setCheckBoxBtn3:nil];
    [self setCheckBoxBtn4:nil];
    [self setCheckBoxBtn5:nil];
    [self setCheckBoxBtn6:nil];
    [self setCheckBoxBtn7:nil];
    [self setCheckBoxBtn8:nil];
    [self setCommenttextView:nil];
    [self setSubmitBtn:nil];
    [self setRequestView:nil];
    [self setLblDgrading:nil];
    [self setLblDsealing:nil];
    [self setLblCDgrading:nil];
    [self setLblCDidentification:nil];
    [self setLblSDgrading:nil];
    [self setLblCSidentification:nil];
    [self setLblCSorigin:nil];
    [self setLblQualityAS:nil];
    [self setLblPidentification:nil];
    [self setLblrequestHeader:nil];
    [self setLblCommentsHeader:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}
@end
