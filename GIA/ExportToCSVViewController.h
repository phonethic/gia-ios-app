//
//  ExportToCSVViewController.h
//  GIA
//
//  Created by Kirti Nikam on 30/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface ExportToCSVViewController : UIViewController<MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIButton *exportDataBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblLastSync;
@property (strong, nonatomic) IBOutlet UILabel *lblFull;
@property (strong, nonatomic) IBOutlet UIButton *btnLastSync;
@property (strong, nonatomic) IBOutlet UIButton *btnFull;


- (IBAction)exportDataBtnCliked:(id)sender;
- (IBAction)btnGroupClicked:(id)sender;

@end
