//
//  GIAPOSOrderViewController.h
//  GIA
//
//  Created by Rishi on 17/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GIAPOSOrderViewControllerDelegate <NSObject>
- (void)didDismissPresentedViewController;
@end

@interface GIAPOSOrderViewController : UIViewController
{
}
@property (nonatomic, weak) id<GIAPOSOrderViewControllerDelegate> posdelegate;
@property (strong, nonatomic) IBOutlet UILabel *namelbl;
@property (strong, nonatomic) IBOutlet UILabel *emaillbl;
@property (strong, nonatomic) IBOutlet UILabel *mobilelbl;
@property (strong, nonatomic) IBOutlet UILabel *citylbl;
@property (strong, nonatomic) IBOutlet UILabel *profilelbl;
@property (strong, nonatomic) IBOutlet UITextView *addresstextView;
@property (strong, nonatomic) IBOutlet UIButton *confirmBtn;

- (IBAction)confirmBtnPressed:(id)sender;

@end
