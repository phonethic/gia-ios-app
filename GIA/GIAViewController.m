//
//  GIAViewController.m
//  GIA
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIAViewController.h"
#import "MFSideMenu.h"
#import "GIALoginViewController.h"
#import "constants.h"

@interface UINavigationController (KeyboardDismiss)

- (BOOL)disablesAutomaticKeyboardDismissal;

@end

@implementation UINavigationController(KeyboardDismiss)

- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

@end

@interface GIAViewController ()
{
    GIALoginViewController *loginController;
}
@end

@implementation GIAViewController

- (void)viewDidLoad
{
 //   [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(destroyEmitter) userInfo:nil repeats:YES];
 //   [self sparklesIt];
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.title = @"Home";
    
    [Flurry logEvent:@"Tab_Home"];
    
    [self setupMenuBarButtonItems];
    
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];

}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *firstName = [defaults objectForKey:LOGIN_FIRST_NAME];
    NSString *phoneNumber = [defaults objectForKey:LOGIN_NUMBER];
    DebugLog(@"firstName %@ phoneNumber %@",firstName,phoneNumber);
    if([firstName isEqualToString:@""] || [phoneNumber isEqualToString:@""] || firstName == nil || phoneNumber == nil)
    {
        if(loginController == nil)
        {
            loginController = [[GIALoginViewController alloc] initWithNibName:@"GIALoginViewController" bundle:nil];
        }
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:loginController];
        navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        navController.modalPresentationStyle = UIModalPresentationFormSheet;
        navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
    //  navController.navigationBar.tintColor = [UIColor colorWithRed:arc4random() % 100 / 100.0f green:arc4random() % 100 / 100.0f blue:arc4random() % 100 / 100.0f alpha:1.0f];
    //    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
    //                                                                                   target:self
    //                                                                                   action:@selector(didDismissPresentedViewController)];
    //    loginController.navigationItem.rightBarButtonItem = doneBarButton;
        loginController.navigationItem.title = @"Login";
        loginController.delegate = self;
        [self presentViewController:navController animated:YES completion:NULL];
    }
}

- (void)didDismissPresentedViewController
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    loginController.delegate = nil;
    loginController = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}


//-(void)sparklesIt
//{
//    int delay;
//    
//    UIImage *image;
//    
////    int imgSel=[self numberWithMin:0 max:6];
////    switch (imgSel) {
////        case 0:
////            image=[UIImage imageNamed:@"green.png"];
////            break;
////        case 1:
////            image=[UIImage imageNamed:@"silver.png"];
////            break;
////        case 2:
////            image=[UIImage imageNamed:@"yellow.png"];
////            break;
////        case 3:
////            image=[UIImage imageNamed:@"red.png"];
////            break;
////        case 4:
////            image=[UIImage imageNamed:@"purple.png"];
////            break;
////        case 5:
////            image=[UIImage imageNamed:@"pink.png"];
////            break;
////        case 6:
////            image=[UIImage imageNamed:@"blue.png"];
////            break;
////        default:
////            break;
////    }
//    
//    image = [UIImage imageNamed:@"sparkle.png"];
//    CGPoint pt=CGPointMake([self numberWithMin:0 max:320],[self numberWithMin:0 max:460]);
//    
//    float multiplier=0.25f;
//    
//    //create the emitter layer
//    emitter=[CAEmitterLayer layer];
//    emitter.emitterPosition=pt;
//    emitter.emitterMode=kCAEmitterLayerOutline;
//    emitter.emitterShape=kCAEmitterLayerCircle;
//    emitter.renderMode=kCAEmitterLayerAdditive;
//    emitter.emitterSize=CGSizeMake(100*multiplier, 0);
//    
//    //Flare particles emitted from the rocket as it flys
//    CAEmitterCell *flare = [CAEmitterCell emitterCell];
//    flare.contents = (__bridge id)([image CGImage]);
//    flare.emissionLongitude = (4 * M_PI) / 2;
//    flare.scale = 0.4;
//    flare.velocity = 100;
//    flare.birthRate = 45;
//    flare.lifetime = 1;
//    flare.emissionRange = M_PI / 7;
//    flare.alphaSpeed = -0.7;
//    flare.scaleSpeed = -0.1;
//    flare.scaleRange = 0.1;
//    flare.beginTime = 0.01;
//    flare.duration = 0.7;
//    
//    emitter.emitterCells=[NSArray arrayWithObject:flare];
//    [self.view.layer addSublayer:emitter];
//    
//    delay=[self numberWithMin:5 max:10];
//    [self performSelector:@selector(sparklesIt) withObject:nil afterDelay:delay];
//}
//
//-(void)destroyEmitter
//{
//    DebugLog(@"destroy emitter called ");
//    if (emitter!=nil) {
//        [emitter removeFromSuperlayer];
//        emitter=nil;
//    }
//}
//
//-(int)numberWithMin:(int)lmin max:(int)lmax
//{
//    return rand() % (lmax - lmin) + lmin;
//}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    float multiplier = 0.25f;
    
    CGPoint pt = [[touches anyObject] locationInView:self.view];
    
    //Create the emitter layer
    emitter = [CAEmitterLayer layer];
    emitter.emitterPosition = pt;
    emitter.emitterMode = kCAEmitterLayerOutline;
    emitter.emitterShape = kCAEmitterLayerCircle;
    emitter.renderMode = kCAEmitterLayerAdditive;
    emitter.emitterSize = CGSizeMake(100 * multiplier, 0);
    
    //Create the emitter cell
    CAEmitterCell* particle = [CAEmitterCell emitterCell];
    particle.emissionLongitude = M_PI;
    particle.birthRate = multiplier * 1000.0;
    particle.lifetime = multiplier;
    particle.lifetimeRange = multiplier * 0.35;
    particle.velocity = 180;
    particle.velocityRange = 130;
    particle.emissionRange = 1.1;
    particle.scaleSpeed = 1.0; // was 0.3
    
  //  particle.color = [[COOKBOOK_PURPLE_COLOR colorWithAlphaComponent:0.5f] CGColor];
    particle.color = [[UIColor colorWithRed:0.8 green:0.4 blue:0.2 alpha:0.1] CGColor];
    particle.contents = (__bridge id)([UIImage imageNamed:@"Particles_fire.png"].CGImage);
    particle.name = @"particle";
    
    emitter.emitterCells = [NSArray arrayWithObject:particle];
    [self.view.layer addSublayer:emitter];
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint pt = [[touches anyObject] locationInView:self.view];
    
    // Disable implicit animations
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    emitter.emitterPosition = pt;
    [CATransaction commit];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [emitter removeFromSuperlayer];
    emitter = nil;
}

- (void) touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchesEnded:touches withEvent:event];
}
- (IBAction)testBtnPressed:(id)sender {

    
}

@end
