//
//  masterdemoMasterViewController.m
//  masterdemo
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "masterdemoMasterViewController.h"
#import "masterdemoDetailViewController.h"
#import "constants.h"

@interface masterdemoMasterViewController ()
@end

@implementation masterdemoMasterViewController
@synthesize sideAboutMenuArray;
@synthesize detailViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"About GIA", @"About GIA");
        self.clearsSelectionOnViewWillAppear = NO;
        self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    self.detailViewController.detailItem = [sideAboutMenuArray objectAtIndex:0];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.tableView.backgroundColor = [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:1];
    self.tableView.separatorColor = [UIColor colorWithPatternImage:[self separatorImage]];
    
    [Flurry logEvent:@"Tab_About"];
    
    [self.navigationController.navigationBar setHidden:YES];
    
    sideAboutMenuArray= [[NSMutableArray alloc] init];
    [sideAboutMenuArray addObject:@"GIA Worldwide"];
    [sideAboutMenuArray addObject:@"GIA India"];
    [sideAboutMenuArray addObject:@"GIA Lab"];
    [sideAboutMenuArray addObject:@"GIA Education"];
    [sideAboutMenuArray addObject:@"Reach In India"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - Seperator
- (UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return sideAboutMenuArray.count; 
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.shadowColor = [UIColor blackColor];
        cell.textLabel.shadowOffset = CGSizeMake(0, -1);
        cell.textLabel.font = TAHOMA_FONT(25);
        cell.textLabel.textColor = [UIColor blackColor];
        
        cell.backgroundColor = [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:1];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:92/255.0 green:15/255.0 blue:46/255.0 alpha:1];
        cell.selectedBackgroundView = bgColorView;
    }
    cell.textLabel.text = [sideAboutMenuArray objectAtIndex:indexPath.row]; //[object description];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSDate *object = _objects[indexPath.row];
    //self.detailViewController.detailItem = [_sideAboutMenuArray objectAtIndex:indexPath.row];//object;
    if(indexPath.row == 4)
    {
        [Flurry logEvent:[NSString stringWithFormat:@"About_%@",[sideAboutMenuArray objectAtIndex:indexPath.row]]];
        [self.detailViewController loadMapView];
        return;
    }
    NSString *pageName;
    switch (indexPath.row) {
        case 0: { pageName = @"about-world"; }
            break;
            
        case 1: { pageName = @"about-india"; }
            break;
            
        case 2: { pageName = @"lab"; }
            break;
            
        case 3: { pageName = @"education"; }
            break;
            
        default:
            break;
    }
    DebugLog(@"-------%@-----------",[sideAboutMenuArray objectAtIndex:indexPath.row]);
    [Flurry logEvent:[NSString stringWithFormat:@"About_%@",[sideAboutMenuArray objectAtIndex:indexPath.row]]];
    [self.detailViewController loadWebViewWithUrl:pageName];
}

@end
