//
//  GIALabServicesMasterViewController.m
//  GIA
//
//  Created by Kirti Nikam on 05/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIALabServicesMasterViewController.h"
#import "GIALabServicesDetailViewController.h"
//#import "GIARequestServiceViewController.h"
#import "constants.h"

#define DIAMOND @"Diamonds"
#define COLORED_STONE @"Colored Stones"
#define PEARL @"Pearl"
#define REQUEST_SERVICE @"Request a Service"
#define PICKUP_SERVICE @"Pick up Service"
@interface GIALabServicesMasterViewController ()

@end

@implementation GIALabServicesMasterViewController
@synthesize sideMenuArray;
@synthesize detailViewController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.clearsSelectionOnViewWillAppear = NO;
        self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    }
    return self;
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    self.detailViewController.detailText = @"diamond";
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.tableView.backgroundColor = [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:1];
    self.tableView.separatorColor = [UIColor colorWithPatternImage:[self separatorImage]];
    
    [Flurry logEvent:@"Tab_Laboratory_Services"];
    
    [self.navigationController.navigationBar setHidden:YES];
    
    sideMenuArray= [[NSMutableArray alloc] init];
    [sideMenuArray addObject:DIAMOND];
    [sideMenuArray addObject:COLORED_STONE];
    [sideMenuArray addObject:PEARL];
    [sideMenuArray addObject:REQUEST_SERVICE];
    [sideMenuArray addObject:PICKUP_SERVICE];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - Seperator
- (UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return sideMenuArray.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.textLabel.shadowColor = [UIColor blackColor];
        cell.textLabel.shadowOffset = CGSizeMake(0, -1);
        cell.textLabel.font = TAHOMA_FONT(25);
        cell.textLabel.textColor = [UIColor blackColor];
        
        cell.backgroundColor = [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:1];
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:92/255.0 green:15/255.0 blue:46/255.0 alpha:1];
        cell.selectedBackgroundView = bgColorView;
    }
    cell.textLabel.text = [sideMenuArray objectAtIndex:indexPath.row]; //[object description];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([[sideMenuArray objectAtIndex:indexPath.row] isEqualToString:REQUEST_SERVICE]) {
        detailViewController.detailText = [sideMenuArray objectAtIndex:indexPath.row];
        DebugLog(@"-------%@-----------",[sideMenuArray objectAtIndex:indexPath.row]);
        [Flurry logEvent:@"Lab_Services_Request_Submit"];
        [self.detailViewController loadRequestView];
    }else{
    NSString *pageName;
    switch (indexPath.row) {
        case 0: { pageName = @"diamond"; }
            break;
            
        case 1: { pageName = @"colored-stones"; }
            break;
            
        case 2: { pageName = @"pearls"; }
            break;
            
        case 4: { pageName = @"pick-up"; }
            break;
            
        default:
            break;
    }
    DebugLog(@"-------%@-----------",[sideMenuArray objectAtIndex:indexPath.row]);
    [Flurry logEvent:[NSString stringWithFormat:@"Lab_Services_%@",[sideMenuArray objectAtIndex:indexPath.row]]];
    detailViewController.detailText = pageName;
    [self.detailViewController loadWebViewWithUrl:pageName];
    }
//    if ([[sideMenuArray objectAtIndex:indexPath.row] isEqualToString:REQUEST_SERVICE]) {
//        GIARequestServiceViewController *detailViewController1 = [[GIARequestServiceViewController alloc] initWithNibName:@"GIARequestServiceViewController" bundle:nil];
//        detailViewController1.title = [sideMenuArray objectAtIndex:indexPath.row];
//        
//        UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailViewController1];
//        
//        self.splitViewController.delegate = detailViewController1;
//        self.splitViewController.viewControllers = @[[self.splitViewController.viewControllers objectAtIndex:0], detailNavigationController];
//        self.splitViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
//    }
//    else{
//        NSString *pageName;
//        switch (indexPath.row) {
//            case 0: { pageName = @"diamond"; }
//                break;
//            case 1: { pageName = @"colored-stones"; }
//                break;
//            case 2: { pageName = @"pearls"; }
//                break;
//            default:
//                break;
//        }
//        GIALabServicesDetailViewController *detailViewController2 = [[GIALabServicesDetailViewController alloc] initWithNibName:@"GIALabServicesDetailViewController" bundle:nil];
//        UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailViewController2];
//        
//        self.detailViewController = detailViewController2;
//        
//        self.splitViewController.delegate = detailViewController2;
//        self.splitViewController.viewControllers = @[[self.splitViewController.viewControllers objectAtIndex:0], detailNavigationController];
//        self.splitViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
//        
//        self.detailViewController.detailText = pageName;
//        [self.detailViewController loadWebViewWithUrl:pageName];
//    }
}

@end
