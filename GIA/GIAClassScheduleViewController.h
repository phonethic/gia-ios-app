//
//  GIAClassScheduleViewController.h
//  GIA
//
//  Created by Kirti Nikam on 14/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"

@interface GIAClassScheduleViewController : UIViewController <MWPhotoBrowserDelegate>
@property (nonatomic, retain) NSMutableArray *photos;
@end
