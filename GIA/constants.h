
#ifndef _ALERT_MESSAGES_H_
#define _ALERT_MESSAGES_H_
#import "Flurry.h"
#define GIADelegate (GIAAppDelegate *)[[UIApplication sharedApplication] delegate]

#define DEVELOPER_MODE NO

#define FLURRY_APPID @"V4KHZGXHPVY89JWD3X92"

#define APPIRATER_APPID @""

#define PARSE_APPID @"DdZoJEvJJIGjsU2NPdFH1vY3Aens0GdEPQSYwKK8"
#define PARSE_CLIENTKEY @"jqETLAmIl37uMQp9X5hwnInc9CGwfw2JRMdUAyRZ"

#define ALERT_TITLE @"GIA"
//#define POS_EMAIL1 @"tmahindr@gia.edu"
//#define POS_EMAIL2 @"asawant@gia.edu"

#define DEFAULT_COLOR [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:1]
#define DEFAULT_COLOR_SELECTION [UIColor colorWithRed:92/255.0 green:15/255.0 blue:46/255.0 alpha:1]

#define LOGIN_FIRST_NAME @"firstName"
#define LOGIN_LAST_NAME @"lastname"
#define LOGIN_EMAIL @"email"
#define LOGIN_NUMBER @"number"
#define LOGIN_CITY @"city"
#define LOGIN_PROFILE @"profile"
#define LOGIN_ADDRESS @"address"

//#define DEBUG_MODE

#ifdef DEBUG_MODE
#define DebugLog( s, ... ) NSLog(@"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DebugLog( s, ... )
#endif

#define EXPORT_NOTIFICATION @"exportNotification"
#define EXPORT_KEY @"export"
#define POS_EMAIL @"megha.karolia@u-5jib45w5ekq3u2rc37nvs20rb75su7r4hgxhhgf3uoybupvhv.3-muzzeay.al.case.salesforce.com"
#define FEEDBACK_EMAIL @"asawant@gia.edu"
#define LAB_SERVICE_EMAIL @"lab.giaindia@gia.edu"
#define ADMIN_EMAILIDS @[@"sagar@phonethics.in",@"kirti@phonethics.in",@"rishi@phonethics.in",FEEDBACK_EMAIL,LAB_SERVICE_EMAIL]

#define POS_FILE @"POS_Requisition.csv"
#define REQEST_SERVICE_FILE @"Request_Service.csv"

#define ADMIN_LASTSYNC_EXPORT_DATE @"lastsyncexportdate"

//CREATE TABLE IF NOT EXISTS POSREQ (ID INTEGER PRIMARY KEY AUTOINCREMENT,FIRSTNAME TEXT,LASTNAME TEXT,PHONE TEXT,EMAIL TEXT,CITY TEXT,PROFILE TEXT,ADDRESS TEXT,PRODUCT TEXT,QUANTITY TEXT,DATETIME DOUBLE);
//CREATE TABLE IF NOT EXISTS SERVICE_REQUEST_FOR (ID INTEGER PRIMARY KEY AUTOINCREMENT,FIRSTNAME TEXT,LASTNAME TEXT,PHONE TEXT,EMAIL TEXT,CITY TEXT,PROFILE TEXT,Diamond_Grading TEXT,Diamond_Sealing TEXT,Coloured_Diamond_Grading TEXT,Coloured_Diamond_Identification_and_Origin TEXT,Synthetic_Diamond_Grading TEXT,Coloured_Stone_Identification TEXT,Coloured_Stone_Country_of_Origin_Reports TEXT,Quality_Assurance_Services TEXT,Pearl_Identification_and_Classification  TEXT,COMMENTS TEXT,DATETIME DOUBLE);
#endif
