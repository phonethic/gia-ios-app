//
//  masterdemoDetailViewController.h
//  masterdemo
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface masterdemoDetailViewController : UIViewController <UISplitViewControllerDelegate,UIWebViewDelegate>

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (strong, nonatomic) IBOutlet UIWebView *contentWebview;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIImageView *mapimageview;

-(void)loadWebViewWithUrl:(NSString *)url;
-(void)loadMapView;
@end
