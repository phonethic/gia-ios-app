//
//  ExportToCSVViewController.m
//  GIA
//
//  Created by Kirti Nikam on 30/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "ExportToCSVViewController.h"
#import "constants.h"
#import "MFSideMenu.h"
#import "GIAAppDelegate.h"
#import "SQLiteDatabaseOpertaions.h"
#import "MBProgressHUD.h"

@interface ExportToCSVViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end

@implementation ExportToCSVViewController
@synthesize progressHUD;
@synthesize lblHeader,lblFull,lblLastSync;
@synthesize exportDataBtn;
@synthesize btnLastSync,btnFull;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupMenuBarButtonItems];
   // [_spinnerView setHidden:YES];
    
    [Flurry logEvent:@"Tab_Export_Data"];
    
    [self changeLabelProperties:lblFull];
    [self changeLabelProperties:lblLastSync];
    [self changeLabelProperties:lblHeader];
    
    exportDataBtn.titleLabel.font    = TAHOMA_FONT(25);
    btnLastSync.selected = YES;
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)changeLabelProperties:(UILabel *)lbl
{
    lbl.backgroundColor = [UIColor clearColor];
    lbl.numberOfLines = 2;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.textAlignment = UITextAlignmentLeft;
    if ([lbl isEqual:lblHeader]) {
        lbl.font   = TAHOMA_FONT(25);
        lbl.textColor   = DEFAULT_COLOR_SELECTION;
        lbl.shadowColor = [UIColor blackColor];
        lbl.shadowOffset = CGSizeMake(0, -1);
    }else{
        lbl.font = TAHOMA_FONT(18);
        lbl.textColor = [UIColor blackColor];
    }
}

- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = TAHOMA_FONT(20);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}

- (IBAction)exportDataBtnCliked:(id)sender {
    [self showProgressHUDWithMessage:@"Proccesing"];
    NSString *exporttype = SQL_LAST_SYNC;
    if ([btnFull isSelected]) {
        exporttype = SQL_FULL;
    }
    BOOL success =[SQLiteDatabaseOpertaions exportSQLITEDataToCSV:exporttype];
    if (success) {
        [self sendMail];
    }
}

- (IBAction)btnGroupClicked:(UIButton *)button {
    DebugLog(@"%d",button.tag);
    UIButton *but;
    for (int index = 101;index < 103;index ++) {
        but = (UIButton *)[self.view viewWithTag:index];
        if ([but isKindOfClass:[UIButton class]] && but.tag!=button.tag) {
            [but setSelected:NO];
        }
    }
    if (!button.selected) {
        button.selected = !button.selected;
    }
}

-(void) sendMail
{
    [self hideProgressHUD:YES];

    if ([MFMailComposeViewController canSendMail])
    {
        [self mailPOSAttachment];
    }
    else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Can't Send Mail"
                                                            message:@"Device is unable to send email."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        
        [alertView show];
    }
}

-(void)mailPOSAttachment
{
    NSString *posString = [GIA_APP_DELEGATE getTextFromFile:POS_FILE];
    if ([posString isEqualToString:@""]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Can't Send Mail"
                                                            message:@"POS data not found to export."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }else{
        MFMailComposeViewController *posmailComposer = [[MFMailComposeViewController alloc] init];
        posmailComposer.mailComposeDelegate = self;
        posmailComposer.toolbar.tag = 1;
        posmailComposer.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        
        [posmailComposer setToRecipients:[[NSArray alloc] initWithObjects:POS_EMAIL, nil]];
        
        if (btnFull.isSelected) {
            [posmailComposer setSubject:[NSString stringWithFormat:@"GIA - iPad exported POS excel file (full)"]];
        }else{
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSDate *lastsyncDate = [defaults objectForKey:ADMIN_LASTSYNC_EXPORT_DATE];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"MMM dd, yyyy 'at' HH:mm"];
            
            NSString *dateString;
            if (lastsyncDate != nil) {
                dateString = [dateFormat stringFromDate:lastsyncDate];
                [posmailComposer setSubject:[NSString stringWithFormat:@"GIA - iPad exported POS excel file (from %@ to present)",dateString]];
            }else{
                dateString = [dateFormat stringFromDate:[NSDate date]];
                [posmailComposer setSubject:[NSString stringWithFormat:@"GIA - iPad exported POS excel file (%@)",dateString]];
            }
        }
        [posmailComposer setMessageBody:@"Hi,\n\nKindly find below attachment." isHTML:NO];
        
        if (![posString isEqualToString:@""])
        {
            NSData *posData = [posString dataUsingEncoding:NSUTF8StringEncoding];
            [posmailComposer addAttachmentData:posData mimeType:@"text/csv" fileName:POS_FILE];
        }
        
        [posmailComposer setModalPresentationStyle:UIModalPresentationFormSheet];
        [self presentModalViewController:posmailComposer animated:YES];
    }
    if(DEVELOPER_MODE) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDate *myDate = [NSDate date];
        [defaults setObject:myDate forKey:ADMIN_LASTSYNC_EXPORT_DATE];
        [defaults synchronize];
    }
}

-(void)mailRequestServiceAttachment
{
    NSString *reqString = [GIA_APP_DELEGATE getTextFromFile:REQEST_SERVICE_FILE];
    if ([reqString isEqualToString:@""]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Can't Send Mail"
                                                            message:@"Request Service data not found to export."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }else{
        MFMailComposeViewController *servicemailComposer = [[MFMailComposeViewController alloc] init];
        servicemailComposer.mailComposeDelegate = self;
        servicemailComposer.toolbar.tag = 2;
        servicemailComposer.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        
        [servicemailComposer setToRecipients:[[NSArray alloc] initWithObjects:POS_EMAIL, nil]];
        
        if (btnFull.isSelected) {
            [servicemailComposer setSubject:[NSString stringWithFormat:@"GIA - iPad exported Service excel file (full)"]];
        }else{
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSDate *lastsyncDate = [defaults objectForKey:ADMIN_LASTSYNC_EXPORT_DATE];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"MMM dd, yyyy 'at' HH:mm"];
            
            NSString *dateString;
            if (lastsyncDate != nil) {
                dateString = [dateFormat stringFromDate:lastsyncDate];
                [servicemailComposer setSubject:[NSString stringWithFormat:@"GIA - iPad exported Service excel file (from %@ to present)",dateString]];
            }else{
                dateString = [dateFormat stringFromDate:[NSDate date]];
                [servicemailComposer setSubject:[NSString stringWithFormat:@"GIA - iPad exported Service excel file (%@)",dateString]];
            }
        }
        [servicemailComposer setMessageBody:@"Hi,\n\nKindly find below attachment." isHTML:NO];
        
        if (![reqString isEqualToString:@""])
        {
            NSData *reqData = [reqString dataUsingEncoding:NSUTF8StringEncoding];
            [servicemailComposer addAttachmentData:reqData mimeType:@"text/csv" fileName:REQEST_SERVICE_FILE];
        }
        [servicemailComposer setModalPresentationStyle:UIModalPresentationFormSheet];
        [self presentModalViewController:servicemailComposer animated:YES];
    }
    if(DEVELOPER_MODE) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDate *myDate = [NSDate date];
        [defaults setObject:myDate forKey:ADMIN_LASTSYNC_EXPORT_DATE];
        [defaults synchronize];
    }
}

#pragma mark MFMailComposeViewController Delegate Methods
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    NSLog(@"selected toolbar tag ====%d",controller.toolbar.tag);
    DebugLog(@"Mail Result %u",result);
    switch (result)
    {
        case MFMailComposeResultCancelled :
            DebugLog(@"Mail Cancelled");
            break;
        case MFMailComposeResultSaved :
            DebugLog(@"Mail Saved");
            break;
        case MFMailComposeResultSent :
        {
            DebugLog(@"Mail Sent");
            
            [Flurry logEvent:@"Export_Data"];
            
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Mail sent successfully"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSDate *myDate = [NSDate date];
            [defaults setObject:myDate forKey:ADMIN_LASTSYNC_EXPORT_DATE];
            [defaults synchronize];
        }
            break;
        case MFMailComposeResultFailed :
        {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:@"Unknown Error\nMail failed"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];
        }
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES  completion:^(void){
        if(controller.toolbar.tag == 1 )
        {
            [self mailRequestServiceAttachment];
        }
    }];
}


- (void)viewDidUnload {
    [self setExportDataBtn:nil];
    [self setLblHeader:nil];
    [self setLblLastSync:nil];
    [self setLblFull:nil];
    [self setBtnLastSync:nil];
    [self setBtnFull:nil];
    [super viewDidUnload];
}
@end
