//
//  GIAMediaViewController.h
//  GIA
//
//  Created by Rishi on 06/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import <MediaPlayer/MediaPlayer.h>

@interface GIAMediaViewController : UIViewController <iCarouselDataSource, iCarouselDelegate> {
    int fullscreen;
}

@property (strong, nonatomic) IBOutlet iCarousel *carousel;
@property (nonatomic, retain) NSMutableArray *thumbsitems;
@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;
@property (strong, nonatomic) IBOutlet UIImageView *messageimgView;
@property (strong, nonatomic) IBOutlet UILabel *messagelbl;
@property (nonatomic, retain) NSMutableDictionary *videoTextDic;
@end
