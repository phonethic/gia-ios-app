//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "MFSideMenu.h"
#import "masterdemoMasterViewController.h"
#import "masterdemoDetailViewController.h"
#import "GIAViewController.h"
#import "GIAPOSRequisitionViewController.h"
//#import "GIAClassScheduleViewController.h"
#import "GIAdefaultViewController.h"
#import "GIACoursesViewController.h"
#import "GIAFeesScheduleViewController.h"
#import "GIAMediaViewController.h"
#import "constants.h"
#import "GIALabServicesMasterViewController.h"
#import "GIALabServicesDetailViewController.h"
//#import "GIARequestServiceViewController.h"
#import "GIAFeedBackViewController.h"
#import "ExportToCSVViewController.h"
#import "GIALocalRepViewController.h"
#import "GIAContactUSViewController.h"

#define HOME @"Home"
#define ABOUT @"About GIA"
#define POS @"POS Requisition"
#define COURSES @"Courses Schedule"
#define FEES @"Fees Structure"
#define CLASS @"Class Schedule"
#define MEDIA @"Media Presentation"
#define FEEDBACK @"Submit a Query"
#define LOGOUT @"Logout"
#define LAB_SERVICES @"GIA Laboratory Services"
#define SERVICE @"Request Service"
#define EXPORT @"Export Data"
#define REPRESENTATIVE @"Local Representative"
#define CONTACT @"Contact Us"

@interface SideMenuViewController()
@property(nonatomic, strong) UISearchBar *searchBar;
@end

@implementation SideMenuViewController

@synthesize sideMenu;
@synthesize searchBar;
@synthesize sideMenuArray;

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void) viewDidLoad {
    [super viewDidLoad];
    currentIndex = 0;
    preIndex = 0;

    self.tableView.backgroundColor = [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:1];
    self.tableView.separatorColor = [UIColor colorWithPatternImage:[self separatorImage]];
    self.tableView.scrollEnabled = NO;

    sideMenuArray = [[NSMutableArray alloc] init];
    [sideMenuArray addObject:HOME];
    [sideMenuArray addObject:ABOUT];
    [sideMenuArray addObject:LAB_SERVICES];
    [sideMenuArray addObject:POS];
    [sideMenuArray addObject:COURSES];
    [sideMenuArray addObject:FEES];
//    [sideMenuArray addObject:CLASS];
    [sideMenuArray addObject:MEDIA];
    [sideMenuArray addObject:FEEDBACK];
    [sideMenuArray addObject:REPRESENTATIVE];
    [sideMenuArray addObject:CONTACT];
    [sideMenuArray addObject:LOGOUT];
//     [sideMenuArray addObject:SERVICE];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *emailString = [defaults objectForKey:LOGIN_EMAIL];
    if(emailString != nil && ![emailString isEqualToString:@""] &&  [ADMIN_EMAILIDS containsObject:emailString]) {
        [sideMenuArray insertObject:EXPORT atIndex:sideMenuArray.count-1];
    }
    
    UINavigationBar *tempNavigationBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [tempNavigationBar setBarStyle:UIBarStyleDefault];
    tempNavigationBar.tintColor = [UIColor blackColor];
    UINavigationItem *navItem = [UINavigationItem alloc];
    navItem.title = @"Menu";
    [tempNavigationBar pushNavigationItem:navItem animated:false];
    self.tableView.tableHeaderView = tempNavigationBar;

//    NSArray *fonts = [UIFont familyNames];
//
//    for(NSString *string in fonts){
//        DebugLog(@"%@", string);
//    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotificationResponse:) name:EXPORT_NOTIFICATION object:nil];
}


#pragma mark -
#pragma mark - UITableViewDataSource

//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return [NSString stringWithFormat:@"Section %d", section];
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 59;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [sideMenuArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.textLabel.backgroundColor = [UIColor clearColor];
//        cell.textLabel.textColor = [UIColor lightGrayColor];
        cell.textLabel.shadowColor = [UIColor blackColor];
        cell.textLabel.shadowOffset = CGSizeMake(0, -1);
        //cell.textLabel.highlightedTextColor = [UIColor colorWithRed:128/255.0 green:126/255.0 blue:124/255.0 alpha:1];
//        UIView *bgColorView = [[UIView alloc] init];
//      //  bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1];
//        cell.selectedBackgroundView = bgColorView;
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = TAHOMA_FONT(25);
        
        cell.backgroundColor = [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:1];

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:92/255.0 green:15/255.0 blue:46/255.0 alpha:1];
        cell.selectedBackgroundView = bgColorView;

//        if (indexPath.row == 0) { //Home
//            cell.imageView.image = [UIImage imageNamed:@"Icon_Home.png"];
//        } else if (indexPath.row == 1) { //Profile
//            cell.imageView.image = [UIImage imageNamed:@"Icon_Profile.png"];
//        } else if (indexPath.row == 2) { //Leader Borad
//            cell.imageView.image = [UIImage imageNamed:@"Icon_Leaderboard.png"];
//        } else if (indexPath.row == 3) { //Gurus Speak
//            cell.imageView.image = [UIImage imageNamed:@"Icon_LearnDance.png"];
//        } else if (indexPath.row == 4) { //Learn Dance
//            cell.imageView.image = [UIImage imageNamed:@""];
//        } else if (indexPath.row == 5) { //Tweets
//            cell.imageView.image = [UIImage imageNamed:@"Icon_Twitter.png"];
//        }  else if (indexPath.row == 6) { //Submissions
//            cell.imageView.image = [UIImage imageNamed:@"Icon_Upload.png"];
//        }  else if (indexPath.row == 7) { //Upload Videos
//            cell.imageView.image = [UIImage imageNamed:@"Icon_Upload_Videos.png"];
//        } else if (indexPath.row == 8) { //Help
//            cell.imageView.image = [UIImage imageNamed:@"Icon_Help.png"];
//        } else if (indexPath.row == 9) { //My Videos
//            cell.imageView.image = [UIImage imageNamed:@"Icon_My_Videos.png"];
//        } else if (indexPath.row == 10) { //Feedback
//            cell.imageView.image = [UIImage imageNamed:@"Icon_Feedback.png"];
//        } else if (indexPath.row == 11) { //Logout
//            cell.imageView.image = [UIImage imageNamed:@"Icon_Logout.png"];
//        }
        
    }
    
    cell.textLabel.text = [sideMenuArray objectAtIndex:indexPath.row];
    cell.selectionStyle=UITableViewCellSelectionStyleBlue;

    return cell;
}

#pragma mark -
#pragma mark - Seperator
- (UIImage *)separatorImage
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 4));
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 2));
    CGContextSetFillColorWithColor(context, [UIColor colorWithRed:79/255.0 green:79/255.0 blue:77/255.0 alpha:1].CGColor);
    CGContextFillRect(context, CGRectMake(0, 3, 1, 2));
    UIGraphicsPopContext();
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithCGImage:outputImage.CGImage scale:2.0 orientation:UIImageOrientationUp];
}

#pragma mark -
#pragma mark - UITableViewDelegate
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    cell.contentView.backgroundColor = [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:1];
//    cell.textLabel.textColor = [UIColor blackColor];
//}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

//    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
//    cell.contentView.backgroundColor=[UIColor colorWithRed:92/255.0 green:15/255.0 blue:46/255.0 alpha:1];
//    cell.textLabel.textColor = [UIColor whiteColor];
    
    DebugLog(@"row=%d",indexPath.row);
    if (currentIndex == indexPath.row) {
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        return;
    }
    
    preIndex = currentIndex;

    NSString *selected = [sideMenuArray objectAtIndex:indexPath.row];
    if ([selected isEqualToString:HOME]) {
        GIAViewController *homeController = [[GIAViewController alloc] initWithNibName:@"GIAViewController" bundle:nil];
        homeController.title = selected;
        NSArray *controllers = [NSArray arrayWithObject:homeController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
    }
    else if ([selected isEqualToString:ABOUT]) {
        masterdemoMasterViewController *masterViewController = [[masterdemoMasterViewController alloc] initWithNibName:@"masterdemoMasterViewController" bundle:nil];
        UINavigationController *masterNavigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
        
        masterdemoDetailViewController *detailViewController = [[masterdemoDetailViewController alloc] initWithNibName:@"masterdemoDetailViewController" bundle:nil];
        UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
        
        masterViewController.detailViewController = detailViewController;
        
        UISplitViewController *splitViewController = [[UISplitViewController alloc] init];
        splitViewController.delegate = detailViewController;
        splitViewController.viewControllers = @[masterNavigationController, detailNavigationController];
        splitViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        splitViewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
        NSArray *controllers = [NSArray arrayWithObject:splitViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:LAB_SERVICES]) {
        GIALabServicesMasterViewController *masterViewController = [[GIALabServicesMasterViewController alloc] initWithNibName:@"GIALabServicesMasterViewController" bundle:nil];
        masterViewController.title = LAB_SERVICES;
        UINavigationController *masterNavigationController = [[UINavigationController alloc] initWithRootViewController:masterViewController];
        
        GIALabServicesDetailViewController *detailViewController = [[GIALabServicesDetailViewController alloc] initWithNibName:@"GIALabServicesDetailViewController" bundle:nil];
        UINavigationController *detailNavigationController = [[UINavigationController alloc] initWithRootViewController:detailViewController];
        
        masterViewController.detailViewController = detailViewController;
        
        UISplitViewController *splitViewController = [[UISplitViewController alloc] init];
        splitViewController.delegate = detailViewController;
        splitViewController.viewControllers = @[masterNavigationController, detailNavigationController];
        splitViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        splitViewController.navigationController.navigationBar.tintColor = [UIColor blackColor];
        NSArray *controllers = [NSArray arrayWithObject:splitViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:POS]) {
        GIAPOSRequisitionViewController *posReqViewController = [[GIAPOSRequisitionViewController alloc] initWithNibName:@"GIAPOSRequisitionViewController" bundle:nil];
        posReqViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        NSArray *controllers = [NSArray arrayWithObject:posReqViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:COURSES]) {
        GIACoursesViewController *coursesViewController = [[GIACoursesViewController alloc] initWithNibName:@"GIACoursesViewController" bundle:nil];
        coursesViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        NSArray *controllers = [NSArray arrayWithObject:coursesViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:FEES]) {
        GIAFeesScheduleViewController *feesScheduleViewController = [[GIAFeesScheduleViewController alloc] initWithNibName:@"GIAFeesScheduleViewController" bundle:nil];
        feesScheduleViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        NSArray *controllers = [NSArray arrayWithObject:feesScheduleViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
//    else if ([selected isEqualToString:CLASS]) {
//        GIAClassScheduleViewController *classScheduleViewController = [[GIAClassScheduleViewController alloc] initWithNibName:@"GIAClassScheduleViewController" bundle:nil];
//        classScheduleViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
//        NSArray *controllers = [NSArray arrayWithObject:classScheduleViewController];
//        self.sideMenu.navigationController.viewControllers = controllers;
//        [self.sideMenu setMenuState:MFSideMenuStateClosed];
//        currentIndex = indexPath.row;
//    }
    else if ([selected isEqualToString:MEDIA]) {
        GIAMediaViewController *defaultViewController = [[GIAMediaViewController alloc] initWithNibName:@"GIAMediaViewController" bundle:nil];
        defaultViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        NSArray *controllers = [NSArray arrayWithObject:defaultViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:FEEDBACK]) {
        GIAFeedBackViewController *feedbackViewController = [[GIAFeedBackViewController alloc] initWithNibName:@"GIAFeedBackViewController" bundle:nil];
        feedbackViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        NSArray *controllers = [NSArray arrayWithObject:feedbackViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:REPRESENTATIVE]) {
        GIALocalRepViewController *representativeViewController = [[GIALocalRepViewController alloc] initWithNibName:@"GIALocalRepViewController" bundle:nil];
        representativeViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        NSArray *controllers = [NSArray arrayWithObject:representativeViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:CONTACT]) {
        GIAContactUSViewController *contactViewController = [[GIAContactUSViewController alloc] initWithNibName:@"GIAContactUSViewController" bundle:nil];
        contactViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        NSArray *controllers = [NSArray arrayWithObject:contactViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
    else if ([selected isEqualToString:LOGOUT]) {
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                          message:@"Are you sure you want to Logout of this app ?"
                                                         delegate:self
                                                cancelButtonTitle:@"NO"
                                                otherButtonTitles:@"YES",nil];
        [message show];
        DebugLog(@"Logout");
    }else if([selected isEqualToString:EXPORT])
    {
        ExportToCSVViewController *exportViewController = [[ExportToCSVViewController alloc] initWithNibName:@"ExportToCSVViewController" bundle:nil];
        exportViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
        NSArray *controllers = [NSArray arrayWithObject:exportViewController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.sideMenu setMenuState:MFSideMenuStateClosed];
        currentIndex = indexPath.row;
    }
//    else if ([selected isEqualToString:SERVICE]) {
//        GIARequestServiceViewController *defaultViewController = [[GIARequestServiceViewController alloc] initWithNibName:@"GIARequestServiceViewController" bundle:nil];
//        defaultViewController.title = [sideMenuArray objectAtIndex:indexPath.row];
//        NSArray *controllers = [NSArray arrayWithObject:defaultViewController];
//        self.sideMenu.navigationController.viewControllers = controllers;
//        [self.sideMenu setMenuState:MFSideMenuStateClosed];
//        currentIndex = indexPath.row;
//    }
    if(self.searchBar.isFirstResponder) [self.searchBar resignFirstResponder];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"NO"])
    {
        DebugLog(@"New User NO was selected.");
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:preIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
    else if([title isEqualToString:@"YES"])
    {
        currentIndex = 0;

        DebugLog(@" New User YES was selected.");
        GIAViewController *homeController = [[GIAViewController alloc] initWithNibName:@"GIAViewController" bundle:nil];
        homeController.title = @"Home";
        NSArray *controllers = [NSArray arrayWithObject:homeController];
        self.sideMenu.navigationController.viewControllers = controllers;
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentIndex inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
        [self clearData];
        
        if(NSNotFound != [sideMenuArray indexOfObject:EXPORT]) {
                [sideMenuArray removeObject:EXPORT];
            }
        [self.tableView reloadData];
    }
}

-(void)clearData
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"" forKey:LOGIN_FIRST_NAME];
    [defaults setObject:@"" forKey:LOGIN_LAST_NAME];
    [defaults setObject:@"" forKey:LOGIN_EMAIL];
    [defaults setObject:@"" forKey:LOGIN_NUMBER];
    [defaults setObject:@"" forKey:LOGIN_CITY];
    [defaults setObject:@"" forKey:LOGIN_PROFILE];
    [defaults setObject:@"" forKey:LOGIN_ADDRESS];
    [defaults synchronize];
}

#pragma mark -
#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [UIView beginAnimations:nil context:NULL];
    [self.sideMenu setMenuWidth:320.0f animated:NO];
    [self.searchBar layoutSubviews];
    [UIView commitAnimations];
    
    [self.searchBar setShowsCancelButton:YES animated:YES];
    
    self.sideMenu.panMode = MFSideMenuPanModeNone;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    [UIView beginAnimations:nil context:NULL];
    [self.sideMenu setMenuWidth:280.0f animated:NO];
    [self.searchBar layoutSubviews];
    [UIView commitAnimations];
    
    [self.searchBar setShowsCancelButton:NO animated:YES];
    
    self.sideMenu.panMode = MFSideMenuPanModeDefault;
}

-(void)handleNotificationResponse:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    BOOL showExport = [[dict objectForKey:EXPORT_KEY] boolValue];
    if(NSNotFound == [sideMenuArray indexOfObject:EXPORT]) {
        //DebugLog(@"not found");
        if (showExport) {
            //DebugLog(@"show jhalak = yes");
            [sideMenuArray insertObject:EXPORT atIndex:sideMenuArray.count-1];
        }
    }else{
        //DebugLog(@"found");
        if (!showExport) {
            //DebugLog(@"show jhalak = no");
            [sideMenuArray removeObject:EXPORT];
        }
    }
    [self.tableView reloadData];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
