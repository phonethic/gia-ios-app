//
//  GIARequestServiceViewController.h
//  GIA
//
//  Created by Rishi on 08/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface GIARequestServiceViewController : UIViewController <MFMailComposeViewControllerDelegate,UISplitViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn1;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn2;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn3;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn4;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn5;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn6;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn7;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn8;
@property (strong, nonatomic) IBOutlet UITextView *commenttextView;

- (IBAction)checkboxBtnPressed:(id)sender;
- (IBAction)submitBtnPressed:(id)sender;

@end
