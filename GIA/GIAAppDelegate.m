//
//  GIAAppDelegate.m
//  GIA
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIAAppDelegate.h"
#import "GIAViewController.h"
#import "MFSideMenu.h"
#import "SideMenuViewController.h"


@implementation GIAAppDelegate
@synthesize databaseName;
@synthesize databasePath;
@synthesize networkavailable;

- (GIAViewController *)giaController {
    return [[GIAViewController alloc] initWithNibName:@"GIAViewController" bundle:nil];
}

- (UINavigationController *)navigationController {
    return [[UINavigationController alloc]
            initWithRootViewController:[self giaController]];
}

- (MFSideMenu *)sideMenu
{
    SideMenuViewController *rightSideMenuController = [[SideMenuViewController alloc] init];
    UINavigationController *navigationController = [self navigationController];
    
    MFSideMenu *sideMenu = [MFSideMenu menuWithNavigationController:navigationController
                                             leftSideMenuController:nil
                                            rightSideMenuController:rightSideMenuController];
    sideMenu.menuWidth = 300;
    rightSideMenuController.sideMenu = sideMenu;
    
    return sideMenu;
}

- (void) setupNavigationControllerApp {
   // self.sideMenu.navigationController.navigationBar.tintColor = [UIColor blackColor];
    self.window.rootViewController = [self sideMenu].navigationController;
    [self.window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    //Add Flurry
    [Flurry startSession:FLURRY_APPID];
    
    //Add Parse
    [Parse setApplicationId:PARSE_APPID clientKey:PARSE_CLIENTKEY];
    
    [self startCheckNetwork];
    
    //Add SlideMenu
    [self setupNavigationControllerApp];    
    [self readAndCreateDatabase];
    
    return YES;
}

-(void)readAndCreateDatabase{
    // Setup some globals
	databaseName = @"gia.db";
    
	// Get the path to the documents directory and append the databaseName
	NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDir = [documentPaths objectAtIndex:0];
	databasePath = [documentsDir stringByAppendingPathComponent:databaseName];
    
    DebugLog(@"databasePath %@",databasePath);
	// Execute the "checkAndCreateDatabase" function
	[self checkAndCreateDatabase];
}
-(void) checkAndCreateDatabase{
	// Check if the SQL database has already been saved to the users phone, if not then copy it over
	BOOL success;
    
	// Create a FileManager object, we will use this to check the status
	// of the database and to copy it over if required
	NSFileManager *fileManager = [NSFileManager defaultManager];
    
	// Check if the database has already been created in the users filesystem
	success = [fileManager fileExistsAtPath:databasePath];
    
	// If the database already exists then return without doing anything
	if(success) return;
    
	// If not then proceed to copy the database from the application to the users filesystem
    
	// Get the path to the database in the application package
	NSString *databasePathFromApp = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:databaseName];
    
	// Copy the database from the package to the users filesystem
	[fileManager copyItemAtPath:databasePathFromApp toPath:databasePath error:nil];
    
}
#pragma Reachability Methods
-(void) startCheckNetwork
{
    
	[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kReachabilityChangedNotification object: nil];
	
    internetReach = [Reachability reachabilityForInternetConnection];
	[internetReach startNotifer];
    [self updateReachabitlityFlag:internetReach];
    
}

-(void) stopCheckNetwork
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
	internetReach = nil;
}

- (void)reachabilityChanged:(NSNotification *)note
{
	Reachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
	[self updateReachabilityStatus: curReach];
}

- (void) updateReachabilityStatus: (Reachability*) curReach
{
	if(curReach == internetReach)
	{
		[self updateReachabitlityFlag: curReach];
    }
}

- (void) updateReachabitlityFlag: (Reachability*) curReach
{
    NetworkStatus netStatus = [curReach currentReachabilityStatus];
    BOOL connectionRequired=[curReach connectionRequired];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            networkavailable =  NO;
            break;
        }
            
        case ReachableViaWWAN:
        {
			if(connectionRequired==NO)
			{
                networkavailable = YES;
			}
			else
			{
				//this is for invoking internet library
				NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"maps.google.com"] cachePolicy:NO timeoutInterval:15.0] ;
				
				NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
				if (theConnection) {
					connectionRequired	= NO;
				}
			}
			break;
        }
        case ReachableViaWiFi:
        {
            if(connectionRequired==NO)
            {
                networkavailable = YES;
            }
            break;
		}
    }
    if(connectionRequired)
    {
		networkavailable = NO;
    }
    DebugLog(@"network status = %d",networkavailable);
}


#pragma file handling

-(NSString *)getFilePath:(NSString *)fileName
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    //DebugLog(@"filename = %@" , fileName);
    return [documentsDirectory stringByAppendingPathComponent:fileName];
}
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lfilename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* fullPath = [self getFilePath:lfilename];
    // DebugLog(@"fullpath = %@" , fullPath);
    
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        [self removeFile:lfilename];
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    } else {
        [lcontent writeToFile:fullPath atomically:NO encoding:NSUTF8StringEncoding error:&error];
    }
}

-(void)removeFile:(NSString*)lfilename {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString* fullPath = [self getFilePath:lfilename];

    BOOL success = [fileManager removeItemAtPath: fullPath error:NULL];
    if(success == YES)
        DebugLog(@" %@ removed",lfilename);
    else
        DebugLog(@" %@ NOT removed",lfilename);
}

-(NSString *) getTextFromFile:(NSString *)lfilename
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
     NSString* fullPath = [self getFilePath:lfilename];
    DebugLog(@"fullpath = %@" , fullPath);
    NSError* error;
    if ([fileManager fileExistsAtPath:fullPath])
    {
        NSString *data = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
        return data;
    } else {
        return @"";
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
