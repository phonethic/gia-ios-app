//
//  GIAFeedBackViewController.m
//  GIA
//
//  Created by Kirti Nikam on 17/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIAFeedBackViewController.h"
#import "constants.h"
#import "MFSideMenu.h"
#import "GIAAppDelegate.h"
#import "MBProgressHUD.h"

@interface GIAFeedBackViewController ()
@property (nonatomic, retain) MBProgressHUD *progressHUD;
@end
@implementation NSURLRequest (NSURLRequestWithIgnoreSSL)

+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}

@end

@implementation GIAFeedBackViewController
@synthesize lblHeader,lblEducation,lblHR,lblLaboratoryEd,lblLaboratory;
@synthesize btnEducation,btnHR,btnLaboratoryEd,btnLaboratory;
@synthesize submitBtn;
@synthesize lblCourseTypeHeader,lblOffCampus,lblOnCampus;
@synthesize btnMumbaiCampus,btnOtherCampus;
@synthesize progressHUD;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [Flurry logEvent:@"Tab_Feedback"];
    
    [self setupMenuBarButtonItems];
    [self changeLabelProperties:lblHeader];
    [self changeLabelProperties:lblEducation];
    [self changeLabelProperties:lblHR];
    [self changeLabelProperties:lblLaboratoryEd];
    [self changeLabelProperties:lblLaboratory];

    [self changeLabelProperties:lblCourseTypeHeader];
    [self changeLabelProperties:lblOnCampus];
    [self changeLabelProperties:lblOffCampus];
    
    submitBtn.titleLabel.font    = TAHOMA_FONT(25);
}
-(void)changeLabelProperties:(UILabel *)lbl
{
    lbl.backgroundColor = [UIColor clearColor];
    lbl.numberOfLines = 2;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.textAlignment = UITextAlignmentLeft;
    if ([lbl isEqual:lblHeader] || [lbl isEqual:lblCourseTypeHeader]) {
        lbl.font   = TAHOMA_FONT(25);
        lbl.textColor   = DEFAULT_COLOR_SELECTION;
        lbl.shadowColor = [UIColor blackColor];
        lbl.shadowOffset = CGSizeMake(0, -1);
    }else{
        lbl.font = TAHOMA_FONT(18);
        lbl.textColor = [UIColor blackColor];
    }
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSubmitBtn:nil];
    [self setLblHeader:nil];
    [self setLblEducation:nil];
    [self setLblHR:nil];
    [self setLblLaboratoryEd:nil];
    [self setLblLaboratory:nil];
    [self setBtnEducation:nil];
    [self setBtnHR:nil];
    [self setBtnLaboratoryEd:nil];
    [self setBtnLaboratory:nil];
    [self setBtnMumbaiCampus:nil];
    [self setBtnOtherCampus:nil];
    [self setLblCourseTypeHeader:nil];
    [self setLblOnCampus:nil];
    [self setLblOffCampus:nil];
    [self setSubmitBtn:nil];
    [super viewDidUnload];
}
- (IBAction)radioBtnClicked:(UIButton *)button {
    DebugLog(@"%d",button.tag);
    UIButton *but;
    for (int index = 101;index < 105;index ++) {
        but = (UIButton *)[self.view viewWithTag:index];
        if ([but isKindOfClass:[UIButton class]] && but.tag!=button.tag) {
            [but setSelected:NO];
        }
    }
    if (!button.selected) {
        button.selected = !button.selected;
    }
}
- (IBAction)campusRadioBtnClicked:(UIButton *)button {
    DebugLog(@"%d",button.tag);
    UIButton *but;
    for (int index = 201;index < 203;index ++) {
        but = (UIButton *)[self.view viewWithTag:index];
        if ([but isKindOfClass:[UIButton class]] && but.tag!=button.tag) {
            [but setSelected:NO];
        }
    }
    if (!button.selected) {
        button.selected = !button.selected;
    }
}
- (IBAction)submitBtnClicked:(id)sender {
    DebugLog(@"networkavailable %d",[GIA_APP_DELEGATE networkavailable]);
    if ([GIA_APP_DELEGATE networkavailable])
    {
        [self sendHttpRequest];
    }
    else
    {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:@"No Network Connection"
                                  message:@"Please check your internet connection and try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
}
- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.progressHUD.labelText = message;
    self.progressHUD.labelFont = TAHOMA_FONT(20);
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.view bringSubviewToFront:self.progressHUD];
    [self.progressHUD show:YES];
}
-(void)hideProgressHUD:(BOOL)animated
{
    [self.progressHUD hide:animated];
}
-(void)sendHttpRequest{
    
    isqueryTypeSelected = NO;
    int value = 56;
    if ([btnEducation isSelected]) {
        value = 56;
        isqueryTypeSelected = YES;
    }else if ([btnHR isSelected]) {
        value = 57;
        isqueryTypeSelected = YES;
    }else if ([btnLaboratoryEd isSelected]) {
        value = 58;
        isqueryTypeSelected = YES;
    }else if ([btnLaboratory isSelected]) {
        value = 72;
        isqueryTypeSelected = YES;
    }
    
    if (!isqueryTypeSelected) {
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"Please select type of query"
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        return;
    }
    
    
    iscourseTypeSelected = NO;
    NSString *coursetypeValue = @"On Campus - Mumbai";
    if ([btnMumbaiCampus isSelected]) {
        coursetypeValue = @"On Campus - Mumbai";
        iscourseTypeSelected = YES;
    }else if ([btnOtherCampus isSelected]) {
        coursetypeValue = @"Off Campus - Other Cities";
        iscourseTypeSelected = YES;
    }
    
//    if (!iscourseTypeSelected) {
//        UIAlertView *errorView = [[UIAlertView alloc]
//                                  initWithTitle:ALERT_TITLE
//                                  message:@"Please select type of course"
//                                  delegate:self
//                                  cancelButtonTitle:@"OK"
//                                  otherButtonTitles:nil];
//        [errorView show];
//        return;
//    }
    
    [self showProgressHUDWithMessage:@"Proccesing"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *firstName      = [defaults objectForKey:LOGIN_FIRST_NAME];
    NSString *lastName       = [defaults objectForKey:LOGIN_LAST_NAME];
    NSString *phoneNumber    = [defaults objectForKey:LOGIN_NUMBER];
    NSString *emailAddress   = [defaults objectForKey:LOGIN_EMAIL];
    NSString *address        = [defaults objectForKey:LOGIN_ADDRESS];
    NSString *city           = [defaults objectForKey:LOGIN_CITY];
    DebugLog(@"%@ %@ %@ %@ %@ -%@-",firstName,lastName,phoneNumber,emailAddress,address,city);
    
    /*
     00N30000005ZmzL	India
     00N30000005Zn0J
     00N30000005Zn0K
     00N30000005Zn0M	Friend
     00N30000005Zn0Z
     00N30000005Zn0e	Web
     00N30000005Zn0f	Mumbai
     00N30000005Zn0h
     00N30000005Zn0j
     00N30000005Zn0k	Indian
     00N30000005bgsP
     00N30000007Q4FK	1
     00N30000008BJAP	Off Campus - Other Cities
     00Na0000009zAc1
     URL
     company
     currency	INR
     email	kirti@phonethics.in
     first_name	Kirti
     last_name	Nikam
     mobile	123456789
     oid	00D30000000muZZ
     phone	
     retURL	http://www.giaindia.in/gia/Web2Lead_India.html?id=b
     
     //eg.    NSString *encodedUrlString = @"oid=00D30000000muZZ&retURL=http%3A%2F%2Fwww.giaindia.in%2Fgia%2FWeb2Lead_India.html%3Fid%3Db&company=&first_name=Kirti&last_name=Nikam&00N30000005Zn0Z=&phone=&mobile=123456789&email=kirti%40phonethics.in&00N30000005Zn0J=&URL=&00N30000005Zn0M=Friend&00N30000005Zn0k=Indian&00N30000005Zn0f=Mumbai&00N30000005Zn0K=&00N30000005bgsP=&00N30000005Zn0j=&00N30000005Zn0h=&00N30000008BJAP=Off+Campus+-+Other+Cities&00Na0000009zAc1=&00N30000007Q4FK=1&00N30000005Zn0e=Web&00N30000005ZmzL=India&currency=INR";
     
     */
    
//    NSString *urlString = @"00N30000005ZmzL=India&00N30000005Zn0J=&00N30000005Zn0K=&00N30000005Zn0M=ipad&00N30000005Zn0Z=&00N30000005Zn0e=Web&00N30000005Zn0f=Mumbai&00N30000005Zn0h=&00N30000005Zn0j=&00N30000005Zn0k=Indian&00N30000005bgsP=&00N30000007Q4FK=1&00N30000008BJAP=Off Campus - Other Cities&00Na0000009zAc1=&URL=&company=&currency=INR&email=kirti@phonethics.in&first_name=Kirti&last_name=Nikam&mobile=123456789&oid=00D30000000muZZ&phone=&retURL=http://www.giaindia.in/gia/Web2Lead_India.html?id=b";
    
    NSString *urlString = [NSString stringWithFormat:@"00N30000005ZmzL=India&00N30000005Zn0J=&00N30000005Zn0K=&00N30000005Zn0M=ipad&00N30000005Zn0Z=&00N30000005Zn0e=Web&00N30000005Zn0f=%@&00N30000005Zn0h=&00N30000005Zn0j=&00N30000005Zn0k=Indian&00N30000005bgsP=&00N30000007Q4FK=1&00N30000008BJAP=%@ Cities&00Na0000009zAc1=&URL=&company=&currency=INR&email=%@&first_name=%@&last_name=%@&mobile=%@&oid=00D30000000muZZ&phone=&retURL=http://www.giaindia.in/gia/Web2Lead_India.html?id=b",city,coursetypeValue,emailAddress,firstName,lastName,phoneNumber];
    
    NSString *encodedUrlString = [self URLEncode:urlString];
    DebugLog(@"-%@-",encodedUrlString);
    
//    NSMutableData *postData = [[NSMutableData alloc] init];
//    [postData appendData:[encodedUrlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURL *url = [NSURL URLWithString:@"https://www.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8"];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request setHTTPMethod:@"POST"];
    
   // [request setValue:[NSString stringWithFormat:@"%d", postData.length] forHTTPHeaderField:@"Content-Length"];
   // [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSData *requestData = [NSData dataWithBytes:[encodedUrlString UTF8String] length:[encodedUrlString length]];

    [request setHTTPBody:requestData];
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];  

}

-(NSString*)URLEncode:(NSString*) string
{
    ////DebugLog(@"==BEFORE ENCODE=%@",string);
    NSMutableString *escaped = [[string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] mutableCopy];
//    [escaped replaceOccurrencesOfString:@"&" withString:@"%26" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"+" withString:@"%2B" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"," withString:@"%2C" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"/" withString:@"%2F" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@":" withString:@"%3A" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@";" withString:@"%3B" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
//    [escaped replaceOccurrencesOfString:@"=" withString:@"%3D" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"?" withString:@"%3F" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"@" withString:@"%40" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"\t" withString:@"%09" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"#" withString:@"%23" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"<" withString:@"%3C" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@">" withString:@"%3E" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"\"" withString:@"%22" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    [escaped replaceOccurrencesOfString:@"\n" withString:@"%0A" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [escaped length])];
    ////DebugLog(@"==AFTER ENCODE=%@",escaped);
    return escaped;
}

#pragma ssl bypass
// Adding this to allow untrusted certificate
- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

// Adding this to allow untrusted certificate
- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        //        if (... user allows connection despite bad certificate ...)
        [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	
	if(responseAsyncData==nil)
	{
		responseAsyncData = [[NSMutableData alloc] initWithLength:0];
	}
    
	[responseAsyncData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    DebugLog(@"didFailWithError: %@", [error localizedDescription]);
    [self connectionDidFinishLoading:nil];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)localconnection
{
    DebugLog(@"connectionDidFinishLoading responseAsyncData %@",responseAsyncData);
    [self hideProgressHUD:YES];
	if(responseAsyncData)
	{
		NSString *result = [[NSString alloc] initWithData:responseAsyncData encoding:NSASCIIStringEncoding];
		DebugLog(@"\n result:%@\n\n", result);
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"Thank you for contacting GIA,\nOur representative will get in touch with you."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
        result = nil;
        self->responseAsyncData = nil;
        connection = nil;
        [Flurry logEvent:@"Feedback_Send"];
    }else{
        UIAlertView *errorView = [[UIAlertView alloc]
                                  initWithTitle:ALERT_TITLE
                                  message:@"Please, try again."
                                  delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [errorView show];
    }
}

@end
