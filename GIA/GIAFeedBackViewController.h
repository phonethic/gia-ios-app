//
//  GIAFeedBackViewController.h
//  GIA
//
//  Created by Kirti Nikam on 17/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GIAFeedBackViewController : UIViewController{
    NSURLConnection *connection;
    NSMutableData *responseAsyncData;
    
    BOOL iscourseTypeSelected;
    BOOL isqueryTypeSelected;
}
@property (strong, nonatomic) IBOutlet UILabel *lblHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblEducation;
@property (strong, nonatomic) IBOutlet UILabel *lblHR;
@property (strong, nonatomic) IBOutlet UILabel *lblLaboratoryEd;
@property (strong, nonatomic) IBOutlet UILabel *lblLaboratory;
@property (strong, nonatomic) IBOutlet UIButton *btnEducation;
@property (strong, nonatomic) IBOutlet UIButton *btnHR;
@property (strong, nonatomic) IBOutlet UIButton *btnLaboratoryEd;
@property (strong, nonatomic) IBOutlet UIButton *btnLaboratory;
@property (strong, nonatomic) IBOutlet UIButton *btnMumbaiCampus;
@property (strong, nonatomic) IBOutlet UIButton *btnOtherCampus;
@property (strong, nonatomic) IBOutlet UILabel *lblCourseTypeHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblOnCampus;
@property (strong, nonatomic) IBOutlet UILabel *lblOffCampus;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;

- (IBAction)submitBtnClicked:(id)sender;
- (IBAction)radioBtnClicked:(id)sender;
- (IBAction)campusRadioBtnClicked:(id)sender;

@end
