//
//  GIACoursesViewController.m
//  GIA
//
//  Created by Kirti Nikam on 03/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIACoursesViewController.h"
#import "MFSideMenu.h"
#import "GIAAppDelegate.h"
#import "CKCalendarView.h"
#import <QuartzCore/QuartzCore.h>
#import "GIACourseDetailsViewController.h"
#import "CourseObject.h"

@interface GIACoursesViewController ()  <CKCalendarDelegate>
@property(nonatomic, weak) CKCalendarView *calendar;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) NSArray *disabledDates;
@end

@implementation GIACoursesViewController
@synthesize regionBtn,regionTableView;
@synthesize zoneBtn,zoneTableView;
@synthesize stateBtn,stateTableView;
@synthesize cityBtn,cityTableView;
@synthesize regionArray,zoneArray,stateArray,cityArray;
@synthesize selectedRegion,selectedZone,selectedState,selectedCity;
@synthesize courseBtn,courseArray,courseTableView,selectedCourse;
@synthesize previousControl;
@synthesize calendar,dateFormatter,minimumDate,disabledDates;
@synthesize datesArray,courseListTableView,courseListArray;
@synthesize resetBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [Flurry logEvent:@"Tab_Courses_Schedule"];
    
    [self setBtnProperties:regionBtn];
    [self setBtnProperties:zoneBtn];
    [self setBtnProperties:cityBtn];
    [self setBtnProperties:courseBtn];
    [self setBtnProperties:resetBtn];

    [self setTableBackgroundView:regionTableView];
    [self setTableBackgroundView:zoneTableView];
    [self setTableBackgroundView:cityTableView];
    [self setTableBackgroundView:courseTableView];
    [self setTableBackgroundView:courseListTableView];
    courseListTableView.layer.cornerRadius  = 1.0;

    [self setupMenuBarButtonItems];
    [self setupCalendarItems];
    [self resetBtnClicked:nil];
}
-(void)setBtnProperties:(UIButton *)btn
{
    btn.titleLabel.font    = TAHOMA_FONT(20);
    btn.titleLabel.lineBreakMode  = NSLineBreakByTruncatingTail;
}
-(void)setTableBackgroundView:(UITableView *)tableView
{
    tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"citydropdownbg.png"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (void)viewDidUnload {
    [self setRegionBtn:nil];
    [self setZoneBtn:nil];
    [self setStateBtn:nil];
    [self setCityBtn:nil];
    [self setRegionTableView:nil];
    [self setZoneTableView:nil];
    [self setStateTableView:nil];
    [self setCityTableView:nil];
    [self setResetBtn:nil];
    [self setCourseBtn:nil];
    [self setCourseTableView:nil];
    [self setCourseListTableView:nil];
    [super viewDidUnload];
}
- (IBAction)resetBtnClicked:(id)sender {
    selectedRegion  = @"";
    selectedZone    = @"";
   // selectedState = @"";
    selectedCity    = @"";
    selectedCourse  = @"";
    previousControl = @"";
    
    [self setBtnTitle:regionBtn text:@"Select Region"];
    [self setBtnTitle:zoneBtn text:@"Select Zone"];
//    [self setBtnTitle:stateBtn text:@"Select State"];
    [self setBtnTitle:cityBtn text:@"Select City"];
    [self setBtnTitle:courseBtn text:@"Select Course"];
    
    [self reloadRegionArrayAndTableView];
    [self reloadZoneArrayAndTableView];
    //[self reloadStateArrayAndTableView];
    [self reloadCityArrayAndTableView];
    [self reloadCourseArrayAndTableView];
    [self reloadCalendarView];
    courseListTableView.hidden = YES;
}

-(void)pullDownAnimation:(UITableView *)tableView btn:(UIButton *)lbtn
{
    [tableView reloadData];
    regionBtn.userInteractionEnabled    = FALSE;
    zoneBtn.userInteractionEnabled      = FALSE;
    cityBtn.userInteractionEnabled      = FALSE;
    courseBtn.userInteractionEnabled    = FALSE;
    
    regionTableView.userInteractionEnabled    = FALSE;
    zoneTableView.userInteractionEnabled      = FALSE;
    cityTableView.userInteractionEnabled      = FALSE;
    courseTableView.userInteractionEnabled    = FALSE;
    
    double Height = 230;
    [UIView  transitionWithView:tableView duration:1  options:UIViewAnimationOptionTransitionNone
                     animations:^(void) {
                         tableView.alpha = 1.0;
                         tableView.frame = CGRectMake(tableView.frame.origin.x,CGRectGetMaxY(lbtn.frame), tableView.frame.size.width,Height);
                         if (![tableView isEqual:courseTableView]) {
                             if ([tableView isEqual:regionTableView]) {
                                 zoneBtn.frame = CGRectMake(zoneBtn.frame.origin.x,CGRectGetMaxY(regionTableView.frame)+10, zoneBtn.frame.size.width,zoneBtn.frame.size.height);
                                 zoneTableView.frame = CGRectMake(zoneTableView.frame.origin.x,CGRectGetMaxY(zoneBtn.frame), zoneTableView.frame.size.width,zoneTableView.frame.size.height);
                                 
                                 cityBtn.frame = CGRectMake(cityBtn.frame.origin.x,CGRectGetMaxY(zoneTableView.frame)+10, cityBtn.frame.size.width,cityBtn.frame.size.height);
                                 cityTableView.frame = CGRectMake(cityTableView.frame.origin.x,CGRectGetMaxY(cityBtn.frame), cityTableView.frame.size.width,cityTableView.frame.size.height);
                             }
                             else if ([tableView isEqual:zoneTableView]){
                                 
                                 cityBtn.frame = CGRectMake(cityBtn.frame.origin.x,CGRectGetMaxY(zoneTableView.frame)+10, cityBtn.frame.size.width,cityBtn.frame.size.height);
                                 cityTableView.frame = CGRectMake(cityTableView.frame.origin.x,CGRectGetMaxY(cityBtn.frame), cityTableView.frame.size.width,cityTableView.frame.size.height);
                                 
                             }
                             courseBtn.frame = CGRectMake(courseBtn.frame.origin.x,CGRectGetMaxY(cityTableView.frame)+10, courseBtn.frame.size.width,courseBtn.frame.size.height);
                             courseTableView.frame = CGRectMake(courseTableView.frame.origin.x,CGRectGetMaxY(courseBtn.frame), courseTableView.frame.size.width,courseTableView.frame.size.height);
                         }
                     }
                     completion:^(BOOL finished) {
                         [tableView reloadData];
                         
                         regionBtn.userInteractionEnabled   = TRUE;
                         zoneBtn.userInteractionEnabled     = TRUE;
                         cityBtn.userInteractionEnabled     = TRUE;
                         courseBtn.userInteractionEnabled   = TRUE;
                         
                         regionTableView.userInteractionEnabled    = TRUE;
                         zoneTableView.userInteractionEnabled      = TRUE;
                         cityTableView.userInteractionEnabled      = TRUE;
                         courseTableView.userInteractionEnabled    = TRUE;
                         [lbtn setBackgroundImage:[UIImage imageNamed:@"titlebutton_pressed.png"] forState:UIControlStateNormal];
                         lbtn.selected = TRUE;
                     }];

}
-(void)pullUpAnimation:(UITableView *)tableView btn:(UIButton *)lbtn
{
    regionBtn.userInteractionEnabled    = FALSE;
    zoneBtn.userInteractionEnabled      = FALSE;
    cityBtn.userInteractionEnabled      = FALSE;
    courseBtn.userInteractionEnabled    = FALSE;
    
    regionTableView.userInteractionEnabled    = FALSE;
    zoneTableView.userInteractionEnabled      = FALSE;
    cityTableView.userInteractionEnabled      = FALSE;
    courseTableView.userInteractionEnabled    = FALSE;
    
    [UIView  transitionWithView:tableView duration:1  options:UIViewAnimationOptionTransitionNone
                     animations:^(void) {
                         tableView.frame = CGRectMake(tableView.frame.origin.x,CGRectGetMaxY(lbtn.frame), tableView.frame.size.width,0);
                         if (![tableView isEqual:courseTableView]) {
                             if ([tableView isEqual:regionTableView]) {
                                 zoneBtn.frame = CGRectMake(zoneBtn.frame.origin.x,CGRectGetMaxY(regionTableView.frame)+10, zoneBtn.frame.size.width,zoneBtn.frame.size.height);
                                 zoneTableView.frame = CGRectMake(zoneTableView.frame.origin.x,CGRectGetMaxY(zoneBtn.frame), zoneTableView.frame.size.width,zoneTableView.frame.size.height);
                                 
                                 cityBtn.frame = CGRectMake(cityBtn.frame.origin.x,CGRectGetMaxY(zoneTableView.frame)+10, cityBtn.frame.size.width,cityBtn.frame.size.height);
                                 cityTableView.frame = CGRectMake(cityTableView.frame.origin.x,CGRectGetMaxY(cityBtn.frame), cityTableView.frame.size.width,cityTableView.frame.size.height);
                             }
                             else if ([tableView isEqual:zoneTableView]){
                                 
                                 cityBtn.frame = CGRectMake(cityBtn.frame.origin.x,CGRectGetMaxY(zoneTableView.frame)+10, cityBtn.frame.size.width,cityBtn.frame.size.height);
                                 cityTableView.frame = CGRectMake(cityTableView.frame.origin.x,CGRectGetMaxY(cityBtn.frame), cityTableView.frame.size.width,cityTableView.frame.size.height);
                                 
                             }
                             courseBtn.frame = CGRectMake(courseBtn.frame.origin.x,CGRectGetMaxY(cityTableView.frame)+10, courseBtn.frame.size.width,courseBtn.frame.size.height);
                             courseTableView.frame = CGRectMake(courseTableView.frame.origin.x,CGRectGetMaxY(courseBtn.frame), courseTableView.frame.size.width,courseTableView.frame.size.height);
                         }
                     }
                     completion:^(BOOL finished) {
                         tableView.alpha = 0.0;
                         regionBtn.userInteractionEnabled   = TRUE;
                         zoneBtn.userInteractionEnabled     = TRUE;
                         cityBtn.userInteractionEnabled     = TRUE;
                         courseBtn.userInteractionEnabled   = TRUE;
                         
                         regionTableView.userInteractionEnabled    = TRUE;
                         zoneTableView.userInteractionEnabled      = TRUE;
                         cityTableView.userInteractionEnabled      = TRUE;
                         courseTableView.userInteractionEnabled    = TRUE;
                         [lbtn setBackgroundImage: [UIImage imageNamed:@"titlebutton_unpressed.png"] forState:UIControlStateNormal];
                         lbtn.selected = FALSE;
                     }];
}

- (IBAction)regionBtnClicked:(id)sender {
    
    if (regionBtn.isSelected)  { //already opened
        //close
        [self pullUpAnimation:regionTableView btn:regionBtn];
        previousControl = @"";
    }else
    {
            //open
        if (![previousControl isEqualToString:@"region"] && ![previousControl isEqualToString:@""]) {
            [self closePreviousDropDown];
            NSMutableArray *passarray = [[NSMutableArray alloc]init];
            [passarray addObject:regionTableView];
            [passarray addObject:sender];
            [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                       withObject:passarray
                       afterDelay:1.0];
        }else{
            [self pullDownAnimation:regionTableView btn:regionBtn];
        }
        previousControl = @"region";
    }
}

- (IBAction)zoneBtnClicked:(id)sender {
    if (zoneBtn.isSelected)  { //already opened
        //close
        [self pullUpAnimation:zoneTableView btn:zoneBtn];
        previousControl = @"";
    }else
    {
        //open
        if (![previousControl isEqualToString:@"zone"] && ![previousControl isEqualToString:@""]) {
            [self closePreviousDropDown];
            NSMutableArray *passarray = [[NSMutableArray alloc]init];
            [passarray addObject:zoneTableView];
            [passarray addObject:sender];
            [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                       withObject:passarray
                       afterDelay:1.0];
        }else{
            [self pullDownAnimation:zoneTableView btn:zoneBtn];
        }
        previousControl = @"zone";
    }
}

- (IBAction)stateBtbClicked:(id)sender {
}

- (IBAction)cityBtnClicked:(id)sender {
    if (cityBtn.isSelected)  { //already opened
        //close
        [self pullUpAnimation:cityTableView btn:cityBtn];
        previousControl = @"";
    }else
    {
        //open
        if (![previousControl isEqualToString:@"city"] && ![previousControl isEqualToString:@""]) {
            [self closePreviousDropDown];
            NSMutableArray *passarray = [[NSMutableArray alloc]init];
            [passarray addObject:cityTableView];
            [passarray addObject:sender];
            [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                       withObject:passarray
                       afterDelay:1.0];
        }else{
            [self pullDownAnimation:cityTableView btn:cityBtn];
        }
        previousControl = @"city";
    }
}
- (IBAction)courseBtnClicked:(id)sender {
    if (courseBtn.isSelected)  { //already opened
        //close
        [self pullUpAnimation:courseTableView btn:courseBtn];
        previousControl = @"";
    }else
    {
        //open
        if (![previousControl isEqualToString:@"course"] && ![previousControl isEqualToString:@""]) {
            [self closePreviousDropDown];
            NSMutableArray *passarray = [[NSMutableArray alloc]init];
            [passarray addObject:courseTableView];
            [passarray addObject:sender];
            [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                       withObject:passarray
                       afterDelay:1.0];
        }else{
            [self pullDownAnimation:courseTableView btn:courseBtn];
        }
        previousControl = @"course";
    }
}

-(void)tableCellSelected:(NSString *)selected
{
    if ([selected isEqualToString:@"region"]) {
        if (zoneBtn.isSelected) {
            DebugLog(@"zone already opened");
        }
        else{
            [self closePreviousDropDown];
            NSMutableArray *passarray = [[NSMutableArray alloc]init];
            [passarray addObject:zoneTableView];
            [passarray addObject:zoneBtn];
            [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                       withObject:passarray
                       afterDelay:1.0];
            previousControl = @"zone";
        }
    }else if ([selected isEqualToString:@"zone"]) {
        if (cityBtn.isSelected) {
            DebugLog(@"city already opened");
        }
        else{
            [self closePreviousDropDown];
            NSMutableArray *passarray = [[NSMutableArray alloc]init];
            [passarray addObject:cityTableView];
            [passarray addObject:cityBtn];
            [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                       withObject:passarray
                       afterDelay:1.0];
            previousControl = @"city";
        }
    }else if ([selected isEqualToString:@"city"]) {
        if (courseBtn.isSelected) {
            DebugLog(@"course already opened");
        }
        else{
            [self closePreviousDropDown];
            NSMutableArray *passarray = [[NSMutableArray alloc]init];
            [passarray addObject:courseTableView];
            [passarray addObject:courseBtn];
            [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
                       withObject:passarray
                       afterDelay:1.0];
            previousControl = @"course";
        }
    }else if ([selected isEqualToString:@"course"]) {
        [self pullUpAnimation:courseTableView btn:courseBtn];
        previousControl = @"";
    }
}

-(void)closePreviousDropDown
{
//    if ([previousControl isEqualToString:@"region"]) {
//        [self pullUpDownAnimation:regionTableView btnstatus:regionBtnStatus btn:regionBtn];
//        regionBtnStatus = NO;
//    }else if ([previousControl isEqualToString:@"zone"]) {
//        [self pullUpDownAnimation:zoneTableView btnstatus:zoneBtnStatus btn:zoneBtn];
//        zoneBtnStatus = NO;
//    }else if ([previousControl isEqualToString:@"state"]) {
//        [self pullUpDownAnimation:stateTableView btnstatus:stateBtnStatus btn:stateBtn];
//        stateBtnStatus = NO;
//    }else if ([previousControl isEqualToString:@"city"]) {
//        [self pullUpDownAnimation:cityTableView btnstatus:cityBtnStatus btn:cityBtn];
//        cityBtnStatus = NO;
//    }else if ([previousControl isEqualToString:@"course"]) {
//        [self pullUpDownAnimation:courseTableView btnstatus:courseBtnStatus btn:courseBtn];
//        courseBtnStatus = NO;
//    }
    
    if ([previousControl isEqualToString:@"region"]) {
        [self pullUpAnimation:regionTableView btn:regionBtn];
        previousControl = @"";
    }else if ([previousControl isEqualToString:@"zone"]) {
        [self pullUpAnimation:zoneTableView btn:zoneBtn];
        previousControl = @"";
    }else if ([previousControl isEqualToString:@"state"]) {
        [self pullUpAnimation:stateTableView btn:stateBtn];
        previousControl = @"";
    }else if ([previousControl isEqualToString:@"city"]) {
        [self pullUpAnimation:cityTableView btn:cityBtn];
        previousControl = @"";
    }else if ([previousControl isEqualToString:@"course"]) {
        [self pullUpAnimation:courseTableView btn:courseBtn];
        previousControl = @"";
    }
}
-(void)delayAndDoRandomAnimationFunction:(NSMutableArray *)array
{
   /* NSMutableArray *passarray = [[NSMutableArray alloc]init];
    [passarray addObject:courseTableView];
    [passarray addObject:[NSNumber numberWithInt:courseBtnStatus]];
    [passarray addObject:sender];
    [self performSelector:@selector(delayAndDoRandomAnimationFunction:)
               withObject:passarray
               afterDelay:1.0];*/
    UITableView *tableView   = [array objectAtIndex:0];
    UIButton *btnSender = [array objectAtIndex:1];
    [self pullDownAnimation:tableView btn:btnSender];
    //[self pullUpDownAnimation:tableView btnstatus:toggle btn:btnSender];
}
-(void)pullUpDownAnimation:(UITableView *)tableView btnstatus:(BOOL)btnStatus btn:(UIButton *)lbtn
{
    
    regionBtn.userInteractionEnabled    = FALSE;
    zoneBtn.userInteractionEnabled      = FALSE;
    cityBtn.userInteractionEnabled      = FALSE;
    courseBtn.userInteractionEnabled    = FALSE;
    
    regionTableView.userInteractionEnabled    = FALSE;
    zoneTableView.userInteractionEnabled      = FALSE;
    cityTableView.userInteractionEnabled      = FALSE;
    courseTableView.userInteractionEnabled    = FALSE;
    
    double Height = 250;
    if (btnStatus){
        [UIView  transitionWithView:tableView duration:1  options:UIViewAnimationOptionTransitionNone
                         animations:^(void) {
                             tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y, tableView.frame.size.width,0);
                             if (![tableView isEqual:courseTableView]) {
                                 if ([tableView isEqual:regionTableView]) {
                                     zoneBtn.frame = CGRectMake(zoneBtn.frame.origin.x,zoneBtn.frame.origin.y-Height, zoneBtn.frame.size.width,zoneBtn.frame.size.height);
                                     zoneTableView.frame = CGRectMake(zoneTableView.frame.origin.x,CGRectGetMaxY(zoneBtn.frame), zoneTableView.frame.size.width,zoneTableView.frame.size.height);
                                     
                                     cityBtn.frame = CGRectMake(cityBtn.frame.origin.x,cityBtn.frame.origin.y-Height, cityBtn.frame.size.width,cityBtn.frame.size.height);
                                     cityTableView.frame = CGRectMake(cityTableView.frame.origin.x,CGRectGetMaxY(cityBtn.frame), cityTableView.frame.size.width,cityTableView.frame.size.height);
                                 }
                                 else if ([tableView isEqual:zoneTableView]){
                                   
                                     cityBtn.frame = CGRectMake(cityBtn.frame.origin.x,cityBtn.frame.origin.y-Height, cityBtn.frame.size.width,cityBtn.frame.size.height);
                                     cityTableView.frame = CGRectMake(cityTableView.frame.origin.x,CGRectGetMaxY(cityBtn.frame), cityTableView.frame.size.width,cityTableView.frame.size.height);
                                     
                                 }
                                 courseBtn.frame = CGRectMake(courseBtn.frame.origin.x,courseBtn.frame.origin.y-Height, courseBtn.frame.size.width,courseBtn.frame.size.height);
                                 courseTableView.frame = CGRectMake(courseTableView.frame.origin.x,CGRectGetMaxY(courseBtn.frame), courseTableView.frame.size.width,courseTableView.frame.size.height);
                             }
                         }
                         completion:^(BOOL finished) {
                             tableView.alpha = 0.0;
                             regionBtn.userInteractionEnabled   = TRUE;
                             zoneBtn.userInteractionEnabled     = TRUE;
                             cityBtn.userInteractionEnabled     = TRUE;
                             courseBtn.userInteractionEnabled   = TRUE;
                             
                             regionTableView.userInteractionEnabled    = TRUE;
                             zoneTableView.userInteractionEnabled      = TRUE;
                             cityTableView.userInteractionEnabled      = TRUE;
                             courseTableView.userInteractionEnabled    = TRUE;
                             [lbtn setBackgroundImage: [UIImage imageNamed:@"titlebutton_unpressed.png"] forState:UIControlStateNormal];
                         }];
    }else{
        //  DebugLog(@"y : %f",tableView.frame.origin.y-tableView.frame.size.height);
        [UIView  transitionWithView:tableView duration:1  options:UIViewAnimationOptionTransitionNone
                         animations:^(void) {
                             tableView.alpha = 1.0;
                             tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y, tableView.frame.size.width,Height);
                             if (![tableView isEqual:courseTableView]) {
                                 if ([tableView isEqual:regionTableView]) {
                                     zoneBtn.frame = CGRectMake(zoneBtn.frame.origin.x,zoneBtn.frame.origin.y+Height, zoneBtn.frame.size.width,zoneBtn.frame.size.height);
                                     zoneTableView.frame = CGRectMake(zoneTableView.frame.origin.x,CGRectGetMaxY(zoneBtn.frame), zoneTableView.frame.size.width,zoneTableView.frame.size.height);
                                     
                                     cityBtn.frame = CGRectMake(cityBtn.frame.origin.x,cityBtn.frame.origin.y+Height, cityBtn.frame.size.width,cityBtn.frame.size.height);
                                     cityTableView.frame = CGRectMake(cityTableView.frame.origin.x,CGRectGetMaxY(cityBtn.frame), cityTableView.frame.size.width,cityTableView.frame.size.height);
                                 }
                                 else if ([tableView isEqual:zoneTableView]){

                                     cityBtn.frame = CGRectMake(cityBtn.frame.origin.x,cityBtn.frame.origin.y+Height, cityBtn.frame.size.width,cityBtn.frame.size.height);
                                     cityTableView.frame = CGRectMake(cityTableView.frame.origin.x,CGRectGetMaxY(cityBtn.frame), cityTableView.frame.size.width,cityTableView.frame.size.height);
                                     
                                 }
                                 courseBtn.frame = CGRectMake(courseBtn.frame.origin.x,courseBtn.frame.origin.y+Height, courseBtn.frame.size.width,courseBtn.frame.size.height);
                                 courseTableView.frame = CGRectMake(courseTableView.frame.origin.x,CGRectGetMaxY(courseBtn.frame), courseTableView.frame.size.width,courseTableView.frame.size.height);
                             }
                         }
                         completion:^(BOOL finished) {
                             [tableView reloadData];

                             regionBtn.userInteractionEnabled   = TRUE;
                             zoneBtn.userInteractionEnabled     = TRUE;
                             cityBtn.userInteractionEnabled     = TRUE;
                             courseBtn.userInteractionEnabled   = TRUE;
                             
                             regionTableView.userInteractionEnabled    = TRUE;
                             zoneTableView.userInteractionEnabled      = TRUE;
                             cityTableView.userInteractionEnabled      = TRUE;
                             courseTableView.userInteractionEnabled    = TRUE;
                             [lbtn setBackgroundImage:[UIImage imageNamed:@"titlebutton_pressed.png"] forState:UIControlStateNormal];
                         }];
    }
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:courseListTableView] && courseListArray.count <= 0)
    {
        return 0;
    }
    return 50;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:regionTableView]){
        return regionArray.count;
    }else if ([tableView isEqual:zoneTableView]){
        return zoneArray.count;
    }
    else if([tableView isEqual:stateTableView]){
        return stateArray.count;
    }else if([tableView isEqual:cityTableView]){
        return cityArray.count;
    }
    else if([tableView isEqual:courseTableView]){
            return courseArray.count;
    }
    else if ([tableView isEqual:courseListTableView])
    {
        return courseListArray.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier;
    if ([tableView isEqual:regionTableView]){
        CellIdentifier = @"regionCell";
    }else if ([tableView isEqual:zoneTableView]){
        CellIdentifier = @"zoneCell";
    }
    else if([tableView isEqual:stateTableView]){
        CellIdentifier = @"stateCell";
    }else if([tableView isEqual:cityTableView]){
        CellIdentifier = @"cityCell";
    }
    else if([tableView isEqual:courseTableView]){
        CellIdentifier = @"courseCell";
    }else if ([tableView isEqual:courseListTableView])
    {
        CellIdentifier = @"courseListCell";
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType              = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled     = YES;
        cell.selectionStyle             = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor        = [UIColor whiteColor];
        cell.textLabel.font             = TAHOMA_FONT(16);
        cell.textLabel.textAlignment    = UITextAlignmentCenter;
//        cell.textLabel.shadowColor = [UIColor blackColor];
//        cell.textLabel.shadowOffset = CGSizeMake(0, -1);
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.5];
        cell.selectedBackgroundView = bgColorView;
        // DebugLog(@"height %f width %f",cell.frame.size.height,cell.frame.size.width);
        
        UIImageView *seperatorImg= [[UIImageView alloc] init];
        if ([tableView isEqual:courseListTableView]) {
            seperatorImg.frame = CGRectMake(0,48,courseListTableView.frame.size.width, 2);
        }
        else
        {
            seperatorImg.frame = CGRectMake(0,48,regionTableView.frame.size.width, 2);
        }
        seperatorImg.image = [UIImage imageNamed:@"citydropdownline.png"];
        seperatorImg.contentMode = UIViewContentModeScaleToFill;
        [cell.contentView addSubview:seperatorImg];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    if ([tableView isEqual:regionTableView]){
        if (regionArray.count > 0) {
            cell.textLabel.text = [regionArray objectAtIndex:indexPath.row];
        }else{
            cell.textLabel.text = @"";
        }
    }
    else if ([tableView isEqual:zoneTableView]){
        if (zoneArray.count > 0) {
            cell.textLabel.text = [zoneArray objectAtIndex:indexPath.row];
        }else{
            cell.textLabel.text = @"";
        }
    }
    else if([tableView isEqual:stateTableView]){
        if (stateArray.count > 0) {
            cell.textLabel.text = [stateArray objectAtIndex:indexPath.row];
        }else{
            cell.textLabel.text = @"";
        }
    }else if([tableView isEqual:cityTableView]){
        if (cityArray.count > 0) {
            cell.textLabel.text = [cityArray objectAtIndex:indexPath.row];
        }else{
            cell.textLabel.text = @"";
        }
    }
    else if([tableView isEqual:courseTableView]){
        if (courseArray.count > 0) {
            cell.textLabel.text = [courseArray objectAtIndex:indexPath.row];
        }else{
        cell.textLabel.text = @"";
        }
    }else if ([tableView isEqual:courseListTableView]){
        if (courseListArray.count > 0) {
            CourseObject *courseObj = (CourseObject *)[courseListArray objectAtIndex:indexPath.row];
            cell.textLabel.text = courseObj.courseName;
        }else{
            cell.textLabel.text = @"";
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    courseListTableView.hidden = YES;
    if ([tableView isEqual:courseListTableView])
    {
        CourseObject *courseObj = (CourseObject *)[courseListArray objectAtIndex:indexPath.row];
        DebugLog(@"you selected course : %@",courseObj.courseName);
    
        
        GIACourseDetailsViewController *coursedetailController = [[GIACourseDetailsViewController alloc] initWithNibName:@"GIACourseDetailsViewController" bundle:nil];
        coursedetailController.title = courseObj.courseName;
        coursedetailController.selectedCourseId = courseObj.courseId;
        [self.navigationController pushViewController:coursedetailController animated:YES];
        [courseListTableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];
    }
    else
    {
       if ([tableView isEqual:regionTableView]){
        if (indexPath.row == 0) {
            selectedRegion = @"";
        }
        else{
        selectedRegion = [regionArray objectAtIndex:indexPath.row];
        }
        [self setBtnTitle:regionBtn text:[regionArray objectAtIndex:indexPath.row]];
        [self setBtnTitle:zoneBtn text:@"Select Zone"];
        [self setBtnTitle:stateBtn text:@"Select State"];
        [self setBtnTitle:cityBtn text:@"Select City"];
        [self setBtnTitle:courseBtn text:@"Select Course"];
           
        selectedZone = @"";
//        selectedState = @"";
        selectedCity = @"";
        selectedCourse = @"";
           
        [self reloadZoneArrayAndTableView];
        //[self reloadStateArrayAndTableView];
        [self reloadCityArrayAndTableView];
        [self reloadCourseArrayAndTableView];

        //[self regionBtnClicked:regionBtn];
        [self tableCellSelected:@"region"];
    }
    else if ([tableView isEqual:zoneTableView]){
        if (indexPath.row == 0) {
            selectedZone = @"";
        }else{
            selectedZone = [zoneArray objectAtIndex:indexPath.row];
        }
        [self setBtnTitle:zoneBtn text:[zoneArray objectAtIndex:indexPath.row]];
        [self setBtnTitle:stateBtn text:@"Select State"];
        [self setBtnTitle:cityBtn text:@"Select City"];
        [self setBtnTitle:courseBtn text:@"Select Course"];
        
//        selectedState = @"";
        selectedCity = @"";
        selectedCourse = @"";
        
       // [self reloadStateArrayAndTableView];
        [self reloadCityArrayAndTableView];
        [self reloadCourseArrayAndTableView];
        
        //[self zoneBtnClicked:zoneBtn];
        [self tableCellSelected:@"zone"];
    }
    else if([tableView isEqual:stateTableView]){
        if (indexPath.row == 0) {
            selectedState = @"";
        }else{
            selectedState = [stateArray objectAtIndex:indexPath.row];
        }
        [self setBtnTitle:stateBtn text:[stateArray objectAtIndex:indexPath.row]];
        [self setBtnTitle:cityBtn text:@"Select City"];
        [self setBtnTitle:courseBtn text:@"Select Course"];
        
        selectedCity = @"";
        selectedCourse = @"";
        
        [self reloadCityArrayAndTableView];
        [self reloadCourseArrayAndTableView];
        
        [self stateBtbClicked:stateBtn];
    }
    else if([tableView isEqual:cityTableView]){
        if (indexPath.row == 0) {
            selectedCity = @"";
        }else{
            selectedCity = [cityArray objectAtIndex:indexPath.row];
        }
        [self setBtnTitle:cityBtn text:[cityArray objectAtIndex:indexPath.row]];
        [self setBtnTitle:courseBtn text:@"Select Course"];
        
        selectedCourse = @"";
        [self reloadCourseArrayAndTableView];
        
       // [self cityBtnClicked:cityBtn];
        [self tableCellSelected:@"city"];
    }
    else if([tableView isEqual:courseTableView]){
        if (indexPath.row == 0) {
            selectedCourse = @"";
        }else{
            selectedCourse = [courseArray objectAtIndex:indexPath.row];
        }
        [self setBtnTitle:courseBtn text:[courseArray objectAtIndex:indexPath.row]];
        
        [self courseBtnClicked:courseBtn];
    }
    [self reloadCalendarView];
    }
}
-(void)setBtnTitle:(UIButton *)btn text:(NSString *)title
{
     [btn setTitle:title forState:UIControlStateNormal];
  //   [btn setTitle:title forState:UIControlStateHighlighted];
}
-(void)reloadRegionArrayAndTableView
{
    DebugLog(@"reloadRegionArrayAndTableView");
    if (regionArray == nil){
        regionArray = [[NSMutableArray alloc] init];
    }else
    {
        [regionArray removeAllObjects];
    }
    [regionArray addObject:@"All Regions"];
    if(sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK){
        const char *sqlQuery = "SELECT course_city_region FROM COURSES_TABLE GROUP BY course_city_region";
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, sqlQuery, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                NSString *region = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                [regionArray addObject:region];
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    DebugLog(@"region Array %@",regionArray);
    [regionTableView reloadData];
}
-(void)reloadZoneArrayAndTableView
{
    DebugLog(@"reloadZoneArrayAndTableView");
    if (zoneArray == nil){
        zoneArray = [[NSMutableArray alloc] init];
    }else{
        [zoneArray removeAllObjects];
    }
    [zoneArray addObject:@"All Zones"];
    if(sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK){
        const char *sqlQuery;
        //if region is selected
        DebugLog(@"selected Region = %@",selectedRegion);
        if ([selectedRegion isEqualToString:@""] || selectedRegion == nil){
            sqlQuery = "SELECT course_city_zone FROM COURSES_TABLE GROUP BY course_city_zone";
        }else{
            NSString *sqlQueryString = [NSString stringWithFormat:@"SELECT course_city_zone FROM COURSES_TABLE WHERE course_city_region LIKE \"%@%%\" COLLATE NOCASE GROUP BY course_city_zone",selectedRegion];
            sqlQuery = [sqlQueryString UTF8String];
        }
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, sqlQuery, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                NSString *zone = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                [zoneArray addObject:zone];
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
        DebugLog(@"zone Array %@",zoneArray);
        [zoneTableView reloadData];
    }
}
-(void)reloadStateArrayAndTableView
{
    DebugLog(@"reloadStateArrayAndTableView");
    if (stateArray == nil){
        stateArray = [[NSMutableArray alloc] init];
    }else{
        [stateArray removeAllObjects];
    }
    [stateArray addObject:@"All States"];
    DebugLog(@"selected Region = %@",selectedRegion);
    DebugLog(@"selected Zone = %@",selectedZone);

    if(sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK){
        const char *sqlQuery;
        //if region is selected
        if (([selectedRegion isEqualToString:@""] || selectedRegion == nil) && ([selectedZone isEqualToString:@""] || selectedZone == nil)){
            sqlQuery = "SELECT course_city_state FROM COURSES_TABLE GROUP BY course_city_state";
        }
//        else if ([selectedZone isEqualToString:@""] || selectedZone == nil)
//        {
//            NSString *sqlQueryString = [NSString stringWithFormat:@"SELECT course_city_state FROM COURSES_TABLE WHERE course_city_region LIKE \"%@%%\" COLLATE NOCASE GROUP BY course_city_state",selectedRegion];
//            sqlQuery = [sqlQueryString UTF8String];
//        }
//        else if ([selectedRegion isEqualToString:@""] || selectedRegion == nil)
//        {
//            NSString *sqlQueryString = [NSString stringWithFormat:@"SELECT course_city_state FROM COURSES_TABLE WHERE course_city_zone LIKE \"%@%%\" COLLATE NOCASE GROUP BY course_city_state",selectedZone];
//            sqlQuery = [sqlQueryString UTF8String];
//        }
        else{
            NSString *sqlQueryString = [NSString stringWithFormat:@"SELECT course_city_state FROM COURSES_TABLE WHERE course_city_region LIKE \"%@%%\" AND course_city_zone LIKE \"%@%%\" COLLATE NOCASE GROUP BY course_city_state",selectedRegion,selectedZone];
            sqlQuery = [sqlQueryString UTF8String];
            DebugLog(@"Query -%@-",sqlQueryString);
        }
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, sqlQuery, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                NSString *state = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                [stateArray addObject:state];
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        DebugLog(@"state Array %@",stateArray);
        [stateTableView reloadData];
    }
}

-(void)reloadCityArrayAndTableView
{
    DebugLog(@"reloadCityArrayAndTableView");
    if (cityArray == nil){
        cityArray = [[NSMutableArray alloc] init];
    }else{
        [cityArray removeAllObjects];
    }
    [cityArray addObject:@"All Cities"];
    DebugLog(@"selected Region = %@",selectedRegion);
    DebugLog(@"selected Zone = %@",selectedZone);
    DebugLog(@"selected state = %@",selectedState);

    if(sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK){
        const char *sqlQuery;
        //if region is selected
        if (([selectedRegion isEqualToString:@""] || selectedRegion == nil) && ([selectedZone isEqualToString:@""] || selectedZone == nil)){
            sqlQuery = "SELECT course_city_name FROM COURSES_TABLE GROUP BY course_city_name";
        }else{
            NSString *sqlQueryString = [NSString stringWithFormat:@"SELECT course_city_name FROM COURSES_TABLE WHERE course_city_region LIKE \"%@%%\" AND course_city_zone LIKE \"%@%%\" COLLATE NOCASE GROUP BY course_city_name",selectedRegion,selectedZone];
            sqlQuery = [sqlQueryString UTF8String];
            DebugLog(@"Query -%@-",sqlQueryString);
        }
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, sqlQuery, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                NSString *city = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                [cityArray addObject:city];
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        DebugLog(@"city Array %@",cityArray);
        [cityTableView reloadData];
    }
}
-(void)reloadCourseArrayAndTableView
{
    DebugLog(@"reloadCourseArrayAndTableView");
    if (courseArray == nil){
        courseArray = [[NSMutableArray alloc] init];
    }else{
        [courseArray removeAllObjects];
    }
    [courseArray addObject:@"All Courses"];

    if(sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK){
        const char *sqlQuery;
        if (([selectedRegion isEqualToString:@""] || selectedRegion == nil) && ([selectedZone isEqualToString:@""] || selectedZone == nil) && ([selectedCity isEqualToString:@""] || selectedCity == nil)){
            sqlQuery = "SELECT product FROM COURSES_TABLE GROUP BY product";
        }else{
            NSString *sqlQueryString = [NSString stringWithFormat:@"SELECT product FROM COURSES_TABLE WHERE course_city_region LIKE \"%@%%\" AND course_city_zone LIKE \"%@%%\" AND course_city_name LIKE \"%@%%\" COLLATE NOCASE GROUP BY product",selectedRegion,selectedZone,selectedCity];
            sqlQuery = [sqlQueryString UTF8String];
            DebugLog(@"Query -%@-",sqlQueryString);
        }
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, sqlQuery, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                NSString *course = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                [courseArray addObject:course];
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    DebugLog(@"course Array %@",courseArray);
    [courseTableView reloadData];
}
-(void)reloadStartDatesArray
{
    DebugLog(@"getStartDatesArray");
    if (datesArray == nil){
        datesArray = [[NSMutableArray alloc] init];
    }else{
        [datesArray removeAllObjects];
    }
    if(sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK){
        const char *sqlQuery;
        if (([selectedRegion isEqualToString:@""] || selectedRegion == nil) && ([selectedZone isEqualToString:@""] || selectedZone == nil) && ([selectedCity isEqualToString:@""] || selectedCity == nil) && ([selectedCourse isEqualToString:@""] || selectedCourse == nil))       {
            sqlQuery = "SELECT course_start_date FROM COURSES_TABLE GROUP BY course_start_date";
        }else{
            NSString *sqlQueryString = [NSString stringWithFormat:@"SELECT course_start_date FROM COURSES_TABLE WHERE course_city_region LIKE \"%@%%\" AND course_city_zone LIKE \"%@%%\" AND course_city_name LIKE \"%@%%\" AND product  LIKE \"%@%%\" COLLATE NOCASE GROUP BY course_start_date",selectedRegion,selectedZone,selectedCity,selectedCourse];
            sqlQuery = [sqlQueryString UTF8String];
            DebugLog(@"Query -%@-",sqlQueryString);
        }
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, sqlQuery, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                NSString *dateString = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                [datesArray addObject:dateString];
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    DebugLog(@"datesArray %@",datesArray);
}

-(void)reloadCalendarView{
    DebugLog(@"reloadCalendarView");
    [self reloadStartDatesArray];
    [self.calendar reloadData];
}
//Calendar
-(void)setupCalendarItems{
    DebugLog(@"setupCalendarItems");
    CKCalendarView *lcalendar = [[CKCalendarView alloc] initWithStartDay:startMonday];
    self.calendar = lcalendar;
    calendar.delegate = self;
 //   self.calendar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"calenderbg.png"]];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.minimumDate = [self.dateFormatter dateFromString:@"20/09/2012"];
    
//    self.disabledDates = @[
//                           [self.dateFormatter dateFromString:@"05/01/2013"],
//                           [self.dateFormatter dateFromString:@"06/01/2013"],
//                           [self.dateFormatter dateFromString:@"07/01/2013"]
//                           ];
    
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    
    calendar.frame = CGRectMake(410, 10, 500, 320);
    [self.view addSubview:calendar];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(localeDidChange) name:NSCurrentLocaleDidChangeNotification object:nil];
    
//    self.datesArray = @[[self.dateFormatter dateFromString:@"05/06/2013"],
//                        [self.dateFormatter dateFromString:@"06/06/2013"],
//                        [self.dateFormatter dateFromString:@"07/06/2013"],
//                        [self.dateFormatter dateFromString:@"25/06/2013"],
//                        [self.dateFormatter dateFromString:@"17/06/2013"]
//                        ];
//    self.datesArray = @[@"05/06/2013",@"26/6/2013"];
    DebugLog(@"datearray %@ ",self.datesArray);
}

- (void)localeDidChange{
    [self.calendar setLocale:[NSLocale currentLocale]];
}

//- (BOOL)dateIsDisabled:(NSDate *)date {
//    for (NSDate *disabledDate in self.disabledDates) {
//        if ([disabledDate isEqualToDate:date]) {
//            return YES;
//        }
//    }
//    return NO;
//}

- (BOOL)dateIsAvailable:(NSDate *)date {
    for (NSString *avdatestring in self.datesArray) {
        NSDate *avdate = [self.dateFormatter dateFromString:avdatestring];
        if ([avdate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}


#pragma mark -
#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date {
    // TODO: play with the coloring if we want to...
//    if ([self dateIsDisabled:date]) {
//        dateItem.backgroundColor = [UIColor redColor];
//        dateItem.textColor = [UIColor whiteColor];
//    }
    if ([self dateIsAvailable:date]) {
        dateItem.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"eventred.png"]];//[UIColor colorWithRed:92/255.0 green:15/255.0 blue:46/255.0 alpha:1];
        dateItem.textColor = [UIColor whiteColor];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date {
 //   return ![self dateIsDisabled:date];
    return YES;
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date {
    NSString *selectedDate = [self.dateFormatter stringFromDate:date];
    DebugLog(@"Dated Selected as %@",selectedDate);
    
    if (courseListArray == nil){
        courseListArray = [[NSMutableArray alloc] init];
    }else{
        [courseListArray removeAllObjects];
    }
    if(sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &database) == SQLITE_OK){
        NSString *sqlQueryString;
        if (([selectedRegion isEqualToString:@""] || selectedRegion == nil) && ([selectedZone isEqualToString:@""] || selectedZone == nil) && ([selectedCity isEqualToString:@""] || selectedCity == nil) && ([selectedCourse isEqualToString:@""] || selectedCourse == nil))
        {
            sqlQueryString = [NSString stringWithFormat:@"SELECT product,course_unique_auto_number FROM COURSES_TABLE WHERE course_start_date = \"%@\" group by product",selectedDate];
        }else{
            sqlQueryString = [NSString stringWithFormat:@"SELECT product,course_unique_auto_number FROM COURSES_TABLE WHERE course_start_date = \"%@\" AND course_city_region LIKE \"%@%%\" AND course_city_zone LIKE \"%@%%\" AND course_city_name LIKE \"%@%%\" AND product  LIKE \"%@%%\" COLLATE NOCASE",selectedDate,selectedRegion,selectedZone,selectedCity,selectedCourse];
        }
        const char *sqlQuery = [sqlQueryString UTF8String];
            DebugLog(@"Query -%@-",sqlQueryString);
        CourseObject *courseObj;
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, sqlQuery, -1, &statement, NULL) == SQLITE_OK){
            while(sqlite3_step(statement) == SQLITE_ROW){
                NSString *product = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                NSString *courseId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                
                courseObj = [[CourseObject alloc]init];
                courseObj.courseName = product;
                courseObj.courseId = courseId;
                [courseListArray addObject:courseObj];
                courseObj = nil;
            }
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    DebugLog(@"results %@",courseListArray);
    if (courseListArray.count == 0) {
        courseListTableView.hidden = YES;
    }
    else
    {
        courseListTableView.hidden = NO;
        [courseListTableView reloadData];
        courseListTableView.frame = CGRectMake(courseListTableView.frame.origin.x, courseListTableView.frame.origin.y,courseListTableView.frame.size.width, 179);
        [courseListTableView sizeToFit];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willChangeToMonth:(NSDate *)date {
    return [date laterDate:self.minimumDate] == date;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
