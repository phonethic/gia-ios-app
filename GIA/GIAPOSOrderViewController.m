//
//  GIAPOSOrderViewController.m
//  GIA
//
//  Created by Rishi on 17/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIAPOSOrderViewController.h"
#import "constants.h"

@interface GIAPOSOrderViewController ()

@end

@implementation GIAPOSOrderViewController
@synthesize posdelegate;
@synthesize namelbl;
@synthesize emaillbl;
@synthesize mobilelbl;
@synthesize citylbl;
@synthesize profilelbl;
@synthesize addresstextView;
@synthesize confirmBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    tapGesture.cancelsTouchesInView = NO;	
    [self.view addGestureRecognizer:tapGesture];
    
    [self changeLabelProperties:namelbl];
    [self changeLabelProperties:emaillbl];
    [self changeLabelProperties:mobilelbl];
    [self changeLabelProperties:citylbl];
    [self changeLabelProperties:profilelbl];
    
    addresstextView.font = TAHOMA_FONT(18);
    confirmBtn.titleLabel.font    = TAHOMA_FONT(25);
    
    [self fillDetails];
}
-(void)changeLabelProperties:(UILabel *)lbl
{
    lbl.backgroundColor = [UIColor clearColor];
    lbl.numberOfLines = 2;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.textAlignment = UITextAlignmentLeft;
    lbl.font = TAHOMA_FONT(18);
    lbl.textColor = [UIColor blackColor];
}

-(void)fillDetails
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *firstName = [defaults objectForKey:LOGIN_FIRST_NAME];
    NSString *lastName  = [defaults objectForKey:LOGIN_LAST_NAME];
    NSString *email = [defaults objectForKey:LOGIN_EMAIL];
    NSString *phoneNumber = [defaults objectForKey:LOGIN_NUMBER];
    NSString *city = [defaults objectForKey:LOGIN_CITY];
    NSString *profile = [defaults objectForKey:LOGIN_PROFILE];
    NSString *address = [defaults objectForKey:LOGIN_ADDRESS];
    namelbl.text = [NSString stringWithFormat:@"%@ %@",firstName,lastName];
    emaillbl.text = email;
    mobilelbl.text = phoneNumber;
    citylbl.text = city;
    profilelbl.text = profile;
    addresstextView.text = address;
}

- (void)dismissKeyboard:(UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)viewDidUnload {
    [self setNamelbl:nil];
    [self setEmaillbl:nil];
    [self setMobilelbl:nil];
    [self setCitylbl:nil];
    [self setProfilelbl:nil];
    [self setAddresstextView:nil];
    [self setConfirmBtn:nil];
    [super viewDidUnload];
}

- (IBAction)confirmBtnPressed:(id)sender {
    if([addresstextView.text isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Please fill Address field."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    } else {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:addresstextView.text forKey:LOGIN_ADDRESS];
        [defaults synchronize];
        [self.posdelegate didDismissPresentedViewController];
    }
}
@end
