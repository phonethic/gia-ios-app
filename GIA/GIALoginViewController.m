//
//  GIALoginViewController.m
//  GIA
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIALoginViewController.h"
#import "constants.h"
#import "GIAAppDelegate.h"

#define NUMBERS_ONLY @"1234567890"
#define CHARACTER_LIMIT 16

@interface GIALoginViewController ()

@end

@implementation GIALoginViewController
@synthesize delegate;
@synthesize bgImageView;
@synthesize firstNameField,lastNameField,emailField,numberField;
@synthesize headinglbl;
@synthesize submitBtn;
@synthesize profileArray;
@synthesize profileBtn;
@synthesize loginscrollView;
@synthesize profilePickerView;
@synthesize otherProfileField;
@synthesize cityField;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)addCurrentUserDetails:(NSString *)firstname last:(NSString *)lastname email:(NSString *)lemail phone:(NSString *)number
{
    PFObject *currentUser = [PFObject objectWithClassName:@"GIA_USER"];
    if (firstname != NULL)
        [currentUser setObject:firstname forKey:@"firstname"];
    
    if (lastname != NULL)
        [currentUser setObject:lastname forKey:@"lastname"];
    
    if (lemail != NULL)
        [currentUser setObject:lemail forKey:@"email"];
    
    if (number != NULL)
        [currentUser setObject:number forKey:@"phone"];
    
    [currentUser saveEventually];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    loginscrollView.frame = CGRectMake(95, 160, loginscrollView.frame.size.width, loginscrollView.frame.size.height);
    loginscrollView.contentSize = CGSizeMake(346, 420);
    [loginscrollView setScrollEnabled:YES];
    loginscrollView.delaysContentTouches = NO;
    
    profilePickerView.hidden = TRUE;
    otherProfileField.hidden = TRUE;
    
    profileArray = [NSArray arrayWithObjects:@"Consumer",@"Diamond Manufacturer",@"Educational Institution",@"Event Organizer",@"Gemstone Manufacturer",@"Government",@"Individual",@"Jewellery Manufacturer",@"Knowledge Associate",@"Laboratory",@"Media Associate",@"Pearl Farming Unit",@"Retailer",@"Trader",@"Other",nil];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard:)];
    [self.bgImageView addGestureRecognizer:tapGesture];
    
    [self addPickerWithDoneButton];
    
    headinglbl.text = @"Enter details to login";
    headinglbl.font = TAHOMA_FONT(20);
    [submitBtn setTitle:@"Login" forState:UIControlStateNormal];
    [submitBtn setTitle:@"Login" forState:UIControlStateHighlighted];
    submitBtn.titleLabel.font    = TAHOMA_FONT(25);
    
    if (DEVELOPER_MODE) {
        firstNameField.text = @"Kirti";
        lastNameField.text = @"Nikam";
        emailField.text = @"kirti@phonethics.in";
        numberField.text = @"1234567890";
        cityField.text = @"Mumbai";
    }
}
-(void) addPickerWithDoneButton
{
    actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                              delegate:nil
                                     cancelButtonTitle:nil
                                destructiveButtonTitle:nil
                                     otherButtonTitles:nil];
    
    [actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    
    profilePickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 540, 0)];
    profilePickerView.showsSelectionIndicator = YES;
    profilePickerView.dataSource = self;
    profilePickerView.delegate = self;
    [actionSheet addSubview:profilePickerView];
    
    UISegmentedControl *doneButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Done"]];
    doneButton.momentary = YES;
    doneButton.frame = CGRectMake(480, 7.0f, 50.0f, 30.0f);
    doneButton.segmentedControlStyle = UISegmentedControlStyleBar;
    doneButton.tintColor = [UIColor colorWithRed:92/255.0 green:15/255.0 blue:46/255.0 alpha:1];
    [doneButton addTarget:self action:@selector(doneFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:doneButton];
    
    UISegmentedControl *cancelButton = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObject:@"Cancel"]];
    cancelButton.momentary = YES;
    cancelButton.frame = CGRectMake(10, 7.0f, 60.0f, 30.0f);
    cancelButton.segmentedControlStyle = UISegmentedControlStyleBar;
    cancelButton.tintColor = [UIColor blackColor];
    [cancelButton addTarget:self action:@selector(cancelFilterBtnClicked:) forControlEvents:UIControlEventValueChanged];
    [actionSheet addSubview:cancelButton];
    [self.view addSubview:actionSheet];
    [profilePickerView reloadAllComponents];
    
    [actionSheet setFrame:CGRectMake(0,580, 540, 260)];
    if([profilePickerView numberOfRowsInComponent:0] != -1)
    {
        [profilePickerView selectRow:0 inComponent:0 animated:YES];
        [profileBtn setTitle:[NSString stringWithFormat:@"%@",[profileArray objectAtIndex:0]] forState:UIControlStateNormal];
        [profileBtn setTitle:[NSString stringWithFormat:@"%@",[profileArray objectAtIndex:0]] forState:UIControlStateHighlighted];
    }
}
- (void)doneFilterBtnClicked:(id)sender
{
 //   [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
    [profileBtn setTitle:[NSString stringWithFormat:@"%@",[profileArray objectAtIndex:[profilePickerView selectedRowInComponent:0]]] forState:UIControlStateNormal];
    [profileBtn setTitle:[NSString stringWithFormat:@"%@",[profileArray objectAtIndex:[profilePickerView selectedRowInComponent:0]]] forState:UIControlStateHighlighted];
    [profileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [self showPicker:NO];
}

- (void)cancelFilterBtnClicked:(id)sender
{
    [self showPicker:NO];
  //  [actionSheet dismissWithClickedButtonIndex:0 animated:YES];
}
-(void)showPicker:(BOOL)Value
{
    if (Value == YES) {
        [UIView  transitionWithView:actionSheet duration:0.3  options:UIViewAnimationOptionTransitionNone
                         animations:^(void) {
                             [profilePickerView setHidden:FALSE];
                             [actionSheet setHidden:FALSE];
                             [actionSheet setFrame:CGRectMake(0,320, 540, 260)];
                         }
                         completion:^(BOOL finished) {
                                              }];
    }
    else
    {
        [UIView  transitionWithView:actionSheet duration:0.3  options:UIViewAnimationOptionTransitionNone
                         animations:^(void) {
                             [actionSheet setFrame:CGRectMake(0,580, 540, 260)];
                             [loginscrollView setContentOffset:CGPointMake(0, 0) animated:YES];
                             [self.view endEditing:YES];
                             [profilePickerView selectRow:[profileArray indexOfObject:profileBtn.titleLabel.text] inComponent:0 animated:YES];
                         }
                         completion:^(BOOL finished) {
                             [profilePickerView setHidden:TRUE];
                             [actionSheet setHidden:TRUE];
                         }];
  
    }
      //  [actionSheet showInView:self.view];
  //  [actionSheet setFrame:CGRectMake(0, 300, self.view.frame.size.height, 485)];
    //[actionSheet showInView:[UIApplication sharedApplication].keyWindow];
 //   [actionSheet showFromRect:profileBtn.frame inView:self.view animated:YES];
 //   [actionSheet setBounds:CGRectMake(0, 400, 1024, 485)];
}

#pragma mark Picker view methods
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [profileArray count];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    [profileBtn setTitle:[NSString stringWithFormat:@"%@",[profileArray objectAtIndex:row]] forState:UIControlStateNormal];
//    [profileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if(row < 14)
    {
        otherProfileField.hidden = TRUE;
    } else {
        otherProfileField.hidden = FALSE;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [profileArray objectAtIndex:row];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setFirstNameField:nil];
    [self setLastNameField:nil];
    [self setEmailField:nil];
    [self setNumberField:nil];
    [self setSubmitBtn:nil];
    [self setBgImageView:nil];
    [self setHeadinglbl:nil];
    [self setSubmitBtn:nil];
    [self setCityField:nil];
    [self setProfileBtn:nil];
    [self setLoginView:nil];
    [self setLoginscrollView:nil];
    [self setProfilePickerView:nil];
    [self setOtherProfileField:nil];
    [super viewDidUnload];
}

- (void)dismissKeyboard:(UITapGestureRecognizer *)recognizer
{
//    [loginscrollView setContentOffset:CGPointMake(0, 0) animated:YES];
//     profilePickerView.hidden = TRUE;
//    [self.view endEditing:YES];
    [self showPicker:NO];
}

- (BOOL)disablesAutomaticKeyboardDismissal {
    return NO;
}
-(BOOL)validateInputs
{
    if([firstNameField.text isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Please enter your first name."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        return FALSE;
    }
    if([lastNameField.text isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Please enter your last name."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        return FALSE;
    }
    if([emailField.text isEqualToString:@""]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Please enter email."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        return FALSE;
    }else{
        if(![self validateEmail:emailField.text])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:@"Please enter a valid email."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            return FALSE;
        }
    }

    if([numberField.text isEqualToString:@""])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Please enter mobile number."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        return FALSE;
    }else{
        if([numberField.text length] < 10)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:@"Mobile number should be 10 digits."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            return FALSE;
        }
    }
    if([cityField.text isEqualToString:@""]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                            message:@"Please enter city."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        return FALSE;
    }
    return TRUE;
}

- (IBAction)submitBtnPressed:(UIButton *)sender {

    if ([self validateInputs]) {
        [self saveData];
    }
}

- (IBAction)profileBtnPressed:(id)sender {
    DebugLog(@"profileBtnPressed");
    [self showPicker:YES];
    [self.view endEditing:YES];
    profilePickerView.hidden = FALSE;
    [loginscrollView setContentOffset:CGPointMake(0, 100) animated:YES];
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
//    if(textField == numberField)
//    {
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:NUMBERS_ONLY] invertedSet];
//        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
//        return (([string isEqualToString:filtered])&&(newLength <= CHARACTER_LIMIT));
//    } else {
//        return TRUE;
//    }
//}

-(BOOL)validateEmail: (NSString *) email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}
-(BOOL)validatePhoneNumber:(NSString *) phoneString {
    //  NSString *phoneRegex=@"[+]+91[0-9]{10}";
    NSString *phoneRegex=@"[0-9]{16}";
    NSPredicate *noTest=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [noTest evaluateWithObject:phoneString];
}
-(void)saveData
{
    [loginscrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    profilePickerView.hidden = TRUE;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:firstNameField.text forKey:LOGIN_FIRST_NAME];
    [defaults setObject:lastNameField.text forKey:LOGIN_LAST_NAME];
    [defaults setObject:emailField.text forKey:LOGIN_EMAIL];
    [defaults setObject:numberField.text forKey:LOGIN_NUMBER];
    [defaults setObject:cityField.text forKey:LOGIN_CITY];
    if(![profileBtn.titleLabel.text isEqualToString:@"Other"])
        [defaults setObject:profileBtn.titleLabel.text forKey:LOGIN_PROFILE];
    else
        [defaults setObject:[NSString stringWithFormat:@"Other - %@",otherProfileField.text] forKey:LOGIN_PROFILE];
    [defaults synchronize];

    
    NSDictionary *dictionary = nil;
    if(emailField.text != nil &&  [ADMIN_EMAILIDS containsObject:emailField.text]) {
        dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:EXPORT_KEY];
    }else{
        dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:EXPORT_KEY];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:EXPORT_NOTIFICATION object:nil userInfo:dictionary];
    
    
    //Save in Parse Cloud
    [self addCurrentUserDetails:firstNameField.text last:lastNameField.text email:emailField.text phone:numberField.text];
    

    NSDictionary *loginParams = [NSDictionary dictionaryWithObjectsAndKeys:[defaults objectForKey:LOGIN_CITY], @"Login_City",[defaults objectForKey:LOGIN_PROFILE], @"Login_Profile", nil];
    [Flurry logEvent:@"Login" withParameters:loginParams];
    
    [self.delegate didDismissPresentedViewController];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == otherProfileField) {
        [loginscrollView setContentOffset:CGPointMake(0, 120) animated:YES];
	}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == firstNameField) {
        [lastNameField becomeFirstResponder];
	}
    else if (textField == lastNameField) {
        [emailField becomeFirstResponder];
	}
    else if (textField == emailField) {
        [numberField becomeFirstResponder];
	}
    else if (textField == numberField) {
        [numberField resignFirstResponder];
    }
    else if (textField == numberField) {
        [cityField becomeFirstResponder];
    }
    else if (textField == otherProfileField) {
        profilePickerView.hidden = TRUE;
        [otherProfileField resignFirstResponder];
    }
     [loginscrollView setContentOffset:CGPointMake(0, 0) animated:YES];
   	return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}
@end
