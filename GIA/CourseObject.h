//
//  CourseObject.h
//  GIA
//
//  Created by Kirti Nikam on 04/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CourseObject : NSObject
@property (nonatomic,copy) NSString *courseName;
@property (nonatomic,copy) NSString *courseId;
@end
