//
//  GIALocalRepViewController.h
//  GIA
//
//  Created by Rishi on 02/08/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GIALocalRepViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIWebView *representativeWebView;
@end
