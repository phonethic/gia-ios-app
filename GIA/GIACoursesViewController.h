//
//  GIACoursesViewController.h
//  GIA
//
//  Created by Kirti Nikam on 03/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

//CREATE TABLE courses_table(sr_no,type,course_calendar_name,course_unique_auto_number,product,course_city_name,
//                           course_city_state,course_city_zone,course_start_date,course_end_date,approval_status,
//                           seats_available,tentative_enrollments,confimed_enrollments,course_calender_id,
//                           course_city_record_id,course_city_region);

@interface GIACoursesViewController : UIViewController
{
    sqlite3 *database;
}
@property (nonatomic,copy) NSString *selectedRegion;
@property (nonatomic,copy) NSString *selectedZone;
@property (nonatomic,copy) NSString *selectedState;
@property (nonatomic,copy) NSString *selectedCity;
@property (nonatomic,copy) NSString *selectedCourse;
@property (nonatomic,copy) NSString *previousControl;

@property (nonatomic, retain) NSMutableArray *regionArray;
@property (nonatomic, retain) NSMutableArray *zoneArray;
@property (nonatomic, retain) NSMutableArray *stateArray;
@property (nonatomic, retain) NSMutableArray *cityArray;
@property (nonatomic, retain) NSMutableArray *courseArray;
@property (nonatomic, retain) NSMutableArray *datesArray;
@property (nonatomic, retain) NSMutableArray *courseListArray;

@property (strong, nonatomic) IBOutlet UIButton *regionBtn;
@property (strong, nonatomic) IBOutlet UIButton *zoneBtn;
@property (strong, nonatomic) IBOutlet UIButton *stateBtn;
@property (strong, nonatomic) IBOutlet UIButton *cityBtn;
@property (strong, nonatomic) IBOutlet UIButton *resetBtn;
@property (strong, nonatomic) IBOutlet UIButton *courseBtn;

@property (strong, nonatomic) IBOutlet UITableView *regionTableView;
@property (strong, nonatomic) IBOutlet UITableView *zoneTableView;
@property (strong, nonatomic) IBOutlet UITableView *stateTableView;
@property (strong, nonatomic) IBOutlet UITableView *cityTableView;
@property (strong, nonatomic) IBOutlet UITableView *courseTableView;
@property (strong, nonatomic) IBOutlet UITableView *courseListTableView;

- (IBAction)resetBtnClicked:(id)sender;
- (IBAction)regionBtnClicked:(id)sender;
- (IBAction)zoneBtnClicked:(id)sender;
- (IBAction)stateBtbClicked:(id)sender;
- (IBAction)cityBtnClicked:(id)sender;
- (IBAction)courseBtnClicked:(id)sender;
@end
