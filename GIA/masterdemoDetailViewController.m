//
//  masterdemoDetailViewController.m
//  masterdemo
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "masterdemoDetailViewController.h"
#import "MFSideMenu.h"
#import <QuartzCore/QuartzCore.h>

@interface masterdemoDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
- (void)configureView;
@end

@implementation masterdemoDetailViewController
@synthesize contentWebview;
@synthesize imageView;
@synthesize mapimageview;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }        
}

- (void)configureView
{
    // Update the user interface for the detail item.

    if (self.detailItem) {
        self.detailDescriptionLabel.text = [self.detailItem description];
    }
    [self loadWebViewWithUrl:@"about-world"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    
    [self setupMenuBarButtonItems];
    //self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BG_new.jpg"]];
    
    [self.navigationController.navigationBar setHidden:YES];
    imageView.hidden = TRUE;
    contentWebview.scrollView.bounces = FALSE;
  //  self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
//    self.detailDescriptionLabel.backgroundColor = [UIColor colorWithRed:209/255.0 green:196/255.0 blue:182/255.0 alpha:0.5];
//    self.detailDescriptionLabel.shadowColor = [UIColor blackColor];
//    self.detailDescriptionLabel.shadowOffset = CGSizeMake(0, -1);
//    self.detailDescriptionLabel.font = TAHOMA_FONT(25);
//    self.detailDescriptionLabel.textColor = [UIColor colorWithRed:92/255.0 green:15/255.0 blue:46/255.0 alpha:1];
//    self.detailDescriptionLabel.layer.cornerRadius = 10.0;
//    self.detailDescriptionLabel.layer.borderColor = [UIColor brownColor].CGColor;
//    self.detailDescriptionLabel.layer.borderWidth = 5.0;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Detail", @"Detail");
    }
    return self;
}
							
#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}


- (void)setupMenuBarButtonItems {
    switch (self.splitViewController.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.splitViewController.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.splitViewController.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.splitViewController.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.splitViewController.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.splitViewController.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.splitViewController.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.splitViewController.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)viewDidUnload {
    [self setContentWebview:nil];
    [self setImageView:nil];
    [self setMapimageview:nil];
    [super viewDidUnload];
}

-(void)loadWebViewWithUrl:(NSString *)pagaeType
{
    //NSURL *pageUrl = [NSURL URLWithString:url];
    //NSURLRequest *requestObj = [NSURLRequest requestWithURL:pageUrl];
    //contentWebview.scalesPageToFit = YES;
    //[contentWebview setDelegate:self];
    //[contentWebview loadRequest:requestObj];
    if ([pagaeType isEqualToString:@"about-india"]) {
        imageView.hidden = FALSE;
        imageView.image = [UIImage imageNamed:@"gia_india_transparent.png"];
    }
    else{
        imageView.hidden = TRUE;
    }
    mapimageview.hidden = TRUE;
    contentWebview.hidden = FALSE;
    [contentWebview loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:pagaeType ofType:@"html"]isDirectory:NO]]];
}

-(void)loadMapView
{
        mapimageview.hidden = FALSE;
        mapimageview.image = [UIImage imageNamed:@"map.jpg"];
        imageView.hidden = TRUE;
        contentWebview.hidden = TRUE;
}

#pragma mark - Web View
- (void)webViewDidStartLoad:(UIWebView *)webView
{
     
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    //[self zoomToFit];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{

}

-(void)zoomToFit
{
    
    if ([contentWebview respondsToSelector:@selector(scrollView)])
    {
        UIScrollView *scroll=[contentWebview scrollView];
        
        float zoom=contentWebview.bounds.size.width/scroll.contentSize.width;
        [scroll setZoomScale:zoom animated:YES];
    }
}

@end
