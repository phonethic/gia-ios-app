//
//  GIAFeesScheduleViewController.h
//  GIA
//
//  Created by Kirti Nikam on 05/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GIAFeesScheduleViewController : UIViewController<UIWebViewDelegate>
{
    BOOL courseBtnStatus;
}
@property (strong, nonatomic) IBOutlet UIButton *courseTypeBtn;
@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (strong, nonatomic) IBOutlet UITableView *courseTypeTableView;

@property (nonatomic, retain) NSMutableDictionary *coursesDic;
@property (nonatomic, retain) NSMutableArray *courseTypeArray;
@property (strong, nonatomic) IBOutlet UIWebView *contentWebView;

- (IBAction)segmentControlIndexChanged:(id)sender;
- (IBAction)courseTypeBtnCliked:(id)sender;
@end
