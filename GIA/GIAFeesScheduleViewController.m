//
//  GIAFeesScheduleViewController.m
//  GIA
//
//  Created by Kirti Nikam on 05/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIAFeesScheduleViewController.h"
#import "MFSideMenu.h"
#import "constants.h"

@interface GIAFeesScheduleViewController ()

@end

@implementation GIAFeesScheduleViewController
@synthesize courseTypeBtn,courseTypeTableView;
@synthesize courseTypeArray;
@synthesize contentWebView;
@synthesize coursesDic;
@synthesize segmentControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [Flurry logEvent:@"Tab_Fees_Structure"];
    
    [self setupMenuBarButtonItems];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    
    courseTypeTableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"citydropdownbg.png"]];
    courseTypeArray = [[NSMutableArray alloc] init];
    [courseTypeArray addObject:@"Diploma Programs"];
    [courseTypeArray addObject:@"Lab Classes"];
    [courseTypeArray addObject:@"Accelerated Programmes"];
    [courseTypeArray addObject:@"GIA Seminars-Half/One Day Duration"];
    [courseTypeArray addObject:@"Skill Building Seminars Series"];
    [courseTypeArray addObject:@"Hobby Classes"];
    [courseTypeArray addObject:@"Student Workroom"];
    
    coursesDic = [[NSMutableDictionary alloc] init];
    [coursesDic setObject:[[NSArray alloc] initWithObjects:@"ind-diploma-programs",@"ind-lab-classes",@"ind-accelerated-programs",@"ind-gia-seminar",@"ind-skill-building",@"ind-hobby-classes",@"ind-student-workroom", nil] forKey:@"course_indian"];
    [coursesDic setObject:[[NSArray alloc] initWithObjects:@"nind-diploma-programs",@"nind-lab-classes",@"nind-accelerated-programs",@"nind-gia-seminar",@"nind-skill-classes",@"nind-hobby-classes",@"nind-student-workroom", nil] forKey:@"course_international"];
    
    [segmentControl setBackgroundImage:[UIImage imageNamed:@"titlebutton_unpressed.png"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [segmentControl setBackgroundImage:[UIImage imageNamed:@"citydropdownbg.png"] forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    [segmentControl setTitleTextAttributes:@{
                  UITextAttributeTextColor: [UIColor whiteColor],
            UITextAttributeTextShadowColor: [UIColor blackColor],
           UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)],
                       UITextAttributeFont: TAHOMA_FONT(20.0)
     } forState:UIControlStateNormal];
    
    [segmentControl setDividerImage:[UIImage imageNamed:@"divider.png"] forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [segmentControl setDividerImage:[UIImage imageNamed:@"selecteddivider.png"] forLeftSegmentState:UIControlStateSelected rightSegmentState:UIControlStateNormal barMetrics:UIBarMetricsDefault];

    [self segmentControlIndexChanged:segmentControl];
    contentWebView.delegate = self;
    contentWebView.contentMode = UIViewContentModeScaleAspectFit;
    contentWebView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    contentWebView.scalesPageToFit = YES;
    contentWebView.autoresizesSubviews = YES;
    contentWebView.scrollView.bounces = FALSE;
    [contentWebView sizeToFit];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setSegmentControl:nil];
    [self setCourseTypeBtn:nil];
    [self setCourseTypeTableView:nil];
    [self setContentWebView:nil];
    [super viewDidUnload];
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (IBAction)segmentControlIndexChanged:(UISegmentedControl *)sender {
    [courseTypeTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    if (segmentControl.selectedSegmentIndex == 0) {
        NSArray *array = [coursesDic objectForKey:@"course_indian"];
        [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:[array objectAtIndex:0] ofType:@"html"]isDirectory:NO]]];
    }
    else
    {
        NSArray *array = [coursesDic objectForKey:@"course_international"];
        [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:[array objectAtIndex:0] ofType:@"html"]isDirectory:NO]]];
    }
}
- (IBAction)courseTypeBtnCliked:(id)sender {
    [self pullUpDownAnimation:courseTypeTableView btnstatus:courseBtnStatus btn:sender];
    if (courseBtnStatus) {
        courseBtnStatus = NO;
        [sender setBackgroundImage: [UIImage imageNamed:@"titlebutton_unpressed.png"] forState:UIControlStateNormal];
    }else{
        courseBtnStatus = YES;
        [sender setBackgroundImage:[UIImage imageNamed:@"titlebutton_pressed.png"] forState:UIControlStateNormal];
    }
}

-(void)pullUpDownAnimation:(UITableView *)tableView btnstatus:(BOOL)btnStatus btn:(UIButton *)lbtn
{
    lbtn.enabled = FALSE;
    double Height = 350;
    if (btnStatus){
        [UIView  transitionWithView:tableView duration:1  options:UIViewAnimationOptionTransitionNone
                         animations:^(void) {
                             tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y, tableView.frame.size.width,0);
                          }
                         completion:^(BOOL finished) {
                             tableView.alpha = 0.0;
                             lbtn.enabled = TRUE;
                         }];
    }else{
        //  DebugLog(@"y : %f",tableView.frame.origin.y-tableView.frame.size.height);
        [UIView  transitionWithView:tableView duration:1  options:UIViewAnimationOptionTransitionNone
                         animations:^(void) {
                             tableView.alpha = 1.0;
                             tableView.frame = CGRectMake(tableView.frame.origin.x,tableView.frame.origin.y, tableView.frame.size.width,Height);
                         }
                         completion:^(BOOL finished) {
                             lbtn.enabled = TRUE;
                         }];
    }
}


#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return courseTypeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"courseTypeCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType              = UITableViewCellAccessoryNone;
        cell.userInteractionEnabled     = YES;
        cell.selectionStyle             = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor        = [UIColor whiteColor];
        cell.textLabel.font             = TAHOMA_FONT(15);
        cell.textLabel.textAlignment    = UITextAlignmentCenter;
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:0.5];
        cell.selectedBackgroundView = bgColorView;
        // DebugLog(@"height %f width %f",cell.frame.size.height,cell.frame.size.width);
        
        UIImageView *seperatorImg= [[UIImageView alloc] init];
        seperatorImg.frame = CGRectMake(0,48,courseTypeTableView.frame.size.width, 2);
        seperatorImg.image = [UIImage imageNamed:@"citydropdownline.png"];
        seperatorImg.contentMode = UIViewContentModeScaleToFill;
        [cell.contentView addSubview:seperatorImg];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    if (courseTypeArray.count > 0) {
            cell.textLabel.text = [courseTypeArray objectAtIndex:indexPath.row];
        }else{
            cell.textLabel.text = @"";
        }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if (indexPath.row == 0 && segmentControl.selectedSegmentIndex == 0) {
//        [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://192.168.254.46/gia-tables/ind-accelerated-programs.html"]]];
//        return;
//    }
    DebugLog(@"---------%@--------",[courseTypeArray objectAtIndex:indexPath.row]);
    NSDictionary *feeParams = [NSDictionary dictionaryWithObjectsAndKeys:[courseTypeArray objectAtIndex:indexPath.row], @"Course_Type", nil];
    [Flurry logEvent:@"Fees_Structure" withParameters:feeParams];
    
    if (segmentControl.selectedSegmentIndex == 0) {
        NSArray *array = [coursesDic objectForKey:@"course_indian"];
        [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:[array objectAtIndex:indexPath.row] ofType:@"html"]isDirectory:NO]]];
    }
    else
    {
        NSArray *array = [coursesDic objectForKey:@"course_international"];
        [contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:[array objectAtIndex:indexPath.row] ofType:@"html"]isDirectory:NO]]];
    }
}
#pragma mark - Web View
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidStartLoad");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    DebugLog(@"webViewDidFinishLoad");
//    CGSize contentSize = contentWebView.scrollView.contentSize;
//    CGSize viewSize = self.view.bounds.size;
//    
//    float rw = viewSize.width / contentSize.width;
//    
//    contentWebView.scrollView.minimumZoomScale = rw;
//    contentWebView.scrollView.maximumZoomScale = rw;
//    contentWebView.scrollView.zoomScale = rw;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    DebugLog(@"didFailLoadWithError : %@",error);
}
-(void)zoomToFit
{
    
    if ([contentWebView respondsToSelector:@selector(scrollView)])
    {
        UIScrollView *scroll=[contentWebView scrollView];
        
        float zoom=contentWebView.bounds.size.width/scroll.contentSize.width;
        [scroll setZoomScale:zoom animated:YES];
    }
}
@end
