//
//  GIAAppDelegate.h
//  GIA
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "constants.h"
#import <Parse/Parse.h>
#import "Reachability.h"

#define GIA_APP_DELEGATE (GIAAppDelegate *)[[UIApplication sharedApplication] delegate]

@class GIAViewController;

@interface GIAAppDelegate : UIResponder <UIApplicationDelegate>
{
    Reachability* internetReach;

}
@property(nonatomic) Boolean networkavailable;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) GIAViewController *viewController;
@property(copy,nonatomic) NSString *databaseName;
@property(copy,nonatomic) NSString *databasePath;
-(void) writeToTextFile:(NSString *)lcontent name:(NSString *)lname;
-(NSString *) getTextFromFile:(NSString *)lname;
-(NSString *)getFilePath:(NSString *)fileName;
@end
