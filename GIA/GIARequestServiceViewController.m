//
//  GIARequestServiceViewController.m
//  GIA
//
//  Created by Rishi on 08/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIARequestServiceViewController.h"
#import "constants.h"

@interface GIARequestServiceViewController ()

@end

@implementation GIARequestServiceViewController
@synthesize checkBoxBtn;
@synthesize checkBoxBtn1;
@synthesize checkBoxBtn2;
@synthesize checkBoxBtn3;
@synthesize checkBoxBtn4;
@synthesize checkBoxBtn5;
@synthesize checkBoxBtn6;
@synthesize checkBoxBtn7;
@synthesize checkBoxBtn8;
@synthesize commenttextView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController.navigationBar setHidden:YES];
}

- (IBAction)checkboxBtnPressed:(UIButton *)sender {
    NSLog(@"%d",sender.tag);
    if([sender isSelected])
        [sender setSelected:FALSE];
    else
        [sender setSelected:TRUE];
}

- (IBAction)submitBtnPressed:(id)sender {
    [self sendServiceEmail];
}


-(void)sendServiceEmail
{
    @try{        
        NSMutableString *bodyMessage = [NSMutableString stringWithString:@"Service Request for ------- \n\n"];
        if([checkBoxBtn isSelected])
            [bodyMessage appendString:@"Diamond Grading \n\n"];
        if([checkBoxBtn1 isSelected])
            [bodyMessage appendString:@"Diamond Sealing \n\n"];
        if([checkBoxBtn2 isSelected])
            [bodyMessage appendString:@"Coloured Diamond Grading \n\n"];
        if([checkBoxBtn3 isSelected])
            [bodyMessage appendString:@"Coloured Diamond Identification and Origin \n\n"];
        if([checkBoxBtn4 isSelected])
            [bodyMessage appendString:@"Synthetic Diamond Grading \n\n"];
        if([checkBoxBtn5 isSelected])
            [bodyMessage appendString:@"Coloured Stone Identification \n\n"];
        if([checkBoxBtn6 isSelected])
            [bodyMessage appendString:@"Coloured Stone Country of Origin Reports \n\n"];
        if([checkBoxBtn7 isSelected])
            [bodyMessage appendString:@"Quality Assurance Services \n\n"];
        if([checkBoxBtn8 isSelected])
            [bodyMessage appendString:@"Pearl Identification and Classification \n\n"];
        if (![commenttextView.text isEqualToString:@""])
            [bodyMessage appendString:commenttextView.text];
        
        MFMailComposeViewController *picker=[[MFMailComposeViewController alloc]init];
        picker.mailComposeDelegate=self;
        picker.modalPresentationStyle = UIModalPresentationPageSheet;
        picker.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
        [picker setToRecipients:[[NSArray alloc] initWithObjects:POS_EMAIL1,POS_EMAIL2, nil]];
        [picker setSubject:@"Service Request"];
        [picker setMessageBody:bodyMessage isHTML:NO];
        [self presentModalViewController:picker animated:YES];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception %@",exception.reason);
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [controller dismissModalViewControllerAnimated:YES];
    if (result == MFMailComposeResultSent){
        NSLog(@"email sent");
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidUnload {
    [self setCheckBoxBtn:nil];
    [self setCheckBoxBtn1:nil];
    [self setCheckBoxBtn2:nil];
    [self setCheckBoxBtn3:nil];
    [self setCheckBoxBtn4:nil];
    [self setCheckBoxBtn5:nil];
    [self setCheckBoxBtn6:nil];
    [self setCheckBoxBtn7:nil];
    [self setCheckBoxBtn8:nil];
    [self setCommenttextView:nil];
    [super viewDidUnload];
}
@end
