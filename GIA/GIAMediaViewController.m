//
//  GIAMediaViewController.m
//  GIA
//
//  Created by Rishi on 06/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIAMediaViewController.h"
#import "MFSideMenu.h"
#import <QuartzCore/QuartzCore.h>
#import "constants.h"

@interface GIAMediaViewController ()

@end

@implementation GIAMediaViewController
@synthesize carousel;
@synthesize thumbsitems;
@synthesize moviePlayer;
@synthesize messagelbl;
@synthesize messageimgView;
@synthesize videoTextDic;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(!fullscreen)
    {
        [carousel scrollToItemAtIndex:0 animated:YES];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    DebugLog(@"DMDSongdetails viewWillDisappear");
    if(self.moviePlayer != nil && fullscreen)
    {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    carousel.type = iCarouselTypeCoverFlow2;
    [self setupMenuBarButtonItems];
//    thumbsitems = [[NSMutableArray alloc] init];
//    NSString *bundlePath1 = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/POS/video_thumbs"];
//    NSError *error1 = nil;
//    NSArray *files1 = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:bundlePath1 error:&error1];
//    for (int i=0; i<[files1 count]; i++) {
//        [thumbsitems addObject:[files1 objectAtIndex:i]];
//        DebugLog(@"%@", [thumbsitems objectAtIndex:i]);
//    }
    [Flurry logEvent:@"Tab_Media_Presentation"];
    messagelbl.hidden = TRUE;
    messageimgView.hidden = TRUE;
    [self fillTextDictionary];
    [carousel reloadData];
    
    [self changeLabelProperties:messagelbl];
}
-(void)changeLabelProperties:(UILabel *)lbl
{
    lbl.backgroundColor = [UIColor clearColor];
    lbl.numberOfLines = 2;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.textAlignment = UITextAlignmentLeft;
    lbl.font = TAHOMA_FONT(18);
    lbl.textColor = [UIColor whiteColor];
}
-(void)fillTextDictionary
{
    if(videoTextDic == nil) {
        videoTextDic = [[NSMutableDictionary alloc] init];
    }
    [videoTextDic setValue:@"GIA - How to Choose a Diamond" forKey:@"video1"];
    [videoTextDic setValue:@"GIA India MD Nirupa Bhatt walks the ramp with 10 GIA Alumni at IIJW 2012" forKey:@"video2"];
    [videoTextDic setValue:@"Mr. Anoop Mehta talks about education at GIA" forKey:@"video3"];
    [videoTextDic setValue:@"Mr. Shreyans Dholakia talks about education at GIA" forKey:@"video4"];
     DebugLog(@"dict%@",videoTextDic);
}

-(void) loadLocalDocument:(NSString *)filePath {
    if(self.moviePlayer != nil) {
        [moviePlayer stop];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerWillEnterFullscreenNotification
                                                       object:self.moviePlayer];
        [[NSNotificationCenter defaultCenter]  removeObserver:self
                                                         name:MPMoviePlayerDidExitFullscreenNotification
                                                       object:self.moviePlayer];
        if ([self.moviePlayer respondsToSelector:@selector(setFullscreen:animated:)])
        {
            [moviePlayer.view removeFromSuperview];
        }
        self.moviePlayer = nil;
    }
    messagelbl.hidden = FALSE;
    messageimgView.hidden = FALSE;
    NSURL* videoURL = [NSURL fileURLWithPath: filePath];
    self.moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:videoURL];
    self.moviePlayer.view.frame = CGRectMake(107, 130, 565, 314);
    [moviePlayer setControlStyle:MPMovieControlStyleDefault];
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
    self.moviePlayer.shouldAutoplay = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinishLocal:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieEnterFullScreen:)
                                                 name:MPMoviePlayerWillEnterFullscreenNotification
                                               object:self.moviePlayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieExitFullScreen:)
                                                 name:MPMoviePlayerDidExitFullscreenNotification
                                               object:self.moviePlayer];

    [self.view addSubview:moviePlayer.view];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
}

- (void) movieEnterFullScreen:(NSNotification*)notification {
    fullscreen = 0;
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
}
- (void) movieExitFullScreen:(NSNotification*)notification {
    fullscreen = 1;
    self.moviePlayer.scalingMode = MPMovieScalingModeFill;
}

- (void) moviePlayBackDidFinishLocal:(NSNotification*)notification {
    [moviePlayer setFullscreen:FALSE animated:TRUE];    
//    MPMoviePlayerController *player = [notification object];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:player];
//    if ([player respondsToSelector:@selector(setFullscreen:animated:)])
//    {
//        [player.view removeFromSuperview];
//    }
//    moviePlayer = nil;
    messagelbl.hidden = FALSE;
    messageimgView.hidden = FALSE;
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    DebugLog(@"count %d",[thumbsitems count]);
    return [videoTextDic count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 100.0f)];
        //((UIImageView *)view).image = [UIImage imageNamed:[thumbsitems objectAtIndex:index]];
        ((UIImageView *)view).image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[[videoTextDic.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
            return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
        }] objectAtIndex:index] ofType:@"png"]];
        view.contentMode = UIViewContentModeCenter;
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    ((UIImageView *)view).image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:[[videoTextDic.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
    }] objectAtIndex:index] ofType:@"png"]];
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    //label.text = [[items objectAtIndex:index] stringValue];
    
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    DebugLog(@"index====%d",index);
   // NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"GIA"] ofType:@"mp4"];
   // [self loadLocalDocument:path];
    
    NSString *imageName = [[videoTextDic.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
    }] objectAtIndex:index];
    DebugLog(@"name====%@",imageName);
//    NSRange temprange = [imageName rangeOfString:@"."];
//    //DebugLog(@"location %d length %d",temprange.location,temprange.length);
//    if ((temprange.location != NSNotFound))
//    {
//        imageName = [imageName substringToIndex:temprange.location];
//    }
//    DebugLog(@"imageName = -%@-",imageName);
    messagelbl.text = [videoTextDic valueForKey:imageName];
    NSString *path = [[NSBundle mainBundle] pathForResource:imageName ofType:@"mp4"];
    [self loadLocalDocument:path];

}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return 0.8f;
    } else if (option ==  iCarouselOptionTilt)
    {
        return 0.43f;
    }
    return value;
}

- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCarousel:nil];
    [self setMessagelbl:nil];
    [self setMessageimgView:nil];
    [super viewDidUnload];
}
@end
