//
//  SQLiteDatabaseOpertaions.m
//  GIA
//
//  Created by Kirti Nikam on 30/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//
#import <sqlite3.h>

#import "GIAAppDelegate.h"
#import "SQLiteDatabaseOpertaions.h"
#import "constants.h"

@implementation SQLiteDatabaseOpertaions

//CREATE TABLE IF NOT EXISTS POSREQ (ID INTEGER PRIMARY KEY AUTOINCREMENT,FIRSTNAME TEXT,LASTNAME TEXT,PHONE TEXT,EMAIL TEXT,CITY TEXT,PROFILE TEXT,ADDRESS TEXT,PRODUCT TEXT,QUANTITY TEXT,DATETIME DOUBLE);
+(BOOL)insertIntoPOSREQTable:(NSString *)lproduct quantity:(NSString *)lquantity;
{
    DebugLog(@"insert into POSREQTable");
    DebugLog(@"databasePath %@",[GIA_APP_DELEGATE databasePath]);
    
    BOOL success = NO;
    sqlite3 *giaDB;
    if (sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &giaDB) == SQLITE_OK) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *firstName      = [defaults objectForKey:LOGIN_FIRST_NAME];
        NSString *lastName       = [defaults objectForKey:LOGIN_LAST_NAME];
        NSString *phoneNumber    = [defaults objectForKey:LOGIN_NUMBER];
        NSString *emailAddress   = [defaults objectForKey:LOGIN_EMAIL];
        NSString *city           = [defaults objectForKey:LOGIN_CITY];
        NSString *profile        = [defaults objectForKey:LOGIN_PROFILE];
        NSString *address        = [defaults objectForKey:LOGIN_ADDRESS];

        double currentDatedouble = [[NSDate date] timeIntervalSinceReferenceDate];
        
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO POSREQ(FIRSTNAME,LASTNAME,PHONE,EMAIL,CITY,PROFILE,ADDRESS,PRODUCT,QUANTITY,DATETIME) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\", \"%f\")",firstName,lastName,phoneNumber,emailAddress,city,profile,address,lproduct,lquantity,currentDatedouble];
        DebugLog(@"---%@---",insertSQL);
        const char *insert_stmnt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(giaDB, insert_stmnt, -1, &statement, NULL) == SQLITE_OK){
            if (sqlite3_step(statement) == SQLITE_DONE) {
                DebugLog(@"POSREQ : successfully saved :");
                success = YES;
            } else {
                DebugLog(@"POSREQ : failed to save");
                success = NO;
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(giaDB);
    }
    
    return success;
}
//CREATE TABLE IF NOT EXISTS SERVICE_REQUEST_FOR (ID INTEGER PRIMARY KEY AUTOINCREMENT,FIRSTNAME TEXT,LASTNAME TEXT,PHONE TEXT,EMAIL TEXT,CITY TEXT,PROFILE TEXT,Diamond_Grading TEXT,Diamond_Sealing TEXT,Coloured_Diamond_Grading TEXT,Coloured_Diamond_Identification_and_Origin TEXT,Synthetic_Diamond_Grading TEXT,Coloured_Stone_Identification TEXT,Coloured_Stone_Country_of_Origin_Reports TEXT,Quality_Assurance_Services TEXT,Pearl_Identification_and_Classification  TEXT,COMMENTS TEXT,DATETIME DOUBLE);
+(BOOL)insertIntoSERVICE_REQUEST_FORTable:(NSArray *)larray
{
    DebugLog(@"array %@",larray);
    BOOL success = NO;

    sqlite3 *giaDB;
    if (sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &giaDB) == SQLITE_OK) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSString *firstName      = [defaults objectForKey:LOGIN_FIRST_NAME];
        NSString *lastName       = [defaults objectForKey:LOGIN_LAST_NAME];
        NSString *phoneNumber    = [defaults objectForKey:LOGIN_NUMBER];
        NSString *emailAddress   = [defaults objectForKey:LOGIN_EMAIL];
        NSString *city           = [defaults objectForKey:LOGIN_CITY];
        NSString *profile        = [defaults objectForKey:LOGIN_PROFILE];
        
        double currentDatedouble = [[NSDate date] timeIntervalSinceReferenceDate];
        
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO SERVICE_REQUEST_FOR(FIRSTNAME,LASTNAME,PHONE,EMAIL,CITY,PROFILE,Diamond_Grading ,Diamond_Sealing,Coloured_Diamond_Grading,Coloured_Diamond_Identification_and_Origin ,Synthetic_Diamond_Grading,Coloured_Stone_Identification,Coloured_Stone_Country_of_Origin_Reports,Quality_Assurance_Services,Pearl_Identification_and_Classification,COMMENTS,DATETIME) VALUES (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\", \"%f\")",firstName,lastName,phoneNumber,emailAddress,city,profile,[larray objectAtIndex:0],[larray objectAtIndex:1],[larray objectAtIndex:2],[larray objectAtIndex:3],[larray objectAtIndex:4],[larray objectAtIndex:5],[larray objectAtIndex:6],[larray objectAtIndex:7],[larray objectAtIndex:8],[larray objectAtIndex:9],currentDatedouble];
        DebugLog(@"---%@---",insertSQL);
        const char *insert_stmnt = [insertSQL UTF8String];
        sqlite3_stmt *statement = nil;
        if(sqlite3_prepare_v2(giaDB, insert_stmnt, -1, &statement, NULL) == SQLITE_OK){
            if (sqlite3_step(statement) == SQLITE_DONE) {
                DebugLog(@"SERVICE_REQUEST_FOR : successfully saved :");
                success = YES;
            } else {
                DebugLog(@"SERVICE_REQUEST_FOR : failed to save");
                success = NO;
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(giaDB);
    }
    
    return success;
}


+(BOOL)exportSQLITEDataToCSV:(NSString *)exportType
{
    DebugLog(@"---exportSQLITEDataToCSV---");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDate *lastsyncDate = [defaults objectForKey:ADMIN_LASTSYNC_EXPORT_DATE];
    double lastsyncDatedouble = [lastsyncDate timeIntervalSinceReferenceDate];
    
    // First POS data
    NSMutableString *posDataString = [[NSMutableString alloc]  initWithString:@"Sr,FirstName,LastName,Phone,Email,City,Profile,Address,Product,Quantity,DateTime\n"];
    
    // 2 nd Request Service data
    NSMutableString *reqDataString = [[NSMutableString alloc]  initWithString:@"Sr,FirstName,LastName,Phone,Email,City,Profile,\"Diamond Grading\",\"Diamond Sealing\",\"Coloured Diamond Grading\",\"Coloured Diamond Identification and Origin\",\"Synthetic Diamond Grading\",\"Coloured Stone Identification\",\"Coloured Stone Country of Origin Reports\",\"Quality Assurance Services\",\"Pearl Identification and Classification\",\"Comments\",\"DateTime\"\n"];

    sqlite3 *giaDB;

    if (sqlite3_open([[GIA_APP_DELEGATE databasePath] UTF8String], &giaDB) == SQLITE_OK) {
        NSString *selectQuery;
        if ([exportType isEqualToString:SQL_FULL]) {
            selectQuery = @"SELECT ID,FIRSTNAME,LASTNAME,PHONE,EMAIL,CITY,PROFILE,ADDRESS,PRODUCT,QUANTITY,DATETIME FROM POSREQ";
        }else{
            selectQuery = [NSString stringWithFormat:@"SELECT ID,FIRSTNAME,LASTNAME,PHONE,EMAIL,CITY,PROFILE,ADDRESS,PRODUCT,QUANTITY,DATETIME FROM POSREQ WHERE DATETIME > %f",lastsyncDatedouble];
        }
        DebugLog(@"---%@---",selectQuery);
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd yyyy HH:mm"];
        
        const char *selectChar = [selectQuery UTF8String];
        sqlite3_stmt *statement = nil;
        if (sqlite3_prepare_v2(giaDB, selectChar, -1, &statement, NULL) == SQLITE_OK) {

            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *rowid     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                NSString *firstname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                NSString *lastname  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                NSString *phone     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                NSString *email     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                NSString *city      = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
                NSString *profile   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
                NSString *address   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
                NSString *product   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)];
                NSString *quantity  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)];
                double doubleDate  = sqlite3_column_double(statement, 10);

                NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:doubleDate];
                NSString *dateString = [dateFormat stringFromDate:date];
                
                [posDataString appendString:[NSString stringWithFormat:@"\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\"\n",rowid,firstname,lastname,phone,email,city,profile,address,product,quantity,dateString]];
            }
            sqlite3_finalize(statement);
        }
        if ([exportType isEqualToString:SQL_FULL]) {
            selectQuery = @"SELECT ID ,FIRSTNAME,LASTNAME,PHONE,EMAIL,CITY,PROFILE,Diamond_Grading,Diamond_Sealing,Coloured_Diamond_Grading,Coloured_Diamond_Identification_and_Origin,Synthetic_Diamond_Grading,Coloured_Stone_Identification,Coloured_Stone_Country_of_Origin_Reports,Quality_Assurance_Services,Pearl_Identification_and_Classification,COMMENTS,DATETIME FROM SERVICE_REQUEST_FOR";
        }else{
            selectQuery = [NSString stringWithFormat:@"SELECT ID ,FIRSTNAME,LASTNAME,PHONE,EMAIL,CITY,PROFILE,Diamond_Grading,Diamond_Sealing,Coloured_Diamond_Grading,Coloured_Diamond_Identification_and_Origin,Synthetic_Diamond_Grading,Coloured_Stone_Identification,Coloured_Stone_Country_of_Origin_Reports,Quality_Assurance_Services,Pearl_Identification_and_Classification,COMMENTS,DATETIME FROM SERVICE_REQUEST_FOR WHERE DATETIME > %f",lastsyncDatedouble];
        }
        DebugLog(@"---%@---",selectQuery);
        
        selectChar = [selectQuery UTF8String];
        if (sqlite3_prepare_v2(giaDB, selectChar, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSString *rowid     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                NSString *firstname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                NSString *lastname  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)];
                NSString *phone     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)];
                NSString *email     = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)];
                NSString *city      = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)];
                NSString *profile   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)];
                NSString *string0   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
                NSString *string1   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)];
                NSString *string2   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)];
                NSString *string3   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)];
                NSString *string4   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 11)];
                NSString *string5   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 12)];
                NSString *string6   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 13)];
                NSString *string7   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 14)];
                NSString *string8   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 15)];
                NSString *comments  = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 16)];
                double doubleDate  = sqlite3_column_double(statement, 17);
                
                NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:doubleDate];
                NSString *dateString = [dateFormat stringFromDate:date];
                DebugLog(@"dateString---%@---",dateString);

                [reqDataString appendString:[NSString stringWithFormat:@"\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\"\n",rowid,firstname,lastname,phone,email,city,profile,string0,string1,string2,string3,string4,string5,string6,string7,string8,comments,dateString]];
            }
            sqlite3_finalize(statement);
        }
        sqlite3_close(giaDB);
    }
    
    
    if (![posDataString isEqualToString:@""]) {
        DebugLog(@"saving as csv : %@",POS_FILE);
        [GIA_APP_DELEGATE writeToTextFile:posDataString name:POS_FILE];
    }
    // 2 nd Request Service data
    if (![reqDataString isEqualToString:@""]) {
        DebugLog(@"saving as csv : %@",REQEST_SERVICE_FILE);
        [GIA_APP_DELEGATE writeToTextFile:reqDataString name:REQEST_SERVICE_FILE];
    }
    
    return TRUE;
}
@end
