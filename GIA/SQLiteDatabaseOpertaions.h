//
//  SQLiteDatabaseOpertaions.h
//  GIA
//
//  Created by Kirti Nikam on 30/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SQL_FULL @"export full data"
#define SQL_LAST_SYNC @"export data from last sync"

@interface SQLiteDatabaseOpertaions : NSObject
+(BOOL)insertIntoPOSREQTable:(NSString *)lproduct quantity:(NSString *)lquantity;
+(BOOL)insertIntoSERVICE_REQUEST_FORTable:(NSArray *)larray;
+(BOOL)exportSQLITEDataToCSV:(NSString *)exportType;
@end
