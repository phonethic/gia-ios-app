//
//  GIAPOSRequisitionViewController.m
//  GIA
//
//  Created by Kirti Nikam on 14/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import "GIAPOSRequisitionViewController.h"
#import "MFSideMenu.h"
#import <QuartzCore/QuartzCore.h>
#import "constants.h"
#import "GIALoginViewController.h"
#import "GIAPOSOrderViewController.h"

#import "SQLiteDatabaseOpertaions.h"

@interface UINavigationController (KeyboardDismiss)

- (BOOL)disablesAutomaticKeyboardDismissal;

@end

@implementation UINavigationController(KeyboardDismiss)

- (BOOL)disablesAutomaticKeyboardDismissal
{
    return NO;
}

@end

@interface GIAPOSRequisitionViewController ()
{
    GIAPOSOrderViewController *orderController;
}
@end

@implementation GIAPOSRequisitionViewController
@synthesize carousel;
@synthesize mainitems;
@synthesize thumbsitems;
@synthesize mainImageView;
@synthesize headinglbl,subheadinglbl,quantityTextField,orderBtn;
@synthesize mainitemsTextDic;
@synthesize descriptiontextview;
@synthesize dimensionlbl;
@synthesize dimensiontextlbl;
@synthesize descriptiontextlbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (mainitemsTextDic.allKeys.count > 0) {
        [carousel scrollToItemAtIndex:0 animated:YES];
        NSString *imageName = [[mainitemsTextDic.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
            return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
        }] objectAtIndex:0];
        
        mainImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"]];

        NSArray *array = [mainitemsTextDic objectForKey:imageName];
        headinglbl.text = [array objectAtIndex:0];
        subheadinglbl.text = [array objectAtIndex:1];
        dimensionlbl.text = [array objectAtIndex:3];
        if([dimensionlbl.text isEqualToString:@""])
        {
            [dimensiontextlbl setHidden:TRUE];
        } else
        {
            [dimensiontextlbl setHidden:FALSE];
        }
        descriptiontextview.text = [array objectAtIndex:2];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [Flurry logEvent:@"Tab_POS_Requisition"];
    
    headinglbl.font     = TAHOMA_FONT(32);
    subheadinglbl.font  = TAHOMA_FONT(23);
    
    dimensiontextlbl.font     = TAHOMA_FONT(20);
    descriptiontextlbl.font  = TAHOMA_FONT(20);
    
    dimensionlbl.font     = TAHOMA_FONT(18);
    descriptiontextview.font  = TAHOMA_FONT(18);

    quantityTextField.font      = TAHOMA_FONT(18);
    orderBtn.titleLabel.font    = TAHOMA_FONT(25);
    
    quantityTextField.hidden = TRUE;
    [orderBtn setTitle:@"Order" forState:UIControlStateNormal];
    
    mainImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    mainImageView.layer.borderWidth = 10.0;
    carousel.type = iCarouselTypeLinear;
    [self setupMenuBarButtonItems];
    
//    mainitems   = [[NSMutableArray alloc] init];
//    thumbsitems = [[NSMutableArray alloc] init];
    mainitemsTextDic = [[NSMutableDictionary alloc] init];

    [self fillTextDictionary];
//    NSString *bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/POS"];
//    NSError *error = nil;
//    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:bundlePath error:&error];
//    for (int i=0; i<[files count]; i++) {
//        [mainitems addObject:[files objectAtIndex:i]];
//        DebugLog(@"%@", [mainitems objectAtIndex:i]);
//    }
//    
//    NSString *bundlePath1 = [[[NSBundle mainBundle] resourcePath] stringByAppendingString:@"/POS/thumbs"];
//    NSError *error1 = nil;
//    NSArray *files1 = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:bundlePath1 error:&error1];
//    for (int i=0; i<[files1 count]; i++) {
//        [thumbsitems addObject:[files1 objectAtIndex:i]];
//        DebugLog(@"%@", [thumbsitems objectAtIndex:i]);
//    }
    
    [carousel reloadData];
    if (mainitemsTextDic.allKeys.count > 0) {
        [carousel scrollToItemAtIndex:0 animated:YES];
        
        NSString *imageName = [[mainitemsTextDic.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
            return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
        }] objectAtIndex:0];
        mainImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"]];
        
        //        NSRange temprange = [imageName rangeOfString:@"."];
        //        //DebugLog(@"location %d length %d",temprange.location,temprange.length);
        //        if ((temprange.location != NSNotFound))
        //        {
        //            imageName = [imageName substringToIndex:temprange.location];
        //        }
        NSArray *array = [mainitemsTextDic objectForKey:imageName];
        headinglbl.text = [array objectAtIndex:0];
        subheadinglbl.text = [array objectAtIndex:1];
    }
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    gestureRecognizer.cancelsTouchesInView=NO;
    [self.view addGestureRecognizer:gestureRecognizer];
}
- (void) hideKeyboard {
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setupMenuBarButtonItems {
    switch (self.navigationController.sideMenu.menuState) {
        case MFSideMenuStateClosed:
            if([[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
                // self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            } else {
                self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
            }
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
        case MFSideMenuStateLeftMenuOpen:
            self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
            break;
        case MFSideMenuStateRightMenuOpen:
            self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
            break;
    }
}
- (UIBarButtonItem *)leftMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleLeftSideMenu)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    return [[UIBarButtonItem alloc]
            initWithImage:[UIImage imageNamed:@"menu-icon.png"] style:UIBarButtonItemStyleBordered
            target:self.navigationController.sideMenu
            action:@selector(toggleRightSideMenu)];
}

- (UIBarButtonItem *)backBarButtonItem {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back-arrow"]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIDeviceOrientationLandscapeRight || interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

- (void)viewDidUnload {
    [self setCarousel:nil];
    [self setMainImageView:nil];
    [self setHeadinglbl:nil];
    [self setSubheadinglbl:nil];
    [self setQuantityTextField:nil];
    [self setOrderBtn:nil];
    [self setDimensionlbl:nil];
    [self setDescriptiontextview:nil];
    [self setDimensionlbl:nil];
    [self setDimensiontextlbl:nil];
    [self setDescriptiontextlbl:nil];
    [super viewDidUnload];
}

-(void)fillTextDictionary
{
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Coloured Stone Poster",@"Diamond",@"A set of 6 posters on the 5 main gemstones for retail display.These can be singly used or a poster in combination with all the gems.The posters bring out the basic highlights of each gem stone.At retail level, it would be informative reading matter for clients.Posters can be customized to size requirements.",@"", nil] forKey:@"image01"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Coloured Stone Poster",@"Ruby",@"A set of 6 posters on the 5 main gemstones for retail display.These can be singly used or a poster in combination with all the gems.The posters bring out the basic highlights of each gem stone.At retail level, it would be informative reading matter for clients.Posters can be customized to size requirements.",@"", nil] forKey:@"image02"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Coloured Stone Poster",@"Emerald",@"A set of 6 posters on the 5 main gemstones for retail display.These can be singly used or a poster in combination with all the gems.The posters bring out the basic highlights of each gem stone.At retail level, it would be informative reading matter for clients.Posters can be customized to size requirements.",@"", nil] forKey:@"image03"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Coloured Stone Poster",@"Sapphire",@"A set of 6 posters on the 5 main gemstones for retail display.These can be singly used or a poster in combination with all the gems.The posters bring out the basic highlights of each gem stone.At retail level, it would be informative reading matter for clients.Posters can be customized to size requirements.",@"", nil] forKey:@"image04"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Coloured Stone Poster",@"Pearls",@"A set of 6 posters on the 5 main gemstones for retail display.These can be singly used or a poster in combination with all the gems.The posters bring out the basic highlights of each gem stone.At retail level, it would be informative reading matter for clients.Posters can be customized to size requirements.",@"", nil] forKey:@"image05"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Combined Poster",@"",@"A set of 6 posters on the 5 main gemstones for retail display.These can be singly used or a poster in combination with all the gems.The posters bring out the basic highlights of each gem stone.At retail level, it would be informative reading matter for clients.Posters can be customized to size requirements.",@"", nil] forKey:@"image06"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"4Cs Brochure",@"",@"Speaks the 4cs  of Diamond Quality – Cut , Color, Clarity & Carat",@"3.75”x7”", nil] forKey:@"image07"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Coloured Stone Brochure",@"",@"An insight into the variety of gems. A guide to selecting coloured stones, understanding their treatments with special tips on how to care for precious stones.",@"", nil] forKey:@"image08"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Pearl Brochure",@"",@"Understand all about Pearls  & the GIA 7 Pearl Value Factors™",@"4”x9”", nil] forKey:@"image09"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"GIA Emeralds Brochure",@"",@"Tailored specifically for emeralds, it guides buyers and sellers on how to evaluate their quality.",@"3.75”x7”", nil] forKey:@"image10"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"How to read Lab Report ",@"",@"A guide to understanding GIA Diamond Grading Reports.",@"5.5”x11.5”", nil] forKey:@"image11"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Table Standees",@"",@"Ideal for a retail counter display – Spells out knowledge on the 4cs of Diamond Quality & GIA Grading Reports.",@"12”x8.5”", nil] forKey:@"image13"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"GIA Placards",@"Acrylic Placard-Diamond",@"For Retail Counter Display to communicate to customers that diamonds sold ( or rubies sold etc) are graded by GIA.",@"2.5”x4”", nil] forKey:@"image14"];
    
    //[mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"GIA Placards",@"Acrylic Placard-Ruby",@"For Retail Counter Display to communicate to customers that diamonds sold ( or rubies sold etc) are graded by GIA.",@"2.5”x4”", nil] forKey:@"image12"];
    
    //[mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"GIA Placards",@"Acrylic Placard-Emerald",@"For Retail Counter Display to communicate to customers that diamonds sold ( or rubies sold etc) are graded by GIA.",@"2.5”x4”", nil] forKey:@"image13"];
    
    //[mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"GIA Placards",@"Acrylic Placard-Sapphire",@"For Retail Counter Display to communicate to customers that diamonds sold ( or rubies sold etc) are graded by GIA.",@"2.5”x4”", nil] forKey:@"image14"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"GIA Placards",@"Acrylic Placard-Pearl",@"For Retail Counter Display to communicate to customers that diamonds sold ( or rubies sold etc) are graded by GIA.",@"2.5”x4”", nil] forKey:@"image15"];
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"LED Signage",@"",@"A Glow Sign at retail outlets/windows communicating that the Diamonds offered are graded by GIA.",@"15”x24”", nil] forKey:@"image16"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Retailer Kit",@"",@"Support retail tools for a retailer to help educate the customer on the GIA 4cs , how to read a GIA  Report & a basic introduction on how to choose A DIAMOND",@"", nil] forKey:@"image17"];
    
    //[mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Acrylic Card Holder",@"",@"An ideal visiting card holder for a retailer on the shop counter",@"L-4 W-2.5", nil] forKey:@"image18"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Retailer Tray",@"",@"An ideal set of 2 nest able trays for a retail counter to display jewellery or loose diamonds & gemstones. Serves well as a diamond sorting tray too.",@"L-15 W-12", nil] forKey:@"image19"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Two-Slot Dispenser",@"",@"The 4cs Dispenser – A 2 slot dispenser for display at a retail counter holding the 4 Cs brochure in 2 languages of choice.",@"12”x8.5”", nil] forKey:@"image20"];
    
//    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Two-Slot Dispenser",@"",@"The 4cs Dispenser – A 2 slot dispenser for display at a retail counter holding the 4 Cs brochure in 2 languages of choice.",@"", nil] forKey:@"image21"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Acrylic Stand",@"",@"For retail counter display announcing “ We Offer Diamonds Graded By GIA”",@"5”x7”", nil] forKey:@"image22"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Diamond Holder",@"",@"Single Diamond Boxes for retail to offer customers. It holds a GIA graded diamond.",@"3”x2.5”", nil] forKey:@"image23"];
    
    //[mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Pen Tumbler",@"",@"A Paper Tumbler to hold all kinds of desk stationary.Ideal usage for students, offices & retail outlets.It communicates the highlights of the important gemstones like ruby, diamond, emerald & sapphire.",@"4.5”", nil] forKey:@"image24"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Flip Chart",@"",@"An Informative Flip Chart for retail counters to educate customers on the GIA 4Cs of diamond quality.",@"12”x10”", nil] forKey:@"image25"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Diamond Holder Tray",@"",@"Display tray for retail counters displaying diamonds graded by GIA in its single box cases.",@"7.5”x9.5", nil] forKey:@"image26"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Diamond Sealed card holder",@"",@"Display tray for a retail counter for displaying diamond sealed cards holding diamonds graded by GIA .  It helps the retailer to educate customers that the sealed cards on diplay helps to keep the diamond & its grading details safe, secure & together. ",@"10”x8”", nil] forKey:@"image27"];
    
    [mainitemsTextDic setObject:[[NSArray alloc] initWithObjects:@"Cut grade chart poster",@"",@"A set of 2 charts & a booklet on the GIA Cut grade System for round brilliant diamonds.Ideal for display at diamond manufacturing units.",@"", nil] forKey:@"image28"];
   // DebugLog(@"%@",mainitemsTextDic);
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    DebugLog(@"count %d",[thumbsitems count]);
    //return [thumbsitems count];
    return mainitemsTextDic.allKeys.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 100.0f)];
        //((UIImageView *)view).image = [UIImage imageNamed:[thumbsitems objectAtIndex:index]];
        NSString *imageName = [[mainitemsTextDic.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
            return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
        }] objectAtIndex:index];
        NSString *fullimageName = [NSString stringWithFormat:@"%@_thumb",imageName];
        
       // DebugLog(@"viewForItemAtIndex index = %d fullimageName = -%@-",index,fullimageName);
        ((UIImageView *)view).image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fullimageName ofType:@"jpg"]];
        view.contentMode = UIViewContentModeCenter;
        
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
   // ((UIImageView *)view).image = [UIImage imageNamed:[thumbsitems objectAtIndex:index]];
    NSString *imageName = [[mainitemsTextDic.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
    }] objectAtIndex:index];
    
//    NSString *imageName = [mainitemsTextDic.allKeys objectAtIndex:index];
    NSString *fullimageName = [NSString stringWithFormat:@"%@_thumb",imageName];
    ((UIImageView *)view).image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fullimageName ofType:@"jpg"]];

   // DebugLog(@"viewForItemAtIndex index = %d fullimageName = -%@-",index,fullimageName);

    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    //label.text = [[items objectAtIndex:index] stringValue];
    
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    //NSString *imageName = [mainitems objectAtIndex:index];
    NSString *imageName = [[mainitemsTextDic.allKeys sortedArrayUsingComparator:^(id firstObject, id secondObject) {
        return [((NSString *)firstObject) compare:((NSString *)secondObject) options:NSNumericSearch];
    }] objectAtIndex:index];
    
    mainImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"]];
    
//    NSRange temprange = [imageName rangeOfString:@"."];
//    //DebugLog(@"location %d length %d",temprange.location,temprange.length);
//    if ((temprange.location != NSNotFound))
//    {
//        imageName = [imageName substringToIndex:temprange.location];
//    }
    //DebugLog(@"didSelectItemAtIndex index====%d imageName = -%@-",index,imageName);
    
    NSArray *array = [mainitemsTextDic objectForKey:imageName];
    headinglbl.text = [array objectAtIndex:0];
    subheadinglbl.text = [array objectAtIndex:1];
    dimensionlbl.text = [array objectAtIndex:3];
    if([dimensionlbl.text isEqualToString:@""])
    {
        [dimensiontextlbl setHidden:TRUE];
    } else
    {
        [dimensiontextlbl setHidden:FALSE];
    }
    descriptiontextview.text = [array objectAtIndex:2];
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.5f;
    }
    return value;
}

- (IBAction)orderBtnClicked:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"Order"]) {
        quantityTextField.hidden = FALSE;
        [orderBtn setTitle:@"Confirm" forState:UIControlStateNormal];
        [quantityTextField becomeFirstResponder];
    }
    else
    {
        if([quantityTextField.text isEqualToString:@""])
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:@"Please enter number of quantity."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            
        }
        else if ([quantityTextField.text doubleValue] > 0 && [quantityTextField.text doubleValue]<=1000)
        {
            DebugLog(@"quantityTextField.text doubleValue %f",[quantityTextField.text doubleValue]);
            orderController = [[GIAPOSOrderViewController alloc] initWithNibName:@"GIAPOSOrderViewController" bundle:nil];
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:orderController];
            navController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            navController.modalPresentationStyle = UIModalPresentationFormSheet;
            navController.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
            orderController.navigationItem.title = @"Order POS Material";
            orderController.posdelegate = self;
            
            UIBarButtonItem *cancelBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                                           target:self
                                                                                           action:@selector(cancelViewController)];
            orderController.navigationItem.rightBarButtonItem = cancelBarButton;
            
            [self presentViewController:navController animated:YES completion:NULL];
            
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                message:@"Please enter number of quantity between 1-1000."
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
            [alertView show];
            quantityTextField.text = @"";
        }
        
    }
}
- (void)didDismissPresentedViewController
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [self sendEmail];
    }];
    orderController.posdelegate = nil;
    orderController = nil;
    
    
}

- (void)cancelViewController
{
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    //orderController.posdelegate = nil;
    orderController = nil;
}

-(void)sendEmail
{
    
    @try{
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        NSString *firstName      = [defaults objectForKey:LOGIN_FIRST_NAME];
//        NSString *lastName       = [defaults objectForKey:LOGIN_LAST_NAME];
//        NSString *phoneNumber    = [defaults objectForKey:LOGIN_NUMBER];
//        NSString *emailAddress   = [defaults objectForKey:LOGIN_EMAIL];
//        NSString *address   = [defaults objectForKey:LOGIN_ADDRESS];
//        NSString *city   = [defaults objectForKey:LOGIN_CITY];
//        NSString *profile  = [defaults objectForKey:LOGIN_PROFILE];
//        
//        DebugLog(@"%@ %@ %@ %@ -%@-",firstName,lastName,phoneNumber,emailAddress,address);
//        NSString *bodyMessage;
        
        NSString *productString;
        if ([subheadinglbl.text isEqualToString:@""]) {
            productString = headinglbl.text;
        }else{
            productString = [NSString stringWithFormat:@"%@ %@",subheadinglbl.text,headinglbl.text];
        }
                
//        bodyMessage = [NSString stringWithFormat:@"  Name :  %@ %@\n\n  PhoneNumber :  %@\n\n  Email :  %@\n\n  City :  %@\n\n  Profile :  %@\n\n  Address :  %@\n\n  Product :  %@\n\n  Quantity :  %@",firstName,lastName,phoneNumber,emailAddress,city,profile, address ,productString, quantityTextField.text];
//        MFMailComposeViewController *picker=[[MFMailComposeViewController alloc]init];
//        picker.mailComposeDelegate=self;
//        picker.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
//        [picker setToRecipients:[[NSArray alloc] initWithObjects:POS_EMAIL, nil]];
//        [picker setSubject:@"POS Order"];
//        [picker setMessageBody:bodyMessage isHTML:NO];
//        [self presentModalViewController:picker animated:YES];
//        
//        if (DEVELOPER_MODE) {
            BOOL success =  [SQLiteDatabaseOpertaions insertIntoPOSREQTable:productString quantity:quantityTextField.text];
            if (success) {
                [Flurry logEvent:@"POS_Requisition_Send"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                    message:@"Your order has been placed successfully."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                
                [alertView show];
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                                    message:@"Oops! an error has occured. Please try again."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                
                [alertView show];
            }
//        }
    }
    @catch (NSException *exception)
    {
        DebugLog(@"Exception %@",exception.reason);
    }
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    [controller dismissModalViewControllerAnimated:YES];
    if (result == MFMailComposeResultSent){
        DebugLog(@"email sent");
        
        NSString *productString;
        
        if ([subheadinglbl.text isEqualToString:@""]) {
            productString = headinglbl.text;
        }else{
            productString = [NSString stringWithFormat:@"%@ %@",subheadinglbl.text,headinglbl.text];
        }
        
        [SQLiteDatabaseOpertaions insertIntoPOSREQTable:productString quantity:quantityTextField.text];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return NO;
}
@end
