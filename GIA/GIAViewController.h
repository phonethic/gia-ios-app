//
//  GIAViewController.h
//  GIA
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"
#import "GIALoginViewController.h"

@interface GIAViewController : UIViewController <GIAViewControllerDelegate>
{
    CAEmitterLayer *emitter;
}
- (IBAction)testBtnPressed:(id)sender;
@end
