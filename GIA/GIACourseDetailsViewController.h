//
//  GIACourseDetailsViewController.h
//  GIA
//
//  Created by Kirti Nikam on 04/06/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface GIACourseDetailsViewController : UIViewController
{
    sqlite3 *database;
}
@property (nonatomic,copy) NSString *selectedCourseId;

@property (nonatomic,copy) NSString *region;
@property (nonatomic,copy) NSString *state;
@property (nonatomic,copy) NSString *city;
@property (nonatomic,copy) NSString *startDate;
@property (nonatomic,copy) NSString *endDate;
@property (nonatomic,copy) NSString *approval;
@property (nonatomic,copy) NSString *seats;
@property (nonatomic,copy) NSString *tentative_enrollments;
@property (nonatomic,copy) NSString *confimed_enrollments;

@property (strong, nonatomic) IBOutlet UITableView *courseDetailTableView;
@end
