//
//  GIALoginViewController.h
//  GIA
//
//  Created by Kirti Nikam on 07/05/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GIAViewControllerDelegate <NSObject>
- (void)didDismissPresentedViewController;
@end

@interface GIALoginViewController : UIViewController <UIActionSheetDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
     UIActionSheet *actionSheet;
}
@property (strong, nonatomic) IBOutlet UILabel *headinglbl;
@property (nonatomic, weak) id<GIAViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITextField *firstNameField;
@property (strong, nonatomic) IBOutlet UITextField *lastNameField;
@property (strong, nonatomic) IBOutlet UITextField *emailField;
@property (strong, nonatomic) IBOutlet UITextField *numberField;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
@property (strong, nonatomic) IBOutlet UITextField *cityField;
@property (strong, nonatomic)  NSArray *profileArray;
@property (strong, nonatomic) IBOutlet UIButton *profileBtn;
@property (strong, nonatomic) IBOutlet UIView *loginView;
@property (strong, nonatomic) IBOutlet UIScrollView *loginscrollView;
@property (strong, nonatomic) IBOutlet UIPickerView *profilePickerView;
@property (strong, nonatomic) IBOutlet UITextField *otherProfileField;

- (IBAction)submitBtnPressed:(id)sender;
- (IBAction)profileBtnPressed:(id)sender;
@end
