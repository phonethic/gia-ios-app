//
//  GIALabServicesDetailViewController.h
//  GIA
//
//  Created by Kirti Nikam on 05/07/13.
//  Copyright (c) 2013 Phonethics. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface GIALabServicesDetailViewController : UIViewController<UISplitViewControllerDelegate,UIWebViewDelegate,MFMailComposeViewControllerDelegate,UITextViewDelegate>
{
    int countSelection;
}
@property (nonatomic,copy) NSString *detailText;
@property (nonatomic,retain) NSMutableArray *requestArray;

@property (strong, nonatomic) IBOutlet UIWebView *contentWebView;
@property (strong, nonatomic) IBOutlet UIView *requestView;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn1;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn2;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn3;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn4;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn5;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn6;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn7;
@property (strong, nonatomic) IBOutlet UIButton *checkBoxBtn8;
@property (strong, nonatomic) IBOutlet UITextView *commenttextView;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) IBOutlet UILabel *lblDgrading;
@property (strong, nonatomic) IBOutlet UILabel *lblDsealing;
@property (strong, nonatomic) IBOutlet UILabel *lblCDgrading;
@property (strong, nonatomic) IBOutlet UILabel *lblCDidentification;
@property (strong, nonatomic) IBOutlet UILabel *lblSDgrading;
@property (strong, nonatomic) IBOutlet UILabel *lblCSidentification;
@property (strong, nonatomic) IBOutlet UILabel *lblCSorigin;
@property (strong, nonatomic) IBOutlet UILabel *lblQualityAS;
@property (strong, nonatomic) IBOutlet UILabel *lblPidentification;
@property (strong, nonatomic) IBOutlet UILabel *lblrequestHeader;
@property (strong, nonatomic) IBOutlet UILabel *lblCommentsHeader;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

-(void)loadWebViewWithUrl:(NSString *)url;
-(void)loadRequestView;

- (IBAction)checkboxBtnPressed:(id)sender;
- (IBAction)submitBtnPressed:(id)sender;
@end
